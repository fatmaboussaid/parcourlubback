-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : jeu. 23 sep. 2021 à 10:46
-- Version du serveur : 5.5.65-MariaDB
-- Version de PHP : 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `lubEnLigne`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id` int(11) NOT NULL,
  `offer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carburant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_receipt` datetime DEFAULT NULL,
  `reception_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` tinyint(1) DEFAULT NULL,
  `order_status` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id`, `offer_id`, `product_id`, `station_id`, `user_id`, `marque`, `modele`, `year`, `carburant`, `date_of_receipt`, `reception_period`, `payment_status`, `order_status`, `is_completed`, `price`, `quantity`, `uid`, `create_at`) VALUES
(138, 2, NULL, 4, 7, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', '2021-08-31 00:00:00', '13:30', 0, 1, 1, 8, 1, '138612d1b99d904f', '2021-08-30 17:53:38'),
(139, 44, NULL, 7, 7, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', '2021-08-31 00:00:00', '08:00', 0, 1, 1, 155.6, 1, '139612d1bcecb922', '2021-08-30 17:55:55'),
(140, NULL, 5, 4, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 8.4, 3, '140612d1c01306cd', '2021-08-30 17:57:17'),
(141, NULL, 6, 12, NULL, 'CHERY', 'A3, E3, A19, Bonus 1.5', '2013', 'Essence', NULL, NULL, 0, 0, 0, 10, 1, NULL, '2021-08-30 17:57:40'),
(142, NULL, 6, 12, 7, 'CHERY', 'A3, E3, A19, Bonus 1.5', '2013', 'Essence', '2021-08-31 00:00:00', '11:00', 0, 1, 1, 10, 1, '142612d1d0309502', '2021-08-30 17:59:52'),
(143, 2, NULL, 4, 7, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Hybride', '2021-09-28 00:00:00', '11:30', 0, 1, 1, 8, 1, '143612d2083c6313', '2021-08-30 18:11:31'),
(144, NULL, 15, 17, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, '144612d2058cdc86', '2021-08-30 18:14:32'),
(145, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, NULL, '2021-08-30 18:19:29'),
(146, 2, NULL, 1, 7, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', '2021-09-21 00:00:00', '10:30', 0, 1, 1, 8, 1, '146612d322e575ea', '2021-08-30 19:31:28'),
(147, NULL, 16, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, NULL, '2021-08-30 21:48:28'),
(148, 51, NULL, 1, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '11:30', 0, 1, 1, 138, 1, '148612d53e8df124', '2021-08-30 21:55:09'),
(149, NULL, 16, 1, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, '149612d5419f0f32', '2021-08-30 21:56:39'),
(150, NULL, 7, 18, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-03 00:00:00', '10:00', 0, 1, 1, 91.5, 1, '150612d5474239a2', '2021-08-30 21:58:04'),
(151, 50, NULL, 6, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 136, 1, NULL, '2021-08-30 21:59:02'),
(152, NULL, 11, 5, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 89.3, 1, '152612d54d5e975b', '2021-08-30 21:59:44'),
(153, 49, NULL, 6, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '09:00', 0, 1, 1, 134, 1, '153612d556a1028a', '2021-08-30 22:00:28'),
(154, NULL, 7, 16, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Diesel', NULL, NULL, 0, 0, 0, 91.5, 1, NULL, '2021-08-30 22:25:25'),
(155, NULL, 7, 16, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-02 00:00:00', '10:00', 0, 1, 1, 91.5, 1, '155612deb271fe78', '2021-08-31 08:39:31'),
(156, NULL, 16, 1, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 1, 1, NULL, '2021-08-31 08:41:42'),
(157, NULL, 5, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 2.8, 1, NULL, '2021-08-31 10:21:10'),
(158, NULL, 5, 1, NULL, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', NULL, NULL, 0, 0, 0, 2.8, 1, NULL, '2021-08-31 10:41:27'),
(159, NULL, 5, 6, NULL, 'CHERY', 'A3, A19, Bonus 1.5', '2009', 'Diesel', NULL, NULL, 0, 0, 0, 2.8, 1, NULL, '2021-08-31 10:44:14'),
(160, NULL, 5, 10, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 2.8, 1, NULL, '2021-08-31 10:44:29'),
(161, 22, NULL, 13, 7, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', '2021-09-01 00:00:00', '08:30', 0, 1, 1, 98.78, 1, '161612e0ec20029e', '2021-08-31 11:11:28'),
(162, 50, NULL, 5, 7, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', '2021-09-01 00:00:00', '13:30', 0, 1, 1, 136, 1, '162612e0f2e48108', '2021-08-31 11:14:40'),
(163, NULL, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 11.2, 4, NULL, '2021-08-31 11:22:33'),
(164, NULL, 14, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 122, 1, '164612e13bdad360', '2021-08-31 11:33:01'),
(165, 22, NULL, 6, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-08 00:00:00', '14:30', 0, 0, 0, 98.78, 1, NULL, '2021-08-31 12:29:36'),
(166, 25, NULL, 18, 7, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '09:30', 0, 1, 1, 103.5, 1, '166612e27bdd0a17', '2021-08-31 12:59:05'),
(167, 48, NULL, 13, 7, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', '2021-09-01 00:00:00', '10:00', 0, 1, 1, 133.5, 1, '167612e27f10850e', '2021-08-31 13:00:21'),
(168, NULL, 6, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 30, 3, '168612e28058ef1b', '2021-08-31 13:00:49'),
(169, NULL, 6, 1, 7, 'CHEVROLET (EU)', 'Aveo 1.2', '2009', 'Diesel', '2021-09-09 00:00:00', '19:00', 0, 1, 1, 10, 2, '169612e28329b406', '2021-08-31 13:01:24'),
(170, 51, NULL, 5, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '11:00', 0, 0, 0, 138, 1, NULL, '2021-08-31 13:09:58'),
(171, 27, NULL, 18, 7, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '10:30', 0, 1, 1, 107.5, 1, '171612e2a4ce56e0', '2021-08-31 13:10:03'),
(172, 40, NULL, 6, 7, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', '2021-09-09 00:00:00', '12:30', 0, 1, 1, 141.36, 1, '172612e2a75a5457', '2021-08-31 13:11:02'),
(173, NULL, 6, 4, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 20, 2, '173612e2a9956c08', '2021-08-31 13:11:43'),
(174, NULL, 6, 4, 7, 'CHERY', 'A5, E5, Fora, Elara 1.5  16V, 1.6  16V, 2.0  16V', '2006', 'Diesel', '2021-09-03 00:00:00', '11:30', 0, 1, 1, 10, 3, '174612e2acb9a80f', '2021-08-31 13:12:28'),
(175, 51, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-08-31 16:35:38'),
(176, 22, NULL, 18, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-03 00:00:00', '08:30', 0, 1, 1, 98.78, 1, '176612e77e03a6df', '2021-08-31 18:41:15'),
(177, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 98.78, 1, NULL, '2021-08-31 19:07:17'),
(178, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 98.78, 1, NULL, '2021-08-31 19:07:36'),
(179, 22, NULL, 18, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-01 00:00:00', '12:30', 0, 1, 1, 98.78, 1, '179612e7ebb1cda1', '2021-08-31 19:08:12'),
(180, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 98.78, 1, NULL, '2021-08-31 21:31:38'),
(181, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 98.78, 1, NULL, '2021-09-01 13:25:54'),
(182, 22, NULL, 18, 26, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-08 00:00:00', '08:30', 0, 1, 1, 98.78, 1, '182612f8021a921b', '2021-09-01 13:27:04'),
(183, 49, NULL, 5, NULL, 'CHERY', 'A5, E5, Fora, Elara 1.5  16V, 1.6  16V, 2.0  16V', '2006', 'Essence', NULL, NULL, 0, 0, 0, 134, 1, NULL, '2021-09-01 13:31:47'),
(184, NULL, 13, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 136.8, 1, NULL, '2021-09-01 13:36:03'),
(185, NULL, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, NULL, '2021-09-02 15:23:22'),
(186, NULL, 17, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 70, 1, NULL, '2021-09-07 12:28:28'),
(187, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-07 12:29:28'),
(188, 26, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 105.5, 1, NULL, '2021-09-07 12:31:24'),
(189, 26, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 105.5, 1, NULL, '2021-09-07 12:31:42'),
(190, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-09 00:00:00', '08:30', 0, 0, 0, 98.78, 1, NULL, '2021-09-08 20:53:57'),
(191, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-10 00:00:00', '09:00', 0, 0, 0, 98.78, 1, NULL, '2021-09-08 23:22:50'),
(192, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-10 00:00:00', '08:30', 0, 0, 0, 98.78, 1, NULL, '2021-09-08 23:44:08'),
(193, 26, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 105.5, 1, NULL, '2021-09-09 11:13:07'),
(194, 22, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-10 00:00:00', '12:00', 0, 0, 0, 98.78, 1, NULL, '2021-09-09 13:33:43'),
(195, 42, NULL, 5, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-16 00:00:00', '09:00', 0, 0, 0, 153.1, 1, NULL, '2021-09-09 13:36:20'),
(196, NULL, 17, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 60, 1, NULL, '2021-09-09 13:36:52'),
(197, NULL, 17, 1, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', '2021-09-10 00:00:00', '19:30', 0, 0, 0, 60, 1, NULL, '2021-09-09 13:37:30'),
(198, NULL, 14, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', '2021-09-14 00:00:00', '08:00', 0, 0, 0, 122, 1, NULL, '2021-09-12 23:09:20'),
(199, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-12 23:51:52'),
(200, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-12 23:52:52'),
(201, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-12 23:53:20'),
(202, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-12 23:54:42'),
(203, 23, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-12 23:57:35'),
(204, 50, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-14 00:00:00', '11:30', 0, 0, 0, 136, 1, NULL, '2021-09-13 00:01:02'),
(205, 22, NULL, 18, 28, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2012', 'Essence', '2021-09-14 00:00:00', '08:30', 0, 1, 1, 98.78, 1, '205613f64ff4bda0', '2021-09-13 14:47:23'),
(206, 24, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '12:00', 0, 0, 0, 103, 1, NULL, '2021-09-13 23:18:33'),
(207, 50, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2012', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 136, 1, NULL, '2021-09-14 08:01:04'),
(208, 50, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 136, 1, NULL, '2021-09-14 08:42:01'),
(209, 50, NULL, 5, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 136, 3, NULL, '2021-09-14 08:44:28'),
(210, 50, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '11:30', 0, 0, 0, 136, 3, NULL, '2021-09-14 08:45:29'),
(211, 50, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '11:30', 0, 0, 0, 136, 3, NULL, '2021-09-14 08:46:09'),
(212, 50, NULL, 5, NULL, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Diesel', '2021-09-15 00:00:00', '11:00', 0, 0, 0, 136, 3, NULL, '2021-09-14 08:47:46'),
(213, 50, NULL, 5, NULL, 'CHEVROLET (EU)', '1210, 1310, 1410, 1610, Denem', '1977', 'Diesel', '2021-09-15 00:00:00', '10:30', 0, 0, 0, 136, 3, NULL, '2021-09-14 08:48:43'),
(214, 50, NULL, 5, NULL, 'CHEVROLET (EU)', 'Aveo 1.2', '2008', 'Essence', NULL, NULL, 0, 0, 0, 408, 3, NULL, '2021-09-14 08:50:08'),
(215, 50, NULL, 5, NULL, 'CHERY', 'A3, A19, Bonus 1.5', '2009', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 408, 3, NULL, '2021-09-14 08:52:16'),
(216, 49, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 134, 1, NULL, '2021-09-14 09:30:35'),
(217, 51, NULL, 18, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-09-14 09:31:33'),
(218, 50, NULL, 18, 7, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-15 00:00:00', '08:00', 0, 1, 1, 136, 1, '21861408a2dc9c26', '2021-09-14 11:37:05'),
(219, 22, NULL, 18, NULL, 'FORD (EU)', 'Fiesta 1.25 16V', '2013', 'Essence', NULL, NULL, 0, 0, 0, 98.78, 1, NULL, '2021-09-14 15:43:03'),
(220, 22, NULL, 18, NULL, 'FORD (EU)', 'Fiesta 1.25 16V', '2013', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 98.78, 1, NULL, '2021-09-14 15:44:08'),
(221, 22, NULL, 18, NULL, 'FORD (EU)', 'Fiesta 1.25 16V', '2013', 'Essence', '2021-09-15 00:00:00', '08:30', 0, 0, 0, 98.78, 1, NULL, '2021-09-14 16:31:27'),
(222, 22, NULL, 9, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 00:00:00', '08:00', 0, 0, 0, 98.78, 1, NULL, '2021-09-14 17:04:20'),
(223, 51, NULL, 13, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 00:00:00', '08:30', 0, 0, 0, 138, 1, NULL, '2021-09-14 20:47:27'),
(224, 51, NULL, 13, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-09-14 20:50:07'),
(225, 51, NULL, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2004', 'Essence', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-09-15 11:14:11'),
(226, NULL, 14, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 122, 1, NULL, '2021-09-15 14:49:03'),
(227, 22, NULL, 14, NULL, 'DACIA', 'Dokker 1.6 SCe, 1.6 SCe LPG', '2018', 'Essence', '2021-09-16 00:00:00', '08:30', 0, 0, 0, 103.35, 1, NULL, '2021-09-15 14:54:57'),
(228, 40, NULL, 13, 29, 'SEAT', 'Ibiza, Cordoba 1.4 TDi DPF', '2007', 'Essence', '2021-09-16 00:00:00', '08:30', 0, 1, 1, 146.34, 1, '22861420d8de3e0f', '2021-09-15 15:01:56'),
(229, 51, NULL, 13, NULL, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', '2021-09-16 00:00:00', '12:30', 0, 0, 0, 138, 1, NULL, '2021-09-15 17:02:14'),
(230, NULL, 14, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 244, 2, NULL, '2021-09-15 21:53:54'),
(231, NULL, 14, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 244, 2, NULL, '2021-09-15 21:55:23'),
(232, NULL, 14, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 244, 2, NULL, '2021-09-15 21:56:32'),
(233, NULL, 14, 9, NULL, 'DACIA', 'Dokker 1.6 MPI, 1.6 LPG', '2013', 'Essence', '2021-09-16 00:00:00', '09:30', 0, 0, 0, 122, 1, NULL, '2021-09-15 21:57:51'),
(234, 45, NULL, 13, NULL, 'BMW', '118d,  120d,  DPF (E87/E82/E81/E88)', '2009', 'Essence', '2021-09-16 00:00:00', '08:00', 0, 0, 0, 157.6, 1, NULL, '2021-09-15 22:05:22'),
(235, 22, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-16 09:20:37'),
(236, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 141.6, 1, NULL, '2021-09-16 09:33:59'),
(237, 22, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-16 09:40:59'),
(238, 51, NULL, 13, NULL, 'AUDI', 'A1 1.2 TDI', '2000', 'Diesel', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-09-16 09:45:51'),
(239, 23, NULL, 13, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '09:30', 0, 0, 0, 104, 1, NULL, '2021-09-16 09:48:10'),
(240, 6, NULL, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 85.2, 1, NULL, '2021-09-16 09:50:20'),
(241, 49, NULL, 14, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 00:00:00', '13:00', 0, 1, 1, 134, 1, '241614313ccb58f7', '2021-09-16 09:50:39'),
(242, NULL, 7, 14, 7, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-17 00:00:00', '08:00', 0, 1, 1, 91.5, 1, '2426143144082b49', '2021-09-16 09:54:01'),
(243, 24, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 103, 1, NULL, '2021-09-16 11:29:08'),
(244, 22, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '10:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-16 11:33:01'),
(245, 22, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-16 11:55:33'),
(246, 23, NULL, 12, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '10:30', 0, 0, 0, 104, 1, NULL, '2021-09-16 11:56:05'),
(247, 24, NULL, 5, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 00:00:00', '10:00', 0, 0, 0, 103, 1, NULL, '2021-09-16 11:56:34'),
(248, 23, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-16 11:57:14'),
(249, 23, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-16 11:57:19'),
(250, 23, NULL, 14, NULL, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-16 11:57:24'),
(251, NULL, 11, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 89.3, 1, NULL, '2021-09-16 12:43:08'),
(252, 51, NULL, 5, NULL, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', NULL, NULL, 0, 0, 0, 138, 1, NULL, '2021-09-16 12:48:51'),
(253, 51, NULL, 5, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Essence', '2021-09-17 00:00:00', '09:00', 0, 0, 0, 138, 1, NULL, '2021-09-16 12:49:30'),
(254, 25, NULL, 13, 31, 'RENAULT', 'Clio III  1.2 16V, 1.4 16V, 1.6 16V', '2009', 'Essence', '2021-09-30 00:00:00', '12:30', 0, 1, 1, 103.5, 1, '254614359f11baf8', '2021-09-16 14:49:44'),
(255, NULL, 14, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 109.8, 1, NULL, '2021-09-16 18:32:34'),
(256, 22, NULL, 5, NULL, 'KIA', 'Carens 1.6 CVVT', '2009', 'Essence', '2021-09-18 00:00:00', '08:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-17 10:16:34'),
(257, 22, NULL, 5, 7, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-18 00:00:00', '12:00', 0, 1, 1, 103.35, 1, '25761446ecfc7631', '2021-09-17 10:31:39'),
(258, NULL, 14, 5, 7, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-18 00:00:00', '08:30', 0, 1, 1, 109.8, 1, '25861446fb3d8ef8', '2021-09-17 10:36:24'),
(259, NULL, 13, 5, NULL, 'FAW', 'Vita', '2001', 'Essence', '2021-09-18 00:00:00', '09:00', 0, 0, 0, 123.12, 1, NULL, '2021-09-17 11:13:10'),
(260, 57, NULL, 5, NULL, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-18 00:00:00', '09:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-17 11:14:11'),
(261, 22, NULL, 14, 25, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-18 00:00:00', '10:00', 0, 1, 1, 103.35, 1, '261614478f876c45', '2021-09-17 11:14:17'),
(262, 52, NULL, 14, 8, 'AUDI', 'A1 1.2 TDI', '2001', 'Essence', '2021-09-18 00:00:00', '08:00', 0, 1, 1, 157.6, 1, '2626144796ab8bea', '2021-09-17 11:17:50'),
(263, 57, NULL, 5, 25, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-18 00:00:00', '10:30', 0, 1, 1, 141.36, 1, '26361447992098c5', '2021-09-17 11:18:34'),
(264, 52, NULL, 14, 32, 'AUDI', 'A2 1.4 TDI (P-D)', '2003', 'Essence', '2021-09-18 00:00:00', '09:30', 0, 1, 1, 157.6, 1, '264614491d990f7f', '2021-09-17 12:58:27'),
(265, 56, NULL, 5, 32, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Essence', '2021-09-18 00:00:00', '09:00', 0, 1, 1, 154.1, 1, '2656144932e0846c', '2021-09-17 13:07:45'),
(266, NULL, 19, 5, 32, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 127.44, 1, '266614493676f456', '2021-09-17 13:08:49'),
(267, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 127.44, 1, NULL, '2021-09-17 13:09:09'),
(268, 25, NULL, 14, NULL, 'BMW', '116i, 118i, 120i (E81/E82/E87/E88)', '2007', 'Essence', '2021-09-20 00:00:00', '09:00', 0, 0, 0, 103.5, 1, NULL, '2021-09-17 13:12:25'),
(269, 52, NULL, 14, 25, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-18 00:00:00', '11:30', 0, 1, 1, 157.6, 1, '2696144b0f5b80c0', '2021-09-17 15:14:47'),
(270, 52, NULL, 14, 8, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2020', 'Diesel', '2021-09-18 00:00:00', '11:00', 0, 1, 1, 157.6, 1, '2706144b31f33b52', '2021-09-17 15:24:01'),
(271, 22, NULL, 14, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-18 00:00:00', '13:00', 0, 0, 0, 103.35, 1, NULL, '2021-09-17 15:34:16'),
(272, 52, NULL, 14, NULL, 'VOLKSWAGEN (VW)', '(103kW, 125kW), 4Motion', '2005', 'Essence', NULL, NULL, 0, 0, 0, 157.6, 1, NULL, '2021-09-17 15:36:31'),
(273, 23, NULL, 13, 8, 'BRILLIANCE', 'FSV', '2003', 'Essence', '2021-09-20 00:00:00', '14:30', 0, 1, 1, 104, 1, '273614648e108ea5', '2021-09-18 20:13:12'),
(274, 57, NULL, 13, 33, 'AUDI', 'A2 1.2 TDI', '2003', 'Diesel', '2021-09-19 00:00:00', '11:00', 0, 1, 1, 141.36, 1, '27461464befa7739', '2021-09-18 20:25:14'),
(275, 22, NULL, 14, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 00:00:00', '15:00', 0, 1, 1, 103.35, 1, '2756147c2675024f', '2021-09-19 22:49:51'),
(276, 54, NULL, 5, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Diesel', '2021-09-21 00:00:00', '12:00', 0, 1, 1, 153.6, 1, '2766147c6bf8130a', '2021-09-19 23:14:06'),
(277, NULL, 12, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 99, 1, NULL, '2021-09-19 23:25:04'),
(278, NULL, 8, 5, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 58.56, 1, '2786147c85c8598e', '2021-09-19 23:30:22'),
(279, NULL, 8, 5, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 00:00:00', '09:00', 0, 1, 1, 58.56, 1, '2796147c897c72f7', '2021-09-19 23:32:21'),
(280, 56, NULL, 5, NULL, 'BRILLIANCE', 'FRV', '2001', 'Essence', NULL, NULL, 0, 0, 0, 154.1, 1, NULL, '2021-09-19 23:37:18'),
(281, 57, NULL, 5, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2012', 'Essence', NULL, NULL, 0, 0, 0, 282.72, 2, NULL, '2021-09-20 07:55:24'),
(282, 57, NULL, 5, NULL, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2012', 'Essence', '2021-09-22 00:00:00', '09:00', 0, 0, 0, 282.72, 2, NULL, '2021-09-20 07:55:25'),
(283, 52, NULL, 14, 25, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-21 00:00:00', '11:30', 0, 1, 1, 157.6, 1, '2836148480c7491f', '2021-09-20 08:36:06'),
(284, NULL, 19, 13, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 127.44, 1, '28461484d948a7ce', '2021-09-20 08:59:56'),
(285, NULL, 12, 5, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 99, 1, '28561484e12973ab', '2021-09-20 09:02:02'),
(286, 54, NULL, 13, 25, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-21 00:00:00', '15:30', 0, 1, 1, 153.6, 1, '28661484fc426823', '2021-09-20 09:08:55'),
(287, 22, NULL, 13, 25, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 00:00:00', '09:30', 0, 1, 1, 103.35, 1, '287614850d90f5e8', '2021-09-20 09:13:47'),
(288, 55, NULL, 13, 8, 'PEUGEOT', '3008  2.0  HDi DPF, HYbrid4 2.0  HDi DPF', '2018', 'Diesel avec FAP', '2021-09-21 00:00:00', '10:00', 0, 1, 1, 153.1, 1, '288614864282a201', '2021-09-20 10:35:36'),
(289, 25, NULL, 9, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2002', 'Essence', '2021-09-21 00:00:00', '08:30', 0, 0, 0, 103.5, 1, NULL, '2021-09-20 10:39:58'),
(290, NULL, 19, 5, 8, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 127.44, 1, '2906148652360238', '2021-09-20 10:40:30'),
(291, NULL, 19, 5, 8, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-21 00:00:00', '08:30', 0, 1, 1, 127.44, 1, '29161486597cad0d', '2021-09-20 10:41:14'),
(292, NULL, 14, 5, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 109.8, 1, '29261486c10412a9', '2021-09-20 11:10:03'),
(293, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 127.44, 1, NULL, '2021-09-20 12:41:42'),
(294, 52, NULL, 14, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-21 00:00:00', '12:30', 0, 0, 0, 157.6, 1, NULL, '2021-09-20 13:28:52'),
(295, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 127.44, 1, NULL, '2021-09-20 19:00:19'),
(296, 57, NULL, 5, NULL, 'AUDI', 'A1 1.2 TDI', '2001', 'Diesel', '2021-09-21 00:00:00', '11:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-20 19:01:12'),
(297, 52, NULL, 20, NULL, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-21 00:00:00', '12:30', 0, 0, 0, 157.6, 1, NULL, '2021-09-20 19:15:52'),
(298, 22, NULL, 20, 8, 'FORD', '(Grand) C-Max 1.6  Ti-VCT, EcoBoost', '2015', 'Essence', '2021-09-23 00:00:00', '09:00', 0, 1, 1, 103.35, 1, '298614971c136ae7', '2021-09-21 05:45:05'),
(299, 57, NULL, 20, 8, 'AUDI', 'A2 1.4 TDI (P-D)', '2001', 'Essence', '2021-09-22 00:00:00', '12:30', 0, 1, 1, 141.36, 1, '299614981ca8438e', '2021-09-21 06:54:52'),
(300, 14, NULL, 12, 25, 'AUDI', 'A1 1.2 TDI', '2003', 'Diesel', '2021-09-22 00:00:00', '10:30', 0, 1, 1, 96, 1, '3006149977e25712', '2021-09-21 08:22:01'),
(301, 55, NULL, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-22 00:00:00', '08:00', 0, 0, 0, 153.1, 1, NULL, '2021-09-21 08:52:20'),
(302, 23, NULL, 14, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-22 00:00:00', '09:30', 0, 1, 1, 104, 1, '3026149aa14177d6', '2021-09-21 09:40:50'),
(303, 56, NULL, 13, 25, 'AUDI', 'A1 1.2 TDI', '2001', 'Diesel', '2021-09-22 00:00:00', '08:30', 0, 3, 1, 154.1, 1, '3036149adc54c2b6', '2021-09-21 10:00:09'),
(304, 23, NULL, 14, 7, 'BRILLIANCE', 'Cross', '2001', 'Essence', '2021-09-22 00:00:00', '09:00', 0, 1, 1, 104, 1, '3046149b0dcf3096', '2021-09-21 10:03:52'),
(305, 55, NULL, 5, 7, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-22 00:00:00', '08:00', 0, 1, 1, 153.1, 1, '3056149afa49c64c', '2021-09-21 10:10:20'),
(306, NULL, 13, 5, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 123.12, 1, '3066149afce3af3f', '2021-09-21 10:11:22'),
(307, NULL, 12, 9, 7, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 99, 1, '3076149b22c41c3a', '2021-09-21 10:16:17'),
(308, 57, NULL, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2002', 'Essence', '2021-09-22 00:00:00', '11:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 11:02:01'),
(309, 29, NULL, 20, 25, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2003', 'Diesel', '2021-09-22 00:00:00', '11:30', 0, 3, 1, 122.5, 1, '3096149c3bb381bc', '2021-09-21 11:31:27'),
(310, 55, NULL, 20, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2002', 'Essence', '2021-09-22 00:00:00', '11:00', 0, 0, 0, 153.1, 1, NULL, '2021-09-21 11:46:48'),
(311, NULL, 19, 5, 25, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-22 00:00:00', '12:00', 0, 1, 1, 127.44, 1, '3116149e5a5dbdfb', '2021-09-21 13:59:27'),
(312, 52, NULL, 9, 25, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-24 00:00:00', '13:30', 0, 1, 1, 157.6, 1, '3126149e6926abe7', '2021-09-21 14:04:46'),
(313, 33, NULL, 9, 25, 'BMW', '214d, 216d Tourer (F45/F46)', '2015', 'Essence', '2021-09-23 00:00:00', '10:30', 0, 1, 1, 126, 1, '3136149eabc9e672', '2021-09-21 14:21:58'),
(314, NULL, 19, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 127.44, 1, NULL, '2021-09-21 14:26:55'),
(315, NULL, 19, 9, 25, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-22 00:00:00', '11:00', 0, 1, 1, 127.44, 1, '3156149ecf78e66b', '2021-09-21 14:32:10'),
(316, 57, NULL, 9, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2002', 'Diesel', '2021-09-22 00:00:00', '10:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 14:38:09'),
(317, 57, NULL, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2001', 'Diesel', '2021-09-22 00:00:00', '13:30', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 14:38:51'),
(318, 57, NULL, 20, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-22 00:00:00', '13:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 14:39:45'),
(319, 57, NULL, 9, NULL, 'ALFA ROMEO', 'GIULIETTA', '2001', 'Diesel', '2021-09-22 00:00:00', '10:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 14:44:47'),
(320, 57, NULL, 13, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-22 00:00:00', '10:00', 0, 0, 0, 141.36, 1, NULL, '2021-09-21 14:46:30'),
(321, 25, NULL, 9, 8, 'KIA', 'Picanto 1.1 CRDi VGT', '2011', 'Essence', '2021-09-22 00:00:00', '13:00', 0, 1, 1, 103.5, 1, '321614a066e5a318', '2021-09-21 16:20:16'),
(322, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:15:02'),
(323, NULL, 19, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:15:14'),
(324, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:16:03'),
(325, NULL, 19, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:16:24'),
(326, NULL, 19, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:16:48'),
(327, NULL, 19, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:17:00'),
(328, NULL, 19, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 17:17:13'),
(329, 29, NULL, 9, NULL, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-22 00:00:00', '13:30', 0, 0, 0, 111.5, 1, NULL, '2021-09-21 19:40:36'),
(330, NULL, 14, 5, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-23 00:00:00', '08:30', 0, 0, 0, 109.8, 1, NULL, '2021-09-21 23:23:54'),
(331, NULL, 19, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 128.7, 1, NULL, '2021-09-21 23:29:42'),
(332, 23, NULL, 20, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', NULL, NULL, 0, 0, 0, 104, 1, NULL, '2021-09-22 07:02:21'),
(333, 23, NULL, 20, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-23 00:00:00', '18:00', 0, 0, 0, 104, 1, NULL, '2021-09-22 07:08:11'),
(334, 53, NULL, 14, 25, 'AUDI', 'A2 1.2 TDI', '2003', 'Essence', '2021-09-23 00:00:00', '08:30', 0, 1, 1, 155.6, 1, '334614aeb0ed38a4', '2021-09-22 08:33:07'),
(335, NULL, 7, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 85.41, 1, NULL, '2021-09-22 12:43:35'),
(336, 52, NULL, 20, NULL, 'BMW', '118d,  120d,  DPF (E87/E82/E81/E88)', '2007', 'Essence', '2021-09-23 00:00:00', '09:30', 0, 0, 0, 157.6, 1, NULL, '2021-09-22 13:08:46'),
(337, 23, NULL, 20, NULL, 'PEUGEOT', '5008 1.6 16V Vti, THP', '2010', 'Essence', '2021-09-23 00:00:00', '16:00', 0, 0, 0, 97.91, 1, NULL, '2021-09-22 13:12:16'),
(338, 54, NULL, 13, 25, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-23 00:00:00', '11:00', 0, 1, 1, 140.2, 1, '338614b4345ca7c2', '2021-09-22 14:45:59'),
(339, 57, NULL, 13, NULL, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-23 00:00:00', '10:00', 0, 0, 0, 149.7, 1, NULL, '2021-09-22 14:54:49'),
(340, 25, NULL, 13, 25, 'AUDI', 'A3 2.0 TDI (P-D) DPF, quattro', '2000', 'Essence', '2021-09-23 00:00:00', '10:00', 0, 5, 1, 97.41, 1, '340614b44676b9e4', '2021-09-22 14:57:22'),
(341, NULL, 19, 13, 25, NULL, NULL, NULL, NULL, NULL, NULL, 0, 5, 1, 128.7, 1, '341614b44d2b73e5', '2021-09-22 14:59:26'),
(342, 57, NULL, 9, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-23 00:00:00', '11:30', 0, 0, 0, 149.7, 1, NULL, '2021-09-22 15:30:33'),
(343, 56, NULL, 13, NULL, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', NULL, NULL, 0, 0, 0, 141.2, 1, NULL, '2021-09-22 17:12:54');

-- --------------------------------------------------------

--
-- Structure de la table `commande_prestation`
--

CREATE TABLE `commande_prestation` (
  `commande_id` int(11) NOT NULL,
  `prestation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commande_prestation`
--

INSERT INTO `commande_prestation` (`commande_id`, `prestation_id`) VALUES
(138, 3),
(174, 3),
(177, 2),
(178, 2),
(179, 2),
(180, 6),
(181, 2),
(181, 11),
(182, 1),
(186, 1),
(186, 2),
(186, 6),
(186, 8),
(186, 11),
(186, 15),
(186, 16),
(187, 1),
(187, 2),
(187, 6),
(187, 8),
(187, 11),
(187, 15),
(187, 16),
(188, 1),
(188, 2),
(188, 6),
(188, 8),
(188, 11),
(188, 15),
(188, 16),
(189, 1),
(189, 2),
(189, 6),
(189, 8),
(189, 11),
(189, 15),
(189, 16),
(190, 1),
(190, 2),
(190, 6),
(190, 8),
(190, 11),
(190, 15),
(190, 16),
(191, 1),
(191, 2),
(191, 6),
(191, 8),
(191, 11),
(191, 15),
(191, 16),
(192, 1),
(192, 2),
(192, 6),
(192, 8),
(192, 11),
(192, 15),
(192, 16),
(193, 1),
(193, 2),
(193, 6),
(193, 8),
(193, 11),
(193, 15),
(193, 16),
(194, 2),
(194, 6),
(194, 8),
(198, 1),
(198, 2),
(199, 1),
(199, 2),
(200, 1),
(200, 2),
(200, 6),
(200, 8),
(200, 11),
(200, 15),
(200, 16),
(201, 1),
(201, 2),
(201, 6),
(201, 8),
(201, 11),
(201, 15),
(201, 16),
(205, 6),
(206, 1),
(206, 2),
(206, 6),
(207, 1),
(207, 2),
(207, 6),
(207, 8),
(207, 11),
(217, 1),
(217, 2),
(218, 1),
(218, 6),
(218, 11),
(219, 2),
(219, 6),
(220, 8),
(220, 11),
(221, 2),
(221, 6),
(223, 19),
(223, 20),
(223, 21),
(224, 21),
(225, 1),
(225, 2),
(225, 6),
(227, 2),
(227, 6),
(227, 19),
(228, 19),
(229, 1),
(229, 6),
(229, 19),
(233, 1),
(233, 8),
(234, 1),
(234, 6),
(235, 1),
(235, 3),
(237, 2),
(237, 3),
(237, 6),
(238, 1),
(238, 2),
(238, 8),
(239, 1),
(239, 3),
(240, 1),
(241, 1),
(242, 3),
(242, 16),
(243, 1),
(243, 3),
(252, 1),
(252, 3),
(253, 1),
(253, 3),
(256, 6),
(257, 1),
(257, 3),
(257, 6),
(258, 1),
(258, 3),
(258, 16),
(259, 1),
(259, 3),
(259, 16),
(260, 1),
(260, 3),
(260, 6),
(261, 1),
(261, 3),
(262, 1),
(262, 3),
(263, 1),
(264, 1),
(264, 19),
(265, 3),
(268, 1),
(268, 3),
(268, 19),
(270, 1),
(270, 3),
(270, 6),
(270, 19),
(271, 1),
(271, 3),
(272, 1),
(272, 3),
(273, 1),
(273, 8),
(274, 1),
(275, 1),
(275, 3),
(276, 1),
(276, 3),
(279, 1),
(279, 16),
(280, 1),
(281, 1),
(281, 3),
(282, 1),
(282, 3),
(283, 3),
(283, 19),
(286, 1),
(287, 1),
(287, 3),
(288, 1),
(288, 6),
(289, 1),
(289, 3),
(291, 16),
(294, 1),
(296, 1),
(297, 13),
(298, 13),
(298, 20),
(298, 21),
(299, 2),
(299, 13),
(300, 8),
(301, 1),
(301, 3),
(302, 1),
(303, 8),
(304, 1),
(304, 3),
(305, 1),
(305, 3),
(308, 1),
(308, 3),
(309, 13),
(310, 13),
(311, 3),
(311, 6),
(311, 16),
(312, 1),
(313, 1),
(313, 2),
(313, 3),
(313, 6),
(313, 8),
(315, 1),
(321, 1),
(321, 6),
(329, 1),
(329, 2),
(329, 6),
(330, 1),
(330, 16),
(332, 20),
(333, 13),
(333, 20),
(334, 6),
(336, 20),
(336, 21),
(337, 2),
(337, 20),
(338, 1),
(338, 3),
(339, 1),
(339, 3);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210608095433', '2021-06-08 09:55:04', 493),
('DoctrineMigrations\\Version20210608112118', '2021-06-08 11:21:32', 181);

-- --------------------------------------------------------

--
-- Structure de la table `offer`
--

CREATE TABLE `offer` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL,
  `price_promo` double DEFAULT NULL,
  `is_promo` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `offer`
--

INSERT INTO `offer` (`id`, `product_id`, `name`, `description`, `price`, `price_promo`, `is_promo`, `is_active`, `is_deleted`, `image`, `quantity`, `file`) VALUES
(1, 5, 'TOTAL Quartz 5000 20W50', NULL, 1.2, 2, 1, 1, 0, 'vector2-60f0105a7b22f.png', 0, NULL),
(2, 6, 'QUARTZ 9000 5W40+ Lavage extérieur offerts', 'QUARTZ 9000 5W40+ Lavage extérieur offerts', 10, 8, 1, 1, 0, 'vector1-60f0103811fac.png', 20, NULL),
(3, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Remise 20% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 4L (Remise 20%)\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange', 94.2, 79.56, 0, 0, 0, 'quartz700010w404l20-61430ca205e5b.png', 1, NULL),
(4, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace 4L gratuit', 102.7, 79.56, 1, 1, 0, 'quartz700010w404llaveglace-614306fc595a7.png', 1, NULL),
(5, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace Démoustiqueur 4L gratuit', 103.7, 79.56, 1, 1, 0, 'quartz700010w404ldemoustiqueur-6143071a625aa.png', 1, NULL),
(6, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur gratuit', 103.2, 79.56, 1, 1, 0, 'quartz700010w404llavage-614307a39f012.png', 1, NULL),
(7, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Ambiance Parfum gratuit', 'Le forfait vidange comprend: \r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 101.2, 79.56, 1, 1, 0, 'quartz700010w404lambiance-61430785d2f2b.png', 1, NULL),
(8, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Main d\'œuvre gratuite', 'Le forfait vidange comprend: \r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 94.2, 89.2, 1, 0, 0, 'asupprimer-61430db9a1f45.png', 1, NULL),
(9, 8, 'Forfait vidange Quartz 7000 10W-40 bidon 4L + Filtre à huile + Main d\'œuvre gratuite', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 4L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 94.2, 74.56, 1, 1, 0, 'quartz700010w404lmain-6143074382c4b.png', 1, NULL),
(10, 10, 'Forfait vidange Quartz 7000 10W-40 bidon 5L + Filtre à huile + Remise 20% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L (Remise 20%)\r\n- Un filtre à huile \r\n- Main d\'œuvre vidange', 103, 86.6, 0, 0, 0, 'quartz700010w405l20-61430ccaa8f56.png', 1, NULL),
(11, 10, 'Forfait vidange Quartz 7000 10W-40 5L + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace 4L gratuit', 111.5, 86.6, 1, 1, 0, 'quartz700010w405llaveglace-6143082d12491.png', 1, NULL),
(12, 10, 'Forfait vidange Quartz 7000 10W-40 5L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace Démoustiqueur 4L gratuit', 112.5, 86.6, 1, 1, 0, 'quartz700010w405ldemoustiqueur-6143085c31290.png', 1, NULL),
(13, 10, 'Forfait vidange Quartz 7000 10W-40 5L + Filtre à huile + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L\r\n- Une remise de 20% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur gratuit', 112.5, 86.6, 1, 1, 0, 'quartz700010w405llavage-6143087da6990.png', 1, NULL),
(14, 10, 'Forfait vidange Quartz 7000 10W-40 bidon 5L + Filtre à huile + Ambiance Parfum gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 110, 86.6, 1, 1, 0, 'quartz700010w405lambiance-61430896ec6c2.png', 1, NULL),
(15, 10, 'Forfait vidange Quartz 7000 10W-40 bidon 5L + Filtre à huile + Main d\'œuvre gratuite', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 7000 10W-40 5L\r\n- Une remise de 20% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 103, 81.6, 1, 1, 0, 'quartz700010w405lmain-614308ae613f8.png', 1, NULL),
(16, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Remise 20% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L (Remise 20%)\r\n- Filtre à huile \r\n- Main d\'œuvre vidange', 119.2, 99.36, 0, 0, 0, 'quartz4x45l20-61430e2472b14.png', 1, NULL),
(17, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace 4L offert', 127.7, 99.36, 1, 1, 0, 'quartz4x45llaveglace-61430958e86b2.png', 1, NULL),
(18, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L\r\n-Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace Démoustiqueur 4L offert', 128.7, 99.36, 1, 1, 0, 'quartz4x45ldemoustiqueur-6143097cd5de7.png', 1, NULL),
(19, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L\r\n- Une remise de 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage Extérieur gratuit', 128.7, 99.36, 1, 1, 0, 'quartz4x45llavage-6143099678415.png', 1, NULL),
(20, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Ambiance Parfum gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L\r\n- Une remise 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 126.2, 99.36, 1, 1, 0, 'quartz4x45lambiance-614309ad55431.png', 1, NULL),
(21, 11, 'Forfait vidange Quartz 4X4 15W-50 bidon 5L + Filtre à huile + Main d\'oeuvre gratuite', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 4X4 15W-50 5L\r\n- Une remise 20% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 119.2, 94.36, 1, 1, 0, 'quartz4x45lmain-614309e6f06f2.png', 0, NULL),
(22, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Filtre à huile + Main d\'œuvre + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 4L (Remise 10%)\r\n- Un filtre à huile\r\n- Main d\'œuvre', 115.9, 106.41, 0, 0, 0, 'quartz90005w404l10-614313f72ce21.png', 1, NULL),
(23, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Main d\'œuvre vidange + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 bidon 4L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange \r\n- Un bidon Lave Glace 4L gratuit', 124.4, 106.41, 1, 1, 0, 'quartz90005w404llaveglace2-614b47dfb066b.png', 1, NULL),
(24, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 4L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace Démoustiqueur 4L gratuit', 125.4, 106.41, 1, 1, 0, 'quartz90005w404ldemoustiqueur-61430a2ac85a5.png', 1, NULL),
(25, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Filtre à huile + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 4L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage Extérieur offert', 124.9, 106.41, 1, 1, 0, 'quartz90005w404llavage-61430a4cf1140.png', 1, NULL),
(26, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Filtre à huile + Ambiance Parfum gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 4L\r\n- Un filtre à huile\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 122.9, 106.41, 1, 1, 0, 'quartz90005w404lambiance-61430a7429731.png', 1, NULL),
(27, 7, 'Forfait vidange Quartz 9000 5W-40 bidon 4L + Filtre à huile + Main d\'œuvre gratuite', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 4L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 115.9, 101.41, 1, 1, 0, 'quartz90005w404lmain-61430a91cc132.png', 1, NULL),
(28, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L (Remise 10%)\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange', 134.8, 123.42, 0, 0, 0, 'quartz90005w405l10-614310ee06f47.png', 1, NULL),
(29, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace 4L offert', 143.3, 123.42, 1, 1, 0, 'quartz90005w405llaveglace-61430c1ec03e3.png', 1, NULL),
(30, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile +  Lave Glace Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace Démoustiqueur 4L gratuit', 144.3, 123.42, 1, 1, 0, 'quartz90005w405ldemoustiqueur-61430bf73c38a.png', 1, NULL),
(31, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur gratuit', 144.3, 123.42, 1, 1, 0, 'quartz90005w405llavage-61430bd8dd371.png', 1, NULL),
(32, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile + Ambiance Parfum gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 141.8, 123.42, 1, 1, 0, 'quartz90005w405lambiance-61430ba7b650f.png', 1, NULL),
(33, 12, 'Forfait vidange Quartz 9000 5W-40 bidon 5L + Filtre à huile + Main d\'œuvre gratuite', 'Le forfait vidange comprend:\r\n- Un bidon Quartz 9000 5W-40 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 134.8, 118.42, 1, 1, 0, 'quartz90005w405lmain-61430b81a351f.png', 1, NULL),
(34, 13, 'Forfait vidange Quartz Inéo MC3 bidon 5L + Filtre à huile + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo MC3 5L (Remise 10%)\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange', 159.2, 145.38, 0, 0, 0, 'quartzineomc35l10-614335f820f5c.png', 1, NULL),
(35, 13, 'Forfait vidange Quartz Inéo MC3 bidon 5L + Filtre à huile + Lave Glace 4L gratuit', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo MC3 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace 4L gratuit', 167.7, 145.38, 1, 1, 0, 'quartzineomc35llaveglace-61430ad2e30d5.png', 1, NULL),
(36, 13, 'Forfait vidange Quartz Inéo MC3 bidon 5L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo MC3 5L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace Démoustiqueur 4L gratuit', 168.7, 145.38, 1, 1, 0, 'quartzineomc35ldemoustiqueur-61430abb25056.png', 1, NULL),
(37, 13, 'Forfait vidange Quartz Inéo MC3 bidon 5L + Filtre à huile + Lavage Extérieur gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo MC3 5L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage Extérieur gratuit', 168.2, 145.38, 1, 1, 0, 'quartzineomc35llavage-61430aa885b21.png', 1, NULL),
(38, 13, 'Forfait vidange Quartz Inéo MC3 bidon 5L + Filtre à huile + Ambiance Parfum gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo MC3 5L\r\n- Une remise de 10% sur le bidon d\'huile \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 166.2, 145.38, 1, 1, 0, 'quartzineomc35lambiance2-614b480f57745.png', 1, NULL),
(39, 13, 'Forfait vidange Quartz Inéo MC3 5W-30 bidon 5L + Filtre à huile + Main d\'œuvre gratuite + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo MC3 5W-30 5L\r\n- Une remise de 10% sur le bidon lub \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 159.2, 140.38, 1, 1, 0, 'quartzineomc35lmain-614309c87680c.png', 1, NULL),
(40, 9, 'Quartz INEO LONG LIFE 5W30 5L + Remise 10%', 'Quartz INEO LONG LIFE 5W30 5L+Main d\'oeuvre vidange+Filtre à huile (10% remise sur l\'huile)', 162.6, 146.34, 1, 1, 0, 'quartzineolonglife5w305l10-61411b828159e.png', 1, NULL),
(41, 9, 'Quartz INEO LONG LIFE 5W30 5L + LG 4L gratuit', 'Quartz INEO LONG LIFE 5W30 5L+Main d\'oeuvre vidange+Filtre à huile+Lave Glace 4L gratuit', 162.6, 154.1, 1, 1, 0, '1-6128cfe5d8d6d.png', 1, NULL),
(42, 9, 'Quartz INEO LONG LIFE 5W30 5L + LG Démoustiqueur 4L gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz INEO LONG LIFE 5L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lave Glace démoustiqueur 4L offert', 162.6, 153.1, 1, 1, 0, '1-6128d02cd510d.png', 1, NULL),
(43, 9, 'Quartz INEO LONG LIFE 5W30 5L + Lavage Extérieur gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz INEO LONG LIFE 5W30 5L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur offert', 162.6, 153.6, 1, 1, 0, 'quartzineolonglife5w305llavage-61406948ed806.png', 1, NULL),
(44, 9, 'Quartz INEO LONG LIFE 5W30 5L + Ambiance Parfum gratuit', 'Le forfait vidange comprend:\r\n- Un bidon Quartz INEO LONG LIFE 5W30 5L\r\n- Main d\'œuvre vidange\r\n- Filtre à huile\r\n- Spray Ambiance Parfum offert', 162.6, 155.6, 1, 1, 0, '1-6128d0df1218a.png', 1, NULL),
(45, 9, 'Quartz INEO LONG LIFE 5W30 5L + Main d\'oeuvre gratuite', 'Le forfait vidange comprend::\r\n- Un bidon Quartz INEO LONG LIFE 5W30 5L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 162.6, 157.6, 1, 1, 0, 'quartzineolonglife5w305lmain-614068e774c41.png', 1, NULL),
(46, 14, 'Forfait vidange Quartz Inéo First 0W-30 bidon 5L + Filtre à huile + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo First 0W-30 5L (Remise 10%)\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange', 143, 128.7, 0, 0, 0, 'quartzineofirst0w305l10-614306167db43.png', 1, NULL),
(47, 14, 'Forfait vidange Quartz Inéo First 0W-30 bidon 5L + Filtre à huile + Lave Glace 4L gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo First 0W-30 5L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace 4L gratuit', 143, 134.5, 1, 1, 0, 'quartzineofirst0w305llaveglace-6143099463df2.png', 1, NULL),
(48, 14, 'Forfait vidange Quartz Inéo First 0W-30 bidon 5L + Filtre à huile + Lave Glace Démoustiqueur 4L gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo First 0W-30 5L\r\n- Un Filtre à huile\r\n- Main d\'œuvre vidange \r\n- Un bidon Lave Glace Démoustiqueur 4L gratuit', 143, 133.5, 1, 1, 0, 'quartzineofirst0w305ldemoustiqueur-6143093d64eda.png', 1, NULL),
(49, 14, 'Forfait vidange Quartz Inéo First 0W-30 bidon 5L + Filtre à huile + Lavage Extérieur gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo First 0W-30 5L\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur gratuit', 143, 134, 1, 1, 0, 'quartzineofirst0w305llavage-614308dedbca4.png', 1, NULL),
(50, 14, 'Forfait vidange  Quartz Inéo First 0W-30 bidon 5L + Filtre à huile  + Main d\'œuvre vidange + Ambiance Parfum gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n-  Un bidon Quartz Inéo First 0W-30 5L\r\n-  Un filtre à huile\r\n-  Main d\'œuvre vidange\r\n-  Spray Ambiance Parfum offert', 143, 136, 1, 1, 0, 'quartzineofirst0w305lambiance-614308ac76dd8.png', 1, NULL),
(51, 14, 'Forfait vidange Quartz Inéo First 0W-30 bidon 5L + Filtre à Huile  + Main d\'œuvre gratuite + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo First 0W-30 5L \r\n- Un filtre à huile\r\n- Main d\'œuvre vidange gratuite', 143, 138, 1, 1, 0, 'quartzineofirst0w305lmain-614308762d6e7.png', 1, NULL),
(52, 19, 'Forfait Vidange Quartz Inéo Long Life 5W-30 bidon 5L + Filtre à Huile + Main d\'œuvre gratuite + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo Long Life 5W-30 5L\r\n- Un filtre à Huile \r\n- Main d\'œuvre gratuite', 164, 144.7, 1, 1, 0, 'quartzineolonglife5w305lmain-61433d36cf310.png', 0, NULL),
(53, 19, 'Forfait vidange : Quartz Inéo Long Life 5W-30 bidon 5L + Main d\'œuvre vidange + Filtre à huile + Ambiance Parfum offert + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo Long Life 5W-30 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Spray Ambiance Parfum offert', 171, 149.7, 1, 1, 0, 'quartzineolonglife5w305lambiance-61434b9a4550e.png', 0, NULL),
(54, 19, 'Forfait vidange : Quartz Inéo Long Life 5W-30 bidon 5L + Main d\'œuvre vidange + Filtre à huile + Lavage Extérieur gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz INEO LONG LIFE 5W30 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Lavage extérieur gratuit', 173.5, 149.7, 1, 1, 0, 'quartzineolonglife5w305llavage-61434c17f07e3.png', 0, NULL),
(55, 19, 'Forfait vidange : Quartz Inéo Long Life 5W-30 bidon 5L + Main d\'œuvre vidange + Filtre à Huile + Lave Glace Démoustiqueur 4L gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo Long Life 5W-30 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile \r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace Démoustiqueur 4L offert', 173.5, 149.7, 1, 1, 0, 'quartzineolonglife5w305ldemoustiqueur-61435199867e9.png', 0, NULL),
(56, 19, 'Forfait vidange : Quartz Inéo Long Life 5W-30 bidon 5L + Main d\'œuvre vidange + Filtre à huile + Lave Glace 4L gratuit + Remise 10% sur l\'huile', 'Le forfait vidange comprend:\r\n- Un bidon Quartz Inéo Long Life 5W-30 5L\r\n- Une remise de 10% sur le bidon d\'huile\r\n- Un filtre à huile\r\n- Main d\'œuvre vidange\r\n- Un bidon Lave Glace 4L offert', 172.5, 149.7, 1, 1, 0, 'quartzineolonglife5w305llaveglace-61435267d1f1a.png', 0, NULL),
(57, 19, 'Forfait vidange Quartz Inéo Long Life 5W-30 bidon 5L + Filtre à huile + Main d\'œuvre + Remise 10% sur l\'huile', 'Le forfait vidange comprend: \r\n- Un bidon Quartz Inéo Long Life 5W-30 5L (Remise 10%)\r\n- Un filtre à huile \r\n- Main d\'œuvre', 164, 149.7, 0, 0, 0, 'quartzineolonglife5w305l10-61435366f2590.png', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `offer_prestation`
--

CREATE TABLE `offer_prestation` (
  `offer_id` int(11) NOT NULL,
  `prestation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `offer_prestation`
--

INSERT INTO `offer_prestation` (`offer_id`, `prestation_id`) VALUES
(1, 1),
(2, 2),
(2, 4),
(3, 15),
(3, 16),
(4, 15),
(4, 16),
(5, 15),
(5, 16),
(6, 6),
(6, 15),
(6, 16),
(7, 15),
(7, 16),
(8, 15),
(8, 16),
(9, 15),
(9, 16),
(10, 15),
(10, 16),
(11, 15),
(11, 16),
(12, 15),
(12, 16),
(13, 15),
(13, 16),
(14, 15),
(14, 16),
(15, 15),
(15, 16),
(16, 15),
(16, 16),
(17, 15),
(17, 16),
(18, 15),
(18, 16),
(19, 15),
(19, 16),
(20, 15),
(20, 16),
(21, 15),
(21, 16),
(22, 15),
(22, 16),
(23, 15),
(23, 16),
(24, 15),
(24, 16),
(25, 15),
(25, 16),
(26, 15),
(26, 16),
(27, 15),
(27, 16),
(28, 15),
(28, 16),
(29, 15),
(29, 16),
(30, 15),
(30, 16),
(31, 6),
(31, 15),
(31, 16),
(32, 15),
(32, 16),
(33, 15),
(33, 16),
(34, 15),
(34, 16),
(35, 15),
(35, 16),
(36, 15),
(36, 16),
(37, 6),
(37, 15),
(37, 16),
(38, 15),
(38, 16),
(39, 15),
(39, 16),
(40, 15),
(40, 16),
(41, 15),
(41, 16),
(42, 15),
(42, 16),
(43, 15),
(43, 16),
(44, 15),
(44, 16),
(45, 15),
(45, 16),
(47, 15),
(47, 16),
(48, 15),
(48, 16),
(49, 6),
(49, 15),
(49, 16),
(50, 15),
(50, 16),
(51, 15),
(51, 16),
(52, 15),
(52, 16),
(53, 15),
(53, 16),
(54, 15),
(54, 16),
(55, 15),
(55, 16),
(56, 15),
(56, 16),
(57, 15),
(57, 16);

-- --------------------------------------------------------

--
-- Structure de la table `offer_station`
--

CREATE TABLE `offer_station` (
  `offer_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `offer_station`
--

INSERT INTO `offer_station` (`offer_id`, `station_id`) VALUES
(1, 1),
(1, 4),
(2, 1),
(2, 4),
(3, 5),
(3, 9),
(3, 12),
(3, 13),
(3, 14),
(3, 20),
(4, 5),
(4, 9),
(4, 12),
(4, 13),
(4, 14),
(4, 20),
(5, 5),
(5, 9),
(5, 12),
(5, 13),
(5, 14),
(5, 20),
(6, 5),
(6, 9),
(6, 12),
(6, 13),
(6, 14),
(6, 20),
(7, 5),
(7, 9),
(7, 12),
(7, 13),
(7, 14),
(7, 20),
(8, 5),
(8, 9),
(8, 12),
(8, 13),
(8, 14),
(8, 20),
(9, 5),
(9, 9),
(9, 12),
(9, 13),
(9, 14),
(9, 20),
(10, 5),
(10, 9),
(10, 12),
(10, 13),
(10, 14),
(10, 20),
(11, 5),
(11, 9),
(11, 12),
(11, 13),
(11, 14),
(11, 20),
(12, 5),
(12, 9),
(12, 12),
(12, 13),
(12, 14),
(12, 20),
(13, 5),
(13, 9),
(13, 12),
(13, 13),
(13, 14),
(13, 20),
(14, 5),
(14, 9),
(14, 12),
(14, 13),
(14, 14),
(14, 20),
(15, 5),
(15, 9),
(15, 12),
(15, 13),
(15, 14),
(15, 20),
(16, 5),
(16, 9),
(16, 12),
(16, 13),
(16, 14),
(16, 20),
(17, 5),
(17, 9),
(17, 12),
(17, 13),
(17, 14),
(17, 20),
(18, 5),
(18, 9),
(18, 12),
(18, 13),
(18, 14),
(18, 20),
(19, 5),
(19, 9),
(19, 12),
(19, 13),
(19, 14),
(19, 20),
(20, 5),
(20, 9),
(20, 12),
(20, 13),
(20, 14),
(20, 20),
(21, 5),
(21, 9),
(21, 12),
(21, 13),
(21, 14),
(21, 20),
(22, 5),
(22, 9),
(22, 12),
(22, 13),
(22, 14),
(22, 20),
(23, 5),
(23, 9),
(23, 12),
(23, 13),
(23, 14),
(23, 20),
(24, 5),
(24, 9),
(24, 12),
(24, 13),
(24, 14),
(24, 20),
(25, 5),
(25, 9),
(25, 12),
(25, 13),
(25, 14),
(25, 20),
(26, 5),
(26, 9),
(26, 12),
(26, 13),
(26, 14),
(26, 20),
(27, 5),
(27, 9),
(27, 12),
(27, 13),
(27, 14),
(27, 20),
(28, 5),
(28, 9),
(28, 12),
(28, 13),
(28, 14),
(28, 20),
(29, 5),
(29, 9),
(29, 12),
(29, 13),
(29, 14),
(29, 20),
(30, 5),
(30, 9),
(30, 12),
(30, 13),
(30, 14),
(30, 20),
(31, 5),
(31, 9),
(31, 12),
(31, 13),
(31, 14),
(31, 20),
(32, 5),
(32, 9),
(32, 12),
(32, 13),
(32, 14),
(32, 20),
(33, 5),
(33, 9),
(33, 12),
(33, 13),
(33, 14),
(33, 20),
(34, 5),
(34, 9),
(34, 12),
(34, 13),
(34, 14),
(34, 20),
(35, 5),
(35, 9),
(35, 12),
(35, 13),
(35, 14),
(35, 20),
(36, 5),
(36, 9),
(36, 12),
(36, 13),
(36, 14),
(36, 20),
(37, 5),
(37, 9),
(37, 12),
(37, 13),
(37, 14),
(37, 20),
(38, 5),
(38, 9),
(38, 12),
(38, 13),
(38, 14),
(38, 20),
(39, 5),
(39, 9),
(39, 12),
(39, 13),
(39, 14),
(39, 20),
(40, 5),
(40, 9),
(40, 12),
(40, 13),
(40, 14),
(41, 5),
(41, 6),
(41, 7),
(41, 8),
(41, 9),
(41, 10),
(41, 11),
(41, 12),
(41, 13),
(41, 14),
(41, 15),
(41, 16),
(41, 17),
(41, 18),
(42, 5),
(42, 9),
(42, 12),
(42, 13),
(42, 14),
(43, 5),
(43, 9),
(43, 12),
(43, 13),
(43, 14),
(44, 5),
(44, 9),
(44, 12),
(44, 13),
(44, 14),
(45, 5),
(45, 9),
(45, 12),
(45, 13),
(45, 14),
(46, 5),
(46, 9),
(46, 12),
(46, 13),
(46, 14),
(46, 20),
(47, 5),
(47, 9),
(47, 12),
(47, 13),
(47, 14),
(47, 20),
(48, 5),
(48, 9),
(48, 12),
(48, 13),
(48, 14),
(48, 20),
(49, 5),
(49, 9),
(49, 12),
(49, 13),
(49, 14),
(49, 20),
(50, 5),
(50, 9),
(50, 12),
(50, 13),
(50, 14),
(50, 20),
(51, 5),
(51, 9),
(51, 12),
(51, 13),
(51, 14),
(51, 20),
(52, 5),
(52, 9),
(52, 12),
(52, 13),
(52, 14),
(52, 20),
(53, 5),
(53, 9),
(53, 12),
(53, 13),
(53, 14),
(53, 20),
(54, 5),
(54, 9),
(54, 12),
(54, 13),
(54, 14),
(54, 20),
(55, 5),
(55, 9),
(55, 12),
(55, 13),
(55, 14),
(55, 20),
(56, 5),
(56, 9),
(56, 12),
(56, 13),
(56, 14),
(56, 20),
(57, 5),
(57, 9),
(57, 12),
(57, 13),
(57, 14),
(57, 20);

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

CREATE TABLE `prestation` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `station_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `prestation`
--

INSERT INTO `prestation` (`id`, `name`, `price`, `is_active`, `is_deleted`, `icon`, `station_id`) VALUES
(1, 'Changement filtre à air', 2, 1, 0, 'frame7330-6134dcb377a57.png', NULL),
(2, 'Changement filtre carburant', 3, 1, 0, 'frame7335-6134e0e0373e3.png', NULL),
(3, 'Changement filtre habitacle', 5, 1, 0, 'frame7336-6134e0f68c895.png', NULL),
(4, 'Vidange', 2, 1, 1, 'lavage-612ce24fbd9d0.png', NULL),
(6, 'Lavage extérieur', 0, 1, 0, 'frame7331-6134dc9fa730b.png', NULL),
(7, 'Lavage Automatique', 0, 1, 1, '2-612dfd0fe97c3.png', NULL),
(8, 'Recharge climatiseur', 0, 1, 0, 'clim3-6140cff4da85d.png', NULL),
(9, 'Café', 0, 1, 1, '4-612dfe0d5ae4c.png', NULL),
(10, 'Boutique et Shop', 0, 1, 1, '5-612dfe4a64b3a.png', NULL),
(11, 'Changement pneumatique', 0, 1, 0, 'frame7327-6134dc7f1d790.png', NULL),
(12, 'LAVAGE', 0, 1, 1, 'pck_total_quartzineomc35w30_lky_201912_5l-612e5b0b49c18.png', NULL),
(13, 'Changement bougies', 0, 1, 0, 'frame7334-6134e0d3ac40d.png', NULL),
(14, 'Lavage intérieur', 0, 1, 0, 'frame7321-6135ef7030752.png', NULL),
(15, 'Main d\'œuvre vidange', 0, 1, 0, 'maindoeuvre2-6140ceffe2f64.png', NULL),
(16, 'Changement filtre à huile', 0, 1, 0, 'filtrecar-614b90ad159ac.png', NULL),
(19, 'Lavage complet', 0, 1, 0, 'frame7328-61368aab15d99.png', NULL),
(20, 'Changement batterie', 0, 1, 0, 'frame7324-61368ac449412.png', NULL),
(21, 'Changement essuie-glace', 0, 1, 0, 'frame7325-61368b4e1d64c.png', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `prestation_station`
--

CREATE TABLE `prestation_station` (
  `id` int(11) NOT NULL,
  `station_id` int(11) DEFAULT NULL,
  `prestation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `prestation_station`
--

INSERT INTO `prestation_station` (`id`, `station_id`, `prestation_id`) VALUES
(4, 4, 4),
(5, 4, 3),
(6, 18, 7),
(8, 18, 1),
(9, 18, 6),
(11, 18, 10),
(12, 18, 9),
(13, 18, 8),
(14, 15, 12),
(15, 15, 16),
(16, 15, 15),
(17, 15, 14),
(18, 15, 13),
(19, 4, 16),
(21, 4, 15),
(22, 4, 14),
(24, 18, 16),
(27, 18, 11),
(32, 19, 1),
(33, 19, 16),
(34, 19, 3),
(35, 19, 19),
(36, 19, 6),
(38, 14, 1),
(39, 14, 16),
(40, 14, 3),
(41, 14, 19),
(42, 14, 6),
(43, 13, 1),
(44, 13, 16),
(45, 13, 3),
(46, 13, 6),
(48, 12, 1),
(49, 12, 16),
(50, 12, 3),
(52, 12, 6),
(54, 9, 1),
(55, 9, 16),
(56, 9, 3),
(58, 9, 6),
(59, 5, 1),
(60, 5, 16),
(61, 5, 3),
(63, 5, 6),
(64, 13, 8),
(65, 12, 8),
(66, 9, 8),
(67, 19, 2),
(68, 14, 2),
(69, 13, 2),
(70, 12, 2),
(71, 9, 2),
(72, 5, 2),
(73, 20, 20),
(74, 20, 13),
(75, 20, 21),
(76, 20, 1),
(77, 20, 16),
(78, 20, 2),
(79, 20, 11),
(80, 20, 19),
(81, 13, 21);

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `price_promo` double DEFAULT NULL,
  `is_promo` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `id_lubricant` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motorisation` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `preconisation` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `alternative` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `price`, `price_promo`, `is_promo`, `is_active`, `is_deleted`, `id_lubricant`, `description`, `quantity`, `file`, `motorisation`, `preconisation`, `alternative`) VALUES
(3, 'QUARTZ 9000 5W40+ GLACELF AUTO SUPRA', 'vector6-60f0120de0390.png', 4, 2.8, 1, 1, 1, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.', 0, NULL, NULL, NULL, NULL),
(4, 'TOTAL Quartz 5000 20W50', 'vector5-60f011f0480dd.png', 2.4, 1.2, 0, 1, 1, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.', 0, NULL, NULL, NULL, NULL),
(5, 'TOTAL Quartz 5000 20W50', 'vector4-60f011d66b1f6.png', 2.9, 2.8, 1, 1, 1, 2395, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.', 0, NULL, NULL, NULL, NULL),
(6, 'EVOLUTION 900 FT 5W-40', 'vector3-60f011bd37011.png', 10, 10, 1, 1, 1, 2490, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Vestibulum ultricies aliquam convallis.', 5, NULL, NULL, NULL, NULL),
(7, 'Quartz 9000 5W-40 en bidon de 4 Litres', 'quartz90004lv1-614a3040a448e.png', 94.9, 85.41, 1, 1, 0, 2403, 'TOTAL QUARTZ 9000 ESSENCE 5W-40 satisfait aux conditions d\'utilisation les plus difficiles (autoroute, circulation urbaine intense...), en toutes saisons.\r\nCe produit est parfaitement adapté aux moteurs équipés de pots catalytiques et convient à tout moteur utilisant du carburant sans plomb ou du GPL. Il convient également à tous les véhicules équipés d’un moteur Essence sans FAP, commercialisés après les années 2000.\r\n\r\nLes plus produit :\r\n- Intervalle de vidange allongé: La résistance à l’oxydation de ce lubrifiant permet de satisfaire aux intervalles de vidange allongés.\r\n- Protection des pièces mécaniques: TOTAL QUARTZ 9000 ESSENCE 5W-40 protège les pièces mécaniques et préserve entièrement la puissance du moteur. Elle contribue ainsi à éviter son vieillissement et à conserver sa puissance.\r\n- Démarrages à froid facilités: La fluidité à basse température de cette huile facilite les démarrages à froid.\r\n- Propreté du moteur: Additivation détergente et dispersive spécifique.', 1, 'tds_total_quartz9000essence5w40_tr9_202009_fr-614b3f6baae8e.pdf', 'a:1:{i:0;s:1:\"0\";}', 'a:1:{i:0;s:1:\"0\";}', 'a:0:{}'),
(8, 'Quartz 7000 10W-40 en bidon de 4 Litres', 'quartz7000v3-614a0b33e03a9.png', 73.2, 58.56, 1, 1, 0, 2401, 'TOTAL QUARTZ 7000 ESSENCE 10W-40 a été développée pour tous les moteurs à essence de véhicules de tourisme et utilitaires légers. Ce lubrifiant convient tout particulièrement aux motorisations turbo-compressées et multisoupapes.\r\n\r\nTOTAL QUARTZ 7000 ESSENCE 10W-40 est parfaitement adaptée à une conduite normale sur route (peu de ville). Ce lubrifiant est adapté aux moteurs équipés de pots catalytiques et convient également à tous les véhicules équipés d’un moteur Essence sans FAP, commercialisés avant les années 2000.\r\n\r\nTOTAL QUARTZ ESSENCE 10W-40 est une référence incontournable et universelle en terme d’application\r\net de compatibilité moteurs.\r\n\r\nLes plus produit :\r\n- Protection du moteur: La nature des bases lubrifiantes utilisées, alliée à une sélection d’additifs spécialement mise au point pour ce produit, confère à cette huile d’excellentes propriétés de protection du moteur contre l’oxydation notamment.\r\n- Protection des pièces les plus sensibles: Protection dans la durée des pièces les plus sensibles du moteur contre l’usure (usure distribution, usure segment, piston, chemise).\r\n- Propreté : Ce lubrifiant garantit une bonne propreté moteur en conduisant les particules (poussières, particules) vers le filtre à huile et grâce à d’efficaces propriétés de dispersion.', 1, 'tds_total_quartzessence700010w40_tr6_202101_fr-613649595ecb7.pdf', 'a:1:{i:0;s:1:\"0\";}', 'a:0:{}', 'a:1:{i:0;s:1:\"0\";}'),
(9, 'QUARTZ INEO LONGLIFE 5W-30', '3-612e284e8e5b7.png', 141.6, 1, 0, 1, 1, 2407, 'TOTAL QUARTZ INEO LONG LIFE 5W-30 est une huile de technologie de synthèse Low SAPS spécialement formulée pour satisfaire les exigences techniques des véhicules du groupe Volkswagen et Porsche.\r\n\r\nLes plus produit :\r\n- Excellente résistance à l’oxydation', 1, 'tds_total_quartzineolonglife5w30_6e5_202104_fr-61364916100c7.pdf', 'a:3:{i:0;s:1:\"0\";i:1;s:1:\"2\";i:2;s:1:\"1\";}', 'a:1:{i:0;s:1:\"2\";}', 'a:0:{}'),
(10, 'Quartz 7000 10W-40 en bidon de 5 Litres', 'quartz70005lv2-614a0c695d340.png', 82, 65.6, 1, 1, 0, 2394, 'TOTAL QUARTZ 7000 DIESEL 10W-40 est parfaitement adaptée à tous les types de parcours (autoroute, route, ville) et aux conditions les plus extrêmes. La nature des bases lubrifiantes utilisées, alliée à une sélection d’additifs spécialement mise au point pour ce produit, confère à cette huile d’excellentes propriétés de protection du moteur contre l’oxydation notamment. Protection dans la durée des pièces les plus sensibles du moteur contre l’usure (usure distribution, usure segment, piston, chemise). Ce lubrifiant garantit une bonne propreté moteur en conduisant les particules (poussières, particules) vers le filtre à huile et grâce à d’efficaces propriétés de dispersion.\r\n\r\nLes plus produit :\r\n-Protection du moteur: La nature des bases lubrifiantes utilisées, alliée à une sélection d’additifs spécialement mise au point pour ce produit, confère à cette huile d’excellentes propriétés de protection du moteur contre l’oxydation notamment.\r\n- Protection des pièces les plus sensibles: Protection dans la durée des pièces les plus sensibles du moteur contre l’usure (usure distribution, usure segment, piston, chemise).\r\n- Propreté: Ce lubrifiant garantit une bonne propreté moteur en conduisant les particules (poussières, particules)\r\nvers le filtre à huile et grâce à d’efficaces propriétés de dispersion.\r\n- TOTAL QUARTZ DIESEL 10W-40 est une référence incontournable et universelle en terme d’application et de compatibilité moteurs', 1, 'tds_total_quartzdiesel700010w40_tr5_202101_fr-614b3e18dd43a.pdf', 'a:1:{i:0;s:1:\"1\";}', 'a:0:{}', 'a:1:{i:0;s:1:\"1\";}'),
(11, 'Quartz 4X4 15W-50 en bidon de 5 Litres', 'quartz4x4v2-614a148277c91.png', 98.2, 88.38, 1, 1, 0, 2393, 'Lubrifiant développé spécialement pour les véhicules 4x4, SUV et Pick-ups de dernière génération, à motorisation essence ou diesel.\r\n• Quartz 4x4 15W40 protège le moteur dans les conditions d’utilisation les plus extrêmes : environnement de poussière intense, désert, piste, route, circulation\r\nurbaine (stop and go)….\r\n• Grâce aux propriétés anti-usure exceptionnelles et à la résistance aux très hautes températures de la Quartz 4X4 15W40, le moteur est plus résistant et plus\r\nperformant. \r\n\r\nAvantages clients:\r\nQuartz 4x4 optimise les performances moteur en mode rapport court et offre au\r\nconducteur plus de confort de conduite.\r\n• Exceptionnelles propriétés anti-usure et anticorrosion.\r\n• Excellente stabilité de viscosité.\r\n• Très haut niveau de détergence et de dispersivité (permet de limiter l’épaississement\r\ndû aux suies).\r\n• Prolonge la durée de vie du moteur (Intervalle de vidange allongé)', 1, 'quartz_4x4-613768a7386bc.pdf', 'a:2:{i:0;s:1:\"0\";i:1;s:1:\"1\";}', 'a:0:{}', 'a:0:{}'),
(12, 'Quartz 9000 5W-40 en bidon de 5 Litres', 'q9000v7-6144a6caed36e.png', 113.8, 102.42, 1, 1, 0, 2393, 'TOTAL QUARTZ 9000 DIESEL 5W-40 satisfait aux conditions d\'utilisation les plus difficiles (autoroute, circulation urbaine intense...), en toutes saisons. Ce produit est parfaitement adapté aux motorisations Diesel non équipées de filtres à particules. Cette huile offre une excellente protection contre l’usure et l’encrassement grâce à son action de lubrification dès le démarrage et à son additivation détergente et dispersive. La fluidité à basse température de cette huile facilite les démarrages à froid, et offre également une bonne résistance aux températures plus élevées.\r\n\r\nLes plus produit :\r\n- Intervalle de vidange allongé: La résistance à l’oxydation de ce lubrifiant permet de satisfaire aux intervalles de vidange allongés.\r\n- Protection des pièces mécaniques: TOTAL QUARTZ 9000 DIESEL 5W-40 protège les pièces mécaniques et préserve entièrement la puissance du moteur. Elle contribue ainsi à éviter son vieillissement et à conserver sa puissance.\r\n- Démarrages à froid facilités: La fluidité à basse température de cette huile facilite les démarrages à froid.\r\n- Propreté du moteur: Additivation détergente et dispersive spécifique.', 1, 'tds_total_quartz9000diesel5w40_tr8_202009_fr-614b3ef522b79.pdf', 'a:1:{i:0;s:1:\"1\";}', 'a:1:{i:0;s:1:\"1\";}', 'a:0:{}'),
(13, 'Quartz Inéo MC3 5W-30 en bidon de 5 Litres', '2-612e2807926c5.png', 138.2, 124.38, 1, 1, 0, 2393, 'TOTAL QUARTZ INEO MC3 5W-30 est une huile moteur Low SAPS de technologie de synthèse spécialement formulée pour satisfaire aux exigences techniques des véhicules des groupes BMW, Mercedes-Benz, Volkswagen et Hyundai-Kia.\r\nCe lubrifiant moteur de technologie de synthèse anti-pollution offre la meilleure protection moteur contre les phénomènes d’usure et d’encrassement.\r\n\r\nLes plus produit :\r\n- Technologie Fuel Economy \r\n- Formulation « Low SAPS »\r\n- Convient aux conditions de conduites les plus sévères \r\n- Propreté du moteur', 1, 'tds_total_quartzineomc35w30_lky_202011_fr-61364a098385c.pdf', 'a:3:{i:0;s:1:\"2\";i:1;s:1:\"0\";i:2;s:1:\"1\";}', 'a:0:{}', 'a:1:{i:0;s:1:\"2\";}'),
(14, 'Quartz Inéo First 0W-30 en bidon de 5 Litres', '1-612e2796073ca.png', 122, 109.8, 1, 1, 0, 2393, 'TOTAL QUARTZ INEO FIRST 0W-30 est une huile de technologie de synthèse offrant le meilleur niveau de protection de sa catégorie contre l’usure et l’encrassement dès le démarrage. \r\nCette huile est particulièrement adaptée aux motorisations récentes équipées de technologies PSA e-HDI, des systèmes Stop & Start et de motorisations hybrides, qui exigent un lubrifiant de toute nouvelle génération.\r\nElle satisfait les homologations constructeurs PEUGEOT CITROEN , JAGUAR LAND ROVER ainsi que les exigences techniques de TOYOTA.\r\n\r\nLes plus produit :\r\n- Technologie Fuel Economy\r\n- Technologie Low-Saps​\r\n- Protection et propreté du moteur\r\n- Démarrage à froid facilité', 1, 'tds_total_quartzineofirst0w30_7bj_202009_fr-61388e358b9b2.pdf', 'a:3:{i:1;s:1:\"2\";i:2;s:1:\"0\";i:3;s:1:\"1\";}', 'a:0:{}', 'a:0:{}'),
(15, 'EVOLUTION 700 ST 10W-40', 'huilemoteurhuilemoteurelfevolution700sti10w40-6127c18cae8a7.png', 1, 1, 0, 1, 1, 2393, 'ELF EVOLUTION 700 ST 10W-40 est un lubrifiant de semi-synthèse très hautes performances, spécialement développé pour les moteurs essence et diesel.\r\nIl est optimisé pour répondre aux exigences accrues des technologies injection direct et est particulièrem', 1, 'tds_elf_evolution700st10w40_dfd_202101_fr-6136469c63152.pdf', 'a:2:{i:0;s:1:\"0\";i:1;s:1:\"1\";}', 'a:0:{}', 'a:0:{}'),
(16, 'EVO ELF 700 10W40 4L', '1-6127c20c30d55.png', 1, 1, 0, 1, 1, 2394, 'ELF EVOLUTION 700 ST 10W-40 est un lubrifiant de semi-synthèse très hautes performances, spécialement développé pour les moteurs essence et diesel.\r\nIl est optimisé pour répondre aux exigences accrues des technologies injection direct et est particulièrem', 1, NULL, NULL, NULL, NULL),
(17, 'QUARTZ INEO FIRST 0W30 4L', '22178966127c07e2a674-612e124377dae.png', 70, 60, 1, 1, 1, 2405, 'Cette est composée de lubrifiants de technologie de synthèse pour lesquels les formules sont optimisées afin d’en abaisser les teneurs SAPS*. Huile spéciale économies de carburant de deuxième génération, notamment applicable à tous les moteurs', 0, 'tds_total_quartzineofirst0w30_7bj_202009_fr-61388df11026d.pdf', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"0\";}', 'a:0:{}', 'a:0:{}'),
(18, 'EVOLUTION 700 10W-40 5L', 'vector1-614222dbe0bbd.png', 1, NULL, 0, 1, 1, 2394, NULL, 0, NULL, 'a:0:{}', 'a:0:{}', 'a:0:{}'),
(19, 'Quartz Inéo Long Life 5W-30 en bidon de 5 Litres', 'ineoll-61430ef90399e.png', 143, 128.7, 1, 1, 0, 2407, 'TOTAL QUARTZ INEO LONG LIFE 5W-30 est une huile de technologie de synthèse Low SAPS spécialement formulée pour satisfaire les exigences techniques des véhicules du groupe Volkswagen et Porsche.\r\n\r\nLes plus produit :\r\n- Excellente résistance à l’oxydation\r\n- Satisfait les plans d’entretien constructeurs les plus exigeants\r\n- Exceptionnelle résistance à l’oxydation\r\n- Respect de l’environnement\r\n- Protection et propreté optimale du moteur', 1, 'tds_total_quartzineolonglife5w30_6e5_202104_fr-6142fd7723ccc.pdf', 'a:3:{i:0;s:1:\"0\";i:1;s:1:\"1\";i:2;s:1:\"2\";}', 'a:1:{i:0;s:1:\"2\";}', 'a:0:{}');

-- --------------------------------------------------------

--
-- Structure de la table `product_station`
--

CREATE TABLE `product_station` (
  `product_id` int(11) NOT NULL,
  `station_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product_station`
--

INSERT INTO `product_station` (`product_id`, `station_id`) VALUES
(1, 1),
(2, 4),
(3, 4),
(4, 4),
(5, 1),
(5, 4),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(5, 10),
(5, 11),
(5, 12),
(5, 13),
(5, 14),
(5, 15),
(5, 16),
(5, 17),
(5, 18),
(6, 1),
(6, 4),
(6, 5),
(6, 7),
(6, 8),
(6, 9),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(7, 5),
(7, 9),
(7, 12),
(7, 13),
(7, 14),
(7, 20),
(8, 5),
(8, 9),
(8, 12),
(8, 13),
(8, 14),
(9, 5),
(9, 9),
(9, 12),
(9, 13),
(9, 14),
(10, 5),
(10, 9),
(10, 12),
(10, 13),
(10, 14),
(10, 20),
(11, 5),
(11, 9),
(11, 12),
(11, 13),
(11, 14),
(11, 20),
(12, 5),
(12, 9),
(12, 12),
(12, 13),
(12, 14),
(12, 20),
(13, 5),
(13, 9),
(13, 12),
(13, 13),
(13, 14),
(13, 20),
(14, 5),
(14, 9),
(14, 12),
(14, 13),
(14, 14),
(14, 20),
(15, 5),
(15, 9),
(15, 12),
(15, 13),
(15, 14),
(16, 1),
(16, 4),
(16, 5),
(16, 6),
(16, 7),
(16, 8),
(16, 9),
(16, 10),
(16, 11),
(16, 12),
(16, 13),
(16, 14),
(16, 15),
(16, 16),
(16, 17),
(16, 18),
(17, 1),
(17, 4),
(17, 5),
(17, 6),
(17, 7),
(17, 8),
(17, 9),
(17, 10),
(17, 11),
(17, 12),
(17, 13),
(17, 14),
(17, 15),
(17, 16),
(17, 17),
(17, 18),
(18, 5),
(18, 9),
(18, 12),
(18, 13),
(18, 14),
(18, 19),
(19, 5),
(19, 9),
(19, 12),
(19, 13),
(19, 14),
(19, 20);

-- --------------------------------------------------------

--
-- Structure de la table `reset_password_request`
--

CREATE TABLE `reset_password_request` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `selector` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hashed_token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `requested_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `expires_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `reset_password_request`
--

INSERT INTO `reset_password_request` (`id`, `user_id`, `selector`, `hashed_token`, `requested_at`, `expires_at`) VALUES
(20, 7, 'YMMvTxKohJKCc11ivtmP', 'DN43ZWucODVTLqejykDCPjDFMA0lDf3z/ZTTW969PWM=', '2021-09-20 08:44:06', '2021-09-20 09:44:06'),
(23, 7, 'PETgaBHinuhSY60zIgDV', 'CXKRyzkhxHGT7Zmj9CQsHUQQ9MX+S52obErRdnsxkfE=', '2021-09-20 10:58:47', '2021-09-20 11:58:47'),
(24, 24, 'KtcgOBb7iqawBFuodOok', 'oH8MNim4gXDthCRrG9BQ74Vv9GwQ8KnltJh6ky4So7Y=', '2021-09-20 11:26:52', '2021-09-20 12:26:52'),
(25, 7, 'vYYyD6J1JQEQQPCDxvI5', 'K3UBlRJNbgJdto7wYRUfWXnRA8RzyNJYbBjJvURbBHo=', '2021-09-21 10:33:08', '2021-09-21 11:33:08');

-- --------------------------------------------------------

--
-- Structure de la table `search`
--

CREATE TABLE `search` (
  `id` int(11) NOT NULL,
  `marque` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modele` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carburant` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `search`
--

INSERT INTO `search` (`id`, `marque`, `modele`, `year`, `carburant`, `create_at`) VALUES
(11, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(12, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(13, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(14, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(15, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(16, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(17, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(18, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(19, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(20, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Diesel', NULL),
(21, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(22, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(23, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(24, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(25, 'PEUGEOT', '508 1.6 HDi 16V DPF, e-HDi 115', '2011', 'Hybride', NULL),
(26, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(27, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(28, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(29, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(30, 'FORD (EU)', '(Grand)  C-Max 1.0 EcoBoost', '2012', 'Essence', NULL),
(31, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(32, 'DACIA', 'Dokker 1.2 Tce', '2013', 'Essence', NULL),
(33, 'DACIA', 'Dokker 1.2 Tce', '2013', 'Essence', NULL),
(34, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(35, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(36, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(37, 'CHEVROLET (EU)', '1210, 1310, 1410, 1610, Denem', '1978', 'Essence', NULL),
(38, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(39, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(40, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Diesel', NULL),
(41, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(42, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(43, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(44, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(45, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(46, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(47, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(48, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(49, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Hybride', NULL),
(50, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Diesel', NULL),
(51, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(52, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(53, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(54, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(55, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(56, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(57, 'CHEVROLET (EU)', '1210, 1310, 1410, 1610, Denem', '1978', 'Diesel', NULL),
(58, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(59, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(60, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(61, 'CHERY', 'A3, E3, A19, Bonus 1.5', '2013', 'Essence', NULL),
(62, 'CHERY', 'A3, E3, A19, Bonus 1.5', '2013', 'Essence', NULL),
(63, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(64, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(65, 'CHEVROLET (EU)', '1210, 1310, 1410, 1610, Denem', '1977', 'Essence', NULL),
(66, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(67, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(68, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Diesel', NULL),
(69, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2010', 'Diesel', NULL),
(70, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(71, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(72, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', NULL),
(73, 'DACIA', 'Dokker 1.2 Tce', '2013', 'Essence', NULL),
(74, 'DACIA', 'Dokker 1.5 dCi', '2013', 'Essence', NULL),
(75, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(76, 'CHERY', 'A3, A19, Bonus 1.5', '2009', 'Essence', NULL),
(77, 'DACIA', 'Dokker 1.5 dCi', '2013', 'Essence', NULL),
(78, 'CHEVROLET (EU)', '1210, 1310, 1410, 1610, Denem', '1977', 'Essence', NULL),
(79, 'DACIA', 'Dokker 1.2 Tce', '2013', 'Essence', NULL),
(80, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(81, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Hybride', NULL),
(82, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(83, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(84, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(85, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Diesel', NULL),
(86, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(87, 'DACIA', 'Duster 1.2 Tce, 1.6, 1.6 16V, AWD', '2010', 'Essence', NULL),
(88, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(89, 'BMW', '116i, 118i, 120i (E81/E82/E87/E88)', '2004', 'Essence', NULL),
(90, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(91, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(92, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(93, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(94, 'DACIA', 'Dokker Van  1.2 TCe', '2012', 'Essence', NULL),
(95, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2010', 'Diesel', NULL),
(96, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2010', 'Essence', NULL),
(97, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(98, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(99, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(100, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(101, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(102, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(103, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(104, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(105, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(106, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(107, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(108, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(109, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2013', 'Essence', NULL),
(110, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(111, 'Autre', 'Autre', 'Autre', 'Essence', NULL),
(112, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(113, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(114, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(115, 'CHEVROLET (EU)', 'Aveo 1.4', '2009', 'Diesel avec FAP', NULL),
(116, 'DACIA', 'Dokker Van  1.2 TCe', '2012', 'Essence', NULL),
(117, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(118, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(119, 'BMW', '116i, 118i, 120i (E81/E82/E87/E88)', '2004', 'Essence', NULL),
(120, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(121, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(122, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(123, 'BMW', '118d, 120d (E87/E82/E81/E88)', '2005', 'Diesel', NULL),
(124, 'DACIA', 'Dokker 1.5 dCi', '2013', 'Diesel', NULL),
(125, 'CHERY', 'A3, A19, Bonus 1.5', '2008', 'Essence', NULL),
(126, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(127, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(128, 'BMW', '116i, 118i, 120i (E81/E82/E87/E88)', '2004', 'Essence', NULL),
(129, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Diesel', NULL),
(130, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(131, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(132, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(133, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(134, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(135, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(136, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(137, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(138, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(139, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(140, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(141, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(142, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(143, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(144, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(145, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(146, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(147, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(148, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(149, 'DACIA', 'Dokker 1.6 MPI, 1.6 LPG', '2014', 'Diesel', NULL),
(150, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(151, 'ALFA ROMEO', 'GIULIETTA', '2001', 'Essence', NULL),
(152, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(153, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(154, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(155, 'DACIA', 'Dokker 1.5 dCi', '2015', 'Essence', NULL),
(156, 'PEUGEOT', '508 1.6 BlueHDi 120', '2015', 'Diesel', NULL),
(157, 'RENAULT', 'Kadjar 1.2 TCe', '2015', 'Essence', NULL),
(158, 'BMW', '114d, 116d, 118d (F20)', '2012', 'Essence', NULL),
(159, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2012', 'Essence', NULL),
(160, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(161, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(162, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(163, 'FORD (EU)', 'Fiesta 1.1, 1.3, 1.25, 1.4, 1.6 16V', '1999', 'Essence', NULL),
(164, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(165, 'KIA', 'Cadenza 3.5', '2015', 'Diesel', NULL),
(166, 'DACIA', 'Dokker 1.6 SCe, 1.6 SCe LPG', '2018', 'Diesel', NULL),
(167, 'FORD (EU)', 'Fiesta 1.1, 1.3, 1.25, 1.4, 1.6 16V', '2002', 'Essence', NULL),
(168, 'FORD (EU)', '(Grand) C-Max 1.6 TDCi', '2013', 'Diesel', NULL),
(169, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(170, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(171, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(172, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(173, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(174, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', NULL),
(175, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(176, 'CHERY', 'A3, E3, A19, Bonus 1.5', '2015', 'Diesel', NULL),
(177, 'FORD (EU)', 'Fiesta 1.25 16V', '2013', 'Essence', NULL),
(178, 'CHEVROLET (EU)', 'Trax 1.6', '2014', 'Diesel', NULL),
(179, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(180, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(181, 'DACIA', 'Dokker 1.6 SCe, 1.6 SCe LPG', '2018', 'Essence', NULL),
(182, 'SEAT', 'Ibiza, Cordoba 1.4 TDi DPF', '2007', 'Essence', NULL),
(183, 'SEAT', 'Ibiza, Cordoba 1.4 TDi DPF', '2007', 'Essence', NULL),
(184, 'SEAT', 'Le', '1998', 'Essence', NULL),
(185, 'SEAT', 'Le', '2005', 'Essence', NULL),
(186, 'SEAT', 'Le', '2000', 'Essence', NULL),
(187, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(188, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(189, 'RENAULT', '(Grand)  Espace IV  2.0 16V, 3.5 V6 24V', '2006', 'Essence', NULL),
(190, 'RENAULT', 'Kadjar 1.2 TCe', '2017', 'Essence', NULL),
(191, 'RENAULT', 'Kangoo 1.2 (16V), 1.6 16V, 4x4', '2008', 'Essence', NULL),
(192, 'RENAULT', 'Kadjar 1.5 dCi DPF', '2019', 'Essence', NULL),
(193, 'PEUGEOT', '508 1.6 BlueHDi 120', '2018', 'Essence', NULL),
(194, 'BMW', '118d,  120d,  DPF (E87/E82/E81/E88)', '2009', 'Essence', NULL),
(195, 'PEUGEOT', '508  1.6i 16V VTi, 1.6i 16V THP, THP 165', '2014', 'Essence', NULL),
(196, 'FORD (EU)', 'Fiesta 1.25 16V', '2015', 'Essence', NULL),
(197, 'PEUGEOT', '508  1.6i 16V VTi, 1.6i 16V THP, THP 165', '2014', 'Essence', NULL),
(198, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(199, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', NULL),
(200, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', NULL),
(201, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', NULL),
(202, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', NULL),
(203, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', NULL),
(204, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-16 08:20:21'),
(205, 'PEUGEOT', '508  1.6i 16V VTi, 1.6i 16V THP, THP 165', '2013', 'Essence', '2021-09-16 08:31:19'),
(206, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 09:19:46'),
(207, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 09:40:28'),
(208, 'BMW', '114d, 116d, 118d (F20)', '2014', 'Essence', '2021-09-16 09:44:57'),
(209, 'BMW', '114i, 116i, 118i, 125i, xDrive (F20)', '2011', 'Essence', '2021-09-16 09:48:03'),
(210, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-16 11:09:10'),
(211, 'PEUGEOT', '508  1.6i 16V VTi, 1.6i 16V THP, THP 165', '2013', 'Essence', '2021-09-16 11:14:47'),
(212, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 11:28:26'),
(213, 'FIAT', 'Barchetta 1.8 16V', '2003', 'Diesel', '2021-09-16 11:29:05'),
(214, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 11:32:37'),
(215, 'PEUGEOT', '1007 1.4 (16V), 1.6 16V', '2005', 'Essence', '2021-09-16 11:55:06'),
(216, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-16 11:55:26'),
(217, 'PEUGEOT', '407 1.6 HDi DPF, 2.2 HDi DPF', '2007', 'Essence', '2021-09-16 12:02:54'),
(218, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-16 12:55:25'),
(219, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2016', 'Essence', '2021-09-16 12:55:32'),
(220, 'VOLKSWAGEN (VW)', 'Golf VII  2.0 TDI, 4Motion', '2016', 'Essence', '2021-09-16 12:56:54'),
(221, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-16 12:56:57'),
(222, 'VOLKSWAGEN (VW)', 'Golf VII  1.6  TDI 4Motion (77kW, 81kW)', '2016', 'Essence', '2021-09-16 12:59:40'),
(223, 'RENAULT', 'Clio III  1.2 16V, 1.4 16V, 1.6 16V', '2009', 'Essence', '2021-09-16 14:48:07'),
(224, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-16 14:59:57'),
(225, 'PEUGEOT', '508  1.6i 16V VTi, 1.6i 16V THP, THP 165', '2011', 'Essence', '2021-09-17 07:29:09'),
(226, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 07:58:25'),
(227, 'LADA', '2105', '2000', 'Essence', '2021-09-17 08:29:29'),
(228, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-17 09:23:39'),
(229, 'VOLKSWAGEN (VW)', '(103kW, 125kW), 4Motion', '2005', 'Essence', '2021-09-17 09:23:56'),
(230, 'VOLKSWAGEN (VW)', 'Golf   V,    Golf   Plus,   CrossGolf,  Jetta   1.9   TDi   (P-D),2SDi (P-D)', '2003', 'Essence', '2021-09-17 09:24:20'),
(231, 'VOLKSWAGEN (VW)', 'Golf  V,   Golf  Plus, CrossGolf, Jetta  1.4  (FSi), 1.4  16V,  1.6', '2004', 'Diesel avec FAP', '2021-09-17 09:41:37'),
(232, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 10:02:47'),
(233, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 10:04:18'),
(234, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', '2021-09-17 10:04:36'),
(235, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Diesel', '2021-09-17 10:05:05'),
(236, 'KIA', 'Carens 1.6 CVVT', '2009', 'Essence', '2021-09-17 10:15:45'),
(237, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 10:18:14'),
(238, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 10:31:21'),
(239, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 10:40:42'),
(240, 'BMW', '114d, 116d, 118d (F20)', '2011', 'Essence', '2021-09-17 11:13:39'),
(241, 'AUDI', 'A1 1.2 TDI', '2001', 'Essence', '2021-09-17 11:17:35'),
(242, 'AUDI', 'A2 1.4 TDI (P-D)', '2003', 'Essence', '2021-09-17 12:57:43'),
(243, 'BMW', '116i, 118i, 120i (E81/E82/E87/E88)', '2007', 'Essence', '2021-09-17 13:11:51'),
(244, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-17 13:37:34'),
(245, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-17 15:10:07'),
(246, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-17 15:14:32'),
(247, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-17 15:22:20'),
(248, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2020', 'Diesel', '2021-09-17 15:22:37'),
(249, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-17 15:24:12'),
(250, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-17 15:26:54'),
(251, 'VOLKSWAGEN (VW)', 'Caddy 1.4, 1.6, 2.0, Ecofuel (CNG)', '2004', 'Essence', '2021-09-17 15:27:34'),
(252, 'VOLKSWAGEN (VW)', '(103kW, 125kW), 4Motion', '2005', 'Essence', '2021-09-17 15:36:20'),
(253, 'BRILLIANCE', 'Galena', '2001', 'Diesel', '2021-09-18 17:42:20'),
(254, 'BRILLIANCE', 'FSV', '2003', 'Essence', '2021-09-18 20:11:42'),
(255, 'AUDI', 'A2 1.2 TDI', '2003', 'Diesel', '2021-09-18 20:24:31'),
(256, 'SEAT', 'Ibiza 1.0, 1.3, 1.4, 1.6, 1.8, 2.0, 16V', '2002', 'Essence', '2021-09-19 18:41:07'),
(257, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Diesel', '2021-09-19 18:42:44'),
(258, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-19 22:40:42'),
(259, 'BRILLIANCE', 'FSV', '2004', 'Diesel avec FAP', '2021-09-20 07:14:49'),
(260, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-20 08:35:22'),
(261, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-20 09:08:35'),
(262, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-20 09:13:26'),
(263, 'PEUGEOT', '3008  2.0  HDi DPF, HYbrid4 2.0  HDi DPF', '2018', 'Diesel avec FAP', '2021-09-20 10:33:18'),
(264, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-20 12:23:47'),
(265, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-20 13:28:38'),
(266, 'CITROEN', 'Berlingo 1.1, 1.4, 1.4i', '2001', 'Diesel', '2021-09-20 14:50:03'),
(267, 'AUDI', 'A2 1.2 TDI', '2001', 'Diesel', '2021-09-20 18:54:58'),
(268, 'AUDI', 'A1 1.2 TDI', '2000', 'Essence', '2021-09-20 19:15:44'),
(269, 'FORD', '(Grand) C-Max 1.6  Ti-VCT, EcoBoost', '2015', 'Essence', '2021-09-21 05:44:18'),
(270, 'CITROEN', 'C-crosser 2.4 16V', '2011', 'Essence', '2021-09-21 06:52:50'),
(271, 'AUDI', 'A2 1.4 TDI (P-D)', '2001', 'Essence', '2021-09-21 06:53:35'),
(272, 'AUDI', 'A1 1.2 TDI', '2003', 'Diesel', '2021-09-21 08:15:53'),
(273, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 08:25:49'),
(274, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 09:40:38'),
(275, 'AUDI', 'A1 1.2 TDI', '2001', 'Diesel', '2021-09-21 09:58:45'),
(276, 'BRILLIANCE', 'Cross', '2001', 'Essence', '2021-09-21 10:03:41'),
(277, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 10:40:17'),
(278, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-21 11:08:52'),
(279, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2003', 'Diesel', '2021-09-21 11:30:09'),
(280, 'BMW', '114d, 116d, 118d (F20)', '2014', 'Essence', '2021-09-21 14:07:29'),
(281, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-21 14:29:03'),
(282, 'KIA', 'Picanto 1.1 CRDi VGT', '2011', 'Essence', '2021-09-21 16:19:14'),
(283, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-21 19:39:36'),
(284, 'ALFA ROMEO', 'GIULIETTA', '2000', 'Essence', '2021-09-22 06:55:15'),
(285, 'AUDI', 'A4 1.9 DI, TDI, quattro', '2014', 'Diesel', '2021-09-22 08:22:35'),
(286, 'AUDI', 'A2 1.2 TDI', '2003', 'Essence', '2021-09-22 08:29:42'),
(287, 'AUDI', 'A1 1.6 TDI (66 kW, 77 kW)', '2001', 'Diesel', '2021-09-22 08:32:25'),
(288, 'AUDI', 'A2 1.4 TDI (P-D)', '2002', 'Diesel', '2021-09-22 09:57:17'),
(289, 'VOLKSWAGEN (VW)', 'Golf VI  1.4 16V, 1.6', '2009', 'Essence', '2021-09-22 12:05:58'),
(290, 'AUDI', 'A4 2.0 16V FSI', '2017', 'Essence', '2021-09-22 12:14:33'),
(291, 'PEUGEOT', '308  1.6  16V VTi  THP, 1.6  16V VTi  THP (128 kW)', '2013', 'Essence', '2021-09-22 12:46:14'),
(292, 'BMW', '118d,  120d,  DPF (E87/E82/E81/E88)', '2007', 'Essence', '2021-09-22 13:08:29'),
(293, 'PEUGEOT', '5008 1.6 16V Vti, THP', '2010', 'Essence', '2021-09-22 13:11:11'),
(294, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-22 14:41:45'),
(295, 'AUDI', 'A6 2.7, 3.0 TDI DPF, quattro', '2000', 'Essence', '2021-09-22 14:44:25'),
(296, 'MAZDA', '3 SKYACTIV-G 1.5', '2017', 'Essence', '2021-09-22 15:10:40'),
(297, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-22 15:19:59'),
(298, 'Autre', 'Autre', 'Autre', 'Essence', '2021-09-22 15:25:50'),
(299, 'VOLKSWAGEN (VW)', 'Golf VII  1.2 TSI (66kW, 77kW)', '2018', 'Essence', '2021-09-22 16:39:15'),
(300, 'VOLKSWAGEN (VW)', 'Golf VII  1.4 TSI (90kW)', '2016', 'Essence', '2021-09-23 08:02:53'),
(301, 'BYD', 'F0', '2002', 'Essence', '2021-09-23 08:16:01');

-- --------------------------------------------------------

--
-- Structure de la table `station`
--

CREATE TABLE `station` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `horaires` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `is_oil_change` tinyint(1) DEFAULT NULL,
  `is_closed_sunday` tinyint(1) DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` decimal(8,6) DEFAULT NULL,
  `longitude` decimal(9,6) DEFAULT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `station`
--

INSERT INTO `station` (`id`, `name`, `address`, `tel`, `is_active`, `is_deleted`, `create_at`, `horaires`, `is_oil_change`, `is_closed_sunday`, `image`, `latitude`, `longitude`, `file`) VALUES
(1, 'Station-service TOTAL Megrine', 'RTE BIZERTE, Ariana 2094', '+216 22 469 495', 0, 0, '0000-00-00 00:00:00', 'a:3:{i:0;s:1:\"2\";i:1;s:1:\"6\";i:2;s:1:\"1\";}', 1, 1, '', NULL, NULL, NULL),
(2, 'Station-service TOTAL Fouchana', 'RTE BIZERTE, Ariana 2094', '+216 22 469 495', 0, 0, '0000-00-00 00:00:00', NULL, NULL, NULL, '', NULL, NULL, NULL),
(3, 'station lac', '123', '', 1, 1, '2021-06-09 12:06:27', NULL, NULL, NULL, '', NULL, NULL, NULL),
(4, 'Station-service TOTAL M\'NIHLA', 'RTE BIZERTE, Ariana 2094', '+216 22 469 495', 0, 0, '2021-06-09 12:06:27', 'a:3:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"1\";}', 1, 0, '', NULL, NULL, NULL),
(5, 'Station TotalEnergies Ezzahra', 'Av de l\'Environnement, Ezzahra, 2034, Tunisie', '+216 25 177 775', 1, 0, '2021-08-25 09:17:02', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";}', 1, 1, 'ezzahra-6141bf6d82e14.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetnezzahra-614363e4225d5.pdf'),
(6, 'Station TotalEnergies Barraket Essahel', 'N1 Route de Sousse GP 1 Hammamet, Hammamet Sud 8056', '+216 58 565 108', 0, 0, '2021-08-25 09:31:35', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'totalhammamet22021314-6127c8f104bcd.png', NULL, NULL, NULL),
(7, 'Station TotalEnergies Sakiet Eddaier sfax', 'SAKIET EDDAIER KM 5, Sfax, Tunisia', '+216 53 739 144', 0, 0, '2021-08-25 09:57:28', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"3\";i:2;s:1:\"2\";}', 1, 1, 'kioske_total_sousse_p2-612e0b5f25b01.png', NULL, NULL, NULL),
(8, 'Station TotalEnergies Menzel Bouzelfa', 'Av H Bourguiba Menzel Bouzelfa, BNI KHALLED 8010, Tunisia', '+216 58 565 112', 0, 0, '2021-08-25 10:07:03', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'bouzelpha-612d583cf0d3e.png', NULL, NULL, NULL),
(9, 'Station TotalEnergies Riadh El Andalous', 'N8 Route de Bizerte GP 8 Cite Andalouse, 2080', '+216 58 565 314', 1, 0, '2021-08-25 10:07:47', 'a:3:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";}', 1, 1, 'gp8-6141bef40ec98.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetngp8-6149efccafd70.pdf'),
(10, 'Station TotalEnargies  Zaouiet Sousse', 'P1 Route régionale N°82 Sousse, 4081', '+216 58 565 517', 0, 0, '2021-08-25 10:15:49', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'zaouietsousse-612d5756bb70a.png', NULL, NULL, NULL),
(11, 'Station TotalEnergies Sabra Kairouan', 'Av. Abi Zamâa El Balaoui, Kairouan 3100', '+216 58 565 166', 0, 0, '2021-08-25 10:17:36', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'stationkairouansabra-612d56bf41307.png', NULL, NULL, NULL),
(12, 'Station TotalEnergies Khereddine Pacha Tunis', '15 Av. Kheireddine Pacha, Tunis 1002', '+216 55 666 677', 1, 0, '2021-08-25 10:22:41', 'a:4:{i:0;s:1:\"2\";i:1;s:1:\"3\";i:2;s:1:\"1\";i:3;s:1:\"4\";}', 1, 1, 'kpacha-6141beb132e3c.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetnkpacha-61436428454e1.pdf'),
(13, 'Station TotalEnergies La Marsa', 'Av. Ali Balhaouène, Marsa 2070', '+216 58 565 163', 1, 0, '2021-08-25 10:26:11', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'marsa-6141f1bc017f8.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetnmarsa-6149f0363f76f.pdf'),
(14, 'Station TotalEnergies Entrée Ouest Tunis', 'RN 5 Entrée Ouest Tunis', '+216 58 790 862', 1, 0, '2021-08-25 10:31:25', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'gp5-6141be5de7026.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetngp5-614363f892693.pdf'),
(15, 'Station TotalEnergies Bizerte', 'Route Menzel Bourguiba RN11 - Hafer Moher - Bizerte', '+216 58 565 236', 0, 0, '2021-08-25 11:07:49', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 0, 'download-6127c6f3a2ae8.png', NULL, NULL, NULL),
(16, 'Station TotalEnergies Sousse la corniche', 'GP 1 Tunis Rd, Route de la Corniche, Sousse 4000', '+216 58 565 101', 0, 0, '2021-08-25 11:11:32', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'kioske_total_sousse_p2-6127c69dea4aa.png', NULL, NULL, NULL),
(17, 'Station TotalEnergies Sousse 2', 'Avenue Léopold Senghor, Sousse 4000', '+216 58 565 547', 0, 0, '2021-08-25 11:12:55', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'totale-6127c673ae57f.png', NULL, NULL, NULL),
(18, 'Station TotalEnergies Zarzouna', 'GP8 Route de Tunis, Zarzouna, 7000', '+216 58 565 332', 0, 0, '2021-08-25 11:13:41', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'totalzarzouna-61362e9042ec3.png', NULL, NULL, NULL),
(19, 'Station TotalEnergies Dubosville', 'Route GP1 Km 2 Tunis', '+216 58 565 109', 0, 0, '2021-09-14 16:50:32', 'a:4:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";}', 1, 1, 'dubosville-6141be4c94e19.png', NULL, NULL, NULL),
(20, 'Station TotalEnergies hammam lif', 'N1 59 Av la République, Hammam-Lif 2050', '+216 98 516 672', 1, 0, '2021-09-20 15:13:22', 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";i:4;s:1:\"5\";i:5;s:1:\"6\";}', 1, 1, 'img20210920wa0015002-6148a53bcfc0a.png', NULL, NULL, 'conditionsgeneralesdeventemavidangetnhamamlif-6149ef62d6f59.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `station_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `create_at` datetime NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_sap` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `station_id`, `first_name`, `last_name`, `tel`, `email`, `roles`, `password`, `is_active`, `is_deleted`, `create_at`, `username`, `code_sap`) VALUES
(3, NULL, 'admin', 'admin', NULL, 'admin@total.com', '[\"ROLE_ADMIN\"]', '$2y$12$rLodA0aq09gTgksaj6F56uaJqXhuWB2.iwlnAofZpzBWh5n3Lqfi6', 0, 0, '0000-00-00 00:00:00', '', NULL),
(4, 1, 'gerant', 'gerant', NULL, 'gerant@total.com', '[\"ROLE_GERANT\"]', '$2y$12$CcOMpfYXLf0g0JInIJsA5eksbbhxPuBEZr9rMl2D4.tTXx5nmXIpi', 1, 0, '2021-06-08 15:02:03', '', NULL),
(5, 4, 'gerant1', 'gerant1', '+216 58 565 436', 'gerant1@total.com', '[\"ROLE_GERANT\"]', '$2y$12$BPt09IbWv17HtVHuMZYWuuZfaeg8qlMsAQHryeTn2Ia3qTZQLmTFu', 1, 0, '2021-06-08 15:02:46', '', '145874'),
(6, NULL, 'boussaid fatma', '', '58256487', 'client@total.com', '[\"ROLE_CLIENT\"]', '$2y$12$C8q1RTt23898pX.7zm9dnORhuvPPA1w78OGbkQ65qadONzocpN6Hq', 1, 0, '2021-06-21 10:44:58', 'client@total.com', NULL),
(7, NULL, 'boussaid fatma', '', '58256487', 'fatma.boussaid@app4mob.net', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$pDzDZ9z8jOE2+TqDwKrdYQ$ZKSsbtsbx7dULin7E7mEpGwKwIlyrUkEeYxPYyIj5LQ', 1, 0, '2021-07-05 07:38:42', 'fatma.boussaid@app4mob.net', NULL),
(8, NULL, 'sami zayene', '', '58565515', 'sami.zayene@total.tn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$KU3H6mBJ4Ke3ctPkRtbrIg$RDdZU8v7bvzb++SQTAK53igP9H1S4FodPFpMr52hTRg', 1, 0, '2021-08-19 07:40:47', 'sami.zayene@total.tn', NULL),
(9, NULL, 'Sami Zayene ', '', '58565515', 'sami.zayene@total.com', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$gCovN9a5H8vMa5Mv8GLWrw$PVmLQJsz52KlfjyvGhpmyUeHZuBeg/2oQnRKE8xw53s', 1, 0, '2021-08-19 15:45:18', 'sami.zayene@total.com', NULL),
(10, 5, 'gerant', 'ezzahra', '+216 65 898 741', 'gerantezzahra@totalenergies.com', '[\"ROLE_GERANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$MFwql4A+Y+Mi0QMWV7XMAA$yUTlGHD31bdGXPlJRTSsiNJdgTWMwBaTPg8yESQOiis', 1, 0, '2021-08-26 09:49:18', 'gerantezzahra@totalenergies.com', '0123654'),
(11, 5, 'Bessamra', 'Sarra', '+216 25 177 775', 'mohieddine.channoufi@totalenergies.tn', '[\"ROLE_GERANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$32GWTSRPPvJ5lYU2gicZKQ$O3yTuvqOY2gkP1+CYW914avm2JWrp4OgZAF1l8Rt5gs', 1, 0, '2021-08-26 15:20:31', 'mohieddine.channoufi@totalenergies.tn', '105082'),
(12, 7, 'Mohamed', 'Dammak', '+216 53 739 144', 'rafik.bouassida@totalenergies.tn', '[\"ROLE_GERANT\"]', '$2y$12$zwD8NdKTJyRJxhLhZeMnxOxdsbzntrlpLP39ssjT2O5QxT/IcayPu', 1, 0, '2021-08-26 15:23:50', 'rafik.bouassida@totalenergies.tn', '508172'),
(13, 8, 'Petromed SARL', 'kamel', '+216 58 565 112', 'admin1@total.com', '[\"ROLE_GERANT\"]', '$2y$12$0MY/.HGQPmRuYWy0i/D4rekwzGN58x02sIxEBma84W/0AP0DZvM/S', 1, 0, '2021-08-26 15:29:44', 'admin1@total.com', '105652'),
(14, 9, 'Debbech', 'Mehdi', '+216 58 565 314', 'admin2@total.com', '[\"ROLE_GERANT\"]', '$2y$12$XULhvSa/7kF34ntoaKDMtOuZNFqP2K3OpcNLICMfvREAzyrMONsYC', 1, 0, '2021-08-26 15:31:55', 'admin2@total.com', '105852'),
(15, 11, 'Kilani', 'Abderhmena', '+216 58 565 166', 'admin3@total.com', '[\"ROLE_GERANT\"]', '$2y$12$Ud1dx1p5FtOQPaoiHw.BDeiOwoYDImhx5ehnsRLDSg.SR4ffHXZ8C', 1, 0, '2021-08-26 15:33:36', 'admin3@total.com', '106792'),
(16, 6, 'Lagha', 'Ghazi', '+216 58 565 108', 'admin4@total.com', '[\"ROLE_GERANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$4I7eQKjqwM1IiCRkH6qE9g$xgajL6HHvt2GFggAZRX+fz1ZhseHcrUM1lg2/QxnXtU', 1, 0, '2021-08-26 15:36:19', 'admin4@total.com', '107325'),
(17, 12, 'Ben Njima', 'Mehdi', '+216 55 666 677', 'admin5@total.com', '[\"ROLE_GERANT\"]', '$2y$12$x1IaIPGLW7cD4KROY4HbbuiMe7I2pblrRsqFjmocTsrYIx5.YziZe', 1, 0, '2021-08-26 15:38:28', 'admin5@total.com', '108870'),
(18, 16, 'Baccouche', 'Tijani', '+216 58 565 101', 'admin6@total.com', '[\"ROLE_GERANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$WgcsaePubYffKwkOLa+0Rg$j033rwp/yYrSiUi7n913cgqbMWAekKc89dXbG2mhAFA', 1, 0, '2021-08-26 15:40:36', 'admin6@total.com', '144919'),
(19, 13, 'Mouhajir', 'Tarek', '+216 58 565 163', 'marsa@total.com', '[\"ROLE_GERANT\"]', '$2y$12$EB5pemBcWBe4QGUej2d9B.8gDZJGvIwJDvU462R1usB.2l0.dWnNa', 1, 0, '2021-08-26 15:42:31', 'marsa@total.com', '163536'),
(20, 17, 'Souilmi', 'Ahmed', '+216 58 565 547', 'admin8@total.com', '[\"ROLE_GERANT\"]', '$argon2id$v=19$m=65536,t=4,p=1$k4+Uqb0YTIjOGLgdIprg+A$iMtHb6k23CQgu+2tnYkG3bwtWGHpwnVazNCSECHhrwE', 1, 0, '2021-08-26 15:44:38', 'admin8@total.com', '517779'),
(21, 10, 'ghaha', 'imed', '+216 58 565 517', 'admin9@total.com', '[\"ROLE_GERANT\"]', '$2y$12$1F5NREe2s/FXkFRmWvh94.gTspGyHrODPUOI6.h4Uc67apEe.LSeK', 1, 0, '2021-08-26 15:47:01', 'admin9@total.com', '175628'),
(22, 14, 'Brahem', 'Med Ali', '+216 58 790 862', 'admin10@total.com', '[\"ROLE_GERANT\"]', '$2y$12$7tm3hB7kmt7j.nM15ifPP.Zlk5GomD3oRyzq2Xc0z.Vk5RRiU9BCu', 1, 0, '2021-08-26 15:48:14', 'admin10@total.com', '509462'),
(23, 15, 'Chateur', 'Ahmed', '+216 58 565 236', 'admin11@total.com', '[\"ROLE_GERANT\"]', '$2y$12$egLPLNbyK5R2h0rnYCyLgOu2lrnAnalSF8x1syzxQpYXrMD7Gnedy', 1, 0, '2021-08-26 15:49:56', 'admin11@total.com', '198831'),
(24, 13, 'Marsa', 'Marsa', '+216 58 565 332', 'rafik.bouassida@total.tn', '[\"ROLE_GERANT\"]', '$2y$12$vPUzX.iSTAqm3u13M8y5xeKbk.ao/znyYjkQ7w38G/z8h3vcPilcW', 1, 0, '2021-08-26 15:51:51', 'rafik.bouassida@total.tn', '500517'),
(25, NULL, 'Bouassida Rafik', '', '58565436', 'rafbouassida@gmail.com', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$jmfdfSykgoWJVwE05VxnAg$+VziQb8wceGehAoE3D2N9gBUhuopfPVBlXDA0O3aEMg', 1, 0, '2021-08-30 15:16:34', 'rafbouassida@gmail.com', NULL),
(26, NULL, 'ZAYENE Sami', '', '58565515', 'sami.zayene@total.cn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$2Rjt4QfRcQ0ZS+60k7OFww$4QDoHWZQAWXgjwIXOX5V26fo1IBi5LbZVfC2E2uBrlM', 1, 0, '2021-08-31 12:31:08', 'sami.zayene@total.cn', NULL),
(27, NULL, 'fatma boussaid', '', '58256487', 'fatma.boussaid@esprit.tn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$mNPQcmHW3TbkO3u8b3Wgcg$cTmZZO/YvIPdX6rFoy21JA2EE07BqRHOngLYcLw1U8w', 1, 0, '2021-09-01 23:15:35', 'fatma.boussaid@esprit.tn', NULL),
(28, NULL, 'Mohieddine channoufi', '', '58565285', 'mouha.DHIB@GMAIL.com', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$/os2x02imt2snRTKZBOEag$VX1TEDqWDU/aJYUl37y4lJgbI/Rv2ll547mEdSUfKMA', 1, 0, '2021-09-13 14:49:21', 'mouha.DHIB@GMAIL.com', NULL),
(29, NULL, 'Mohieddine channoufi', '', '58565285', 'mohieddine.channoufi@total.tn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$XrQdKPxXMB8KO1NE+AoZ5w$HjOqag9SQKZt9RcyZ5TOlTWX+odr44/SARymw40dzcQ', 1, 0, '2021-09-15 15:13:05', 'mohieddine.channoufi@total.tn', NULL),
(30, NULL, 'SAMI ZAYENE', '', '85565515', 'sami.zayene@totalenergies.tn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$BOP+FG5Fh77iHFIRul6+xw$N+Y4+sk+g9ugGL4Hdrxe+SYmal0OMPwAitkPody+e+A', 1, 0, '2021-09-16 12:44:39', 'sami.zayene@totalenergies.tn', NULL),
(31, NULL, 'Slim azzabi', '', '58585610', 'slim.azzabi@umww.com', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$nm3o6tG8bYOXv/ZrZmzyvQ$SCbSeWH3F4a3sTJfRLbpq7rzq4PwEZJgt4xQ9mgYLE8', 1, 0, '2021-09-16 14:51:22', 'slim.azzabi@umww.com', NULL),
(32, NULL, 'Mohamed manaii', '', '58565285', 'mohamed.manai@total.tn', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$wYzS8gj9/3mOhSlHW0Hc9g$9LrEYc9fHKOiwKoVRECdgakqhGTSwvJgwn337LVw9Yk', 1, 0, '2021-09-17 13:02:07', 'mohamed.manai@total.tn', NULL),
(33, NULL, 'Drisse', '', '05456667', 'drisse.aghnaj@gmail.com', '[\"ROLE_CLIENT\"]', '$argon2id$v=19$m=65536,t=4,p=1$BlNiAtrK2XcCuk+hGt5R1w$EdDcOB0XIuucsVLqmn90weiqmH3/Iq6/JcEJV/XQQR8', 1, 0, '2021-09-18 20:27:59', 'drisse.aghnaj@gmail.com', NULL),
(34, 20, 'Toumi', 'khaled', '+216 98 516 672', 'admin13@total.com', '[\"ROLE_GERANT\"]', '$2y$12$Z./AKgS5s2sNiU5i3jo8M./dZaU87FnWJBU.RGPq8FbjTtIA9CeGC', 1, 0, '2021-09-20 15:19:37', 'admin13@total.com', '197356');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6EEAA67D53C674EE` (`offer_id`),
  ADD KEY `IDX_6EEAA67D4584665A` (`product_id`),
  ADD KEY `IDX_6EEAA67D21BDB235` (`station_id`),
  ADD KEY `IDX_6EEAA67DA76ED395` (`user_id`);

--
-- Index pour la table `commande_prestation`
--
ALTER TABLE `commande_prestation`
  ADD PRIMARY KEY (`commande_id`,`prestation_id`),
  ADD KEY `IDX_C90AA34B82EA2E54` (`commande_id`),
  ADD KEY `IDX_C90AA34B9E45C554` (`prestation_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_29D6873E4584665A` (`product_id`);

--
-- Index pour la table `offer_prestation`
--
ALTER TABLE `offer_prestation`
  ADD PRIMARY KEY (`offer_id`,`prestation_id`),
  ADD KEY `IDX_CD624C7953C674EE` (`offer_id`),
  ADD KEY `IDX_CD624C799E45C554` (`prestation_id`);

--
-- Index pour la table `offer_station`
--
ALTER TABLE `offer_station`
  ADD PRIMARY KEY (`offer_id`,`station_id`),
  ADD KEY `IDX_3E313EB853C674EE` (`offer_id`),
  ADD KEY `IDX_3E313EB821BDB235` (`station_id`);

--
-- Index pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_51C88FAD21BDB235` (`station_id`);

--
-- Index pour la table `prestation_station`
--
ALTER TABLE `prestation_station`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_5CAF0B2821BDB235` (`station_id`),
  ADD KEY `IDX_5CAF0B289E45C554` (`prestation_id`);

--
-- Index pour la table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `product_station`
--
ALTER TABLE `product_station`
  ADD PRIMARY KEY (`product_id`,`station_id`),
  ADD KEY `IDX_4EE0E3014584665A` (`product_id`),
  ADD KEY `IDX_4EE0E30121BDB235` (`station_id`);

--
-- Index pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_7CE748AA76ED395` (`user_id`);

--
-- Index pour la table `search`
--
ALTER TABLE `search`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD KEY `IDX_8D93D64921BDB235` (`station_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=344;

--
-- AUTO_INCREMENT pour la table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT pour la table `prestation`
--
ALTER TABLE `prestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT pour la table `prestation_station`
--
ALTER TABLE `prestation_station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT pour la table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `search`
--
ALTER TABLE `search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT pour la table `station`
--
ALTER TABLE `station`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `FK_6EEAA67D21BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `FK_6EEAA67D4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_6EEAA67D53C674EE` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`),
  ADD CONSTRAINT `FK_6EEAA67DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `commande_prestation`
--
ALTER TABLE `commande_prestation`
  ADD CONSTRAINT `FK_C90AA34B82EA2E54` FOREIGN KEY (`commande_id`) REFERENCES `commande` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_C90AA34B9E45C554` FOREIGN KEY (`prestation_id`) REFERENCES `prestation` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `FK_29D6873E4584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`);

--
-- Contraintes pour la table `offer_prestation`
--
ALTER TABLE `offer_prestation`
  ADD CONSTRAINT `FK_CD624C7953C674EE` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_CD624C799E45C554` FOREIGN KEY (`prestation_id`) REFERENCES `prestation` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `offer_station`
--
ALTER TABLE `offer_station`
  ADD CONSTRAINT `FK_3E313EB821BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_3E313EB853C674EE` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `prestation`
--
ALTER TABLE `prestation`
  ADD CONSTRAINT `FK_51C88FAD21BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`);

--
-- Contraintes pour la table `prestation_station`
--
ALTER TABLE `prestation_station`
  ADD CONSTRAINT `FK_5CAF0B2821BDB235` FOREIGN KEY (`station_id`) REFERENCES `station` (`id`),
  ADD CONSTRAINT `FK_5CAF0B289E45C554` FOREIGN KEY (`prestation_id`) REFERENCES `prestation` (`id`);

--
-- Contraintes pour la table `reset_password_request`
--
ALTER TABLE `reset_password_request`
  ADD CONSTRAINT `FK_7CE748AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
