<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210608095433 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE offer (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION NOT NULL, price_promo DOUBLE PRECISION DEFAULT NULL, is_promo TINYINT(1) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, is_deleted TINYINT(1) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, INDEX IDX_29D6873E4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION NOT NULL, price_promo DOUBLE PRECISION DEFAULT NULL, is_promo TINYINT(1) DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, is_deleted TINYINT(1) DEFAULT NULL, id_lubricant INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_station (product_id INT NOT NULL, station_id INT NOT NULL, INDEX IDX_4EE0E3014584665A (product_id), INDEX IDX_4EE0E30121BDB235 (station_id), PRIMARY KEY(product_id, station_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, tel VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, station_id INT DEFAULT NULL, fist_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) DEFAULT NULL, tel VARCHAR(255) DEFAULT NULL, INDEX IDX_8D93D64921BDB235 (station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE offer ADD CONSTRAINT FK_29D6873E4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_station ADD CONSTRAINT FK_4EE0E3014584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_station ADD CONSTRAINT FK_4EE0E30121BDB235 FOREIGN KEY (station_id) REFERENCES station (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64921BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE offer DROP FOREIGN KEY FK_29D6873E4584665A');
        $this->addSql('ALTER TABLE product_station DROP FOREIGN KEY FK_4EE0E3014584665A');
        $this->addSql('ALTER TABLE product_station DROP FOREIGN KEY FK_4EE0E30121BDB235');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64921BDB235');
        $this->addSql('DROP TABLE offer');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_station');
        $this->addSql('DROP TABLE station');
        $this->addSql('DROP TABLE user');
    }
}
