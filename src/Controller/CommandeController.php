<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\CommandeRepository;
use App\Repository\OfferRepository;
use App\Repository\ParametreRepository;
use App\Repository\PrestationRepository;
use App\Repository\ProductRepository;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/commande")
 */
class CommandeController extends AbstractController
{
    /**
     * @Route("/", name="commande_completed", methods={"GET"})
     */
    public function commandeCompleted(ParametreRepository $parametreRepository,CommandeRepository $commandeRepository,PrestationRepository $prestationRepository,ProductRepository $productRepository,OfferRepository $offerRepository, StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $page = $request->get('page', '1');
        $product = $request->get('product', '');
        $offer = $request->get('offer', '');
        $station = $request->get('station', '');
        $statut = $request->get('statut', '');
        $qrcode = $request->get('qrcode', '');
        $dateDebut = $request->get('dateDebut', '');
        $dateFin = $request->get('dateFin', '');
        $client=$request->get('client', '');
        $prestation = $request->get('prestation', '');
        $vehicule = $request->get('vehicule', '');
        $idcommande = $request->get('idcommande', '');
        $prestations= $prestationRepository->findPrestations('');
        if($this->getUser()->hasRole('ROLE_GERANT')){
            $station=$this->getUser()->getStation()->getId();
        }

        $offers=$offerRepository->findOffers('',$station);
        $stations = $stationRepository->findStations();
        $products= $productRepository->findProducts('');
        $commandes = $paginator->paginate(
        // Doctrine Query, not results
            $commandeRepository->findCompletedCommandes($product,$offer,$station,$statut,$qrcode,$dateDebut,$dateFin,$client,$idcommande,$vehicule,$prestation),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        $parametre=$parametreRepository->find(1);

        return $this->render('commande/completed.html.twig', [
            'commandes' => $commandes,
            'products' => $products,
            'offers' => $offers,
            'stations' => $stations,
            'product' => $product,
            'offer' => $offers,
            'prestations' => $prestations,
            'station' => $station,
            'statut' => $statut,
            'qrcode'=>$qrcode,
            'client'=>$client,
            'idcommande'=>$idcommande,
            'dateDebut'=>$dateDebut,
            'dateFin'=>$dateFin,
            'vehicule' => $vehicule,
            'prestation' => $prestation,
            'parametre' => $parametre,


        ]);
    }
    /**
     * @Route("/incomplete", name="commande_incompleted", methods={"GET"})
     */
    public function commandeInCompleted(ParametreRepository $parametreRepository,CommandeRepository $commandeRepository,PrestationRepository $prestationRepository,
                                        ProductRepository $productRepository,OfferRepository $offerRepository, StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $page = $request->get('page', '1');
        $product = $request->get('product', '');
        $offer = $request->get('offer', '');
        $station = $request->get('station', '');
        $dateDebut = $request->get('dateDebut', '');
        $dateFin = $request->get('dateFin', '');
        $client=$request->get('client', '');
        $prestation = $request->get('prestation', '');
        $vehicule = $request->get('vehicule', '');
        $prestations= $prestationRepository->findPrestations('');

        $prestations= $prestationRepository->findPrestations('');
        if($this->getUser()->hasRole('ROLE_GERANT')){
            $station=$this->getUser()->getStation()->getId();
        }

        $offers=$offerRepository->findOffers('',$station);
        $stations = $stationRepository->findStations();
        $products= $productRepository->findProducts('');
        $prestations= $prestationRepository->findPrestations('');

        $commandes = $paginator->paginate(
        // Doctrine Query, not results
            $commandeRepository->findInCompletedCommandes($product,$offer,$station,$dateDebut,$dateFin,$client,$vehicule,$prestation),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        $parametre=$parametreRepository->find(1);

        return $this->render('commande/incompleted.html.twig', [
            'commandes' => $commandes,
            'products' => $products,
            'offers' => $offers,
            'stations' => $stations,
            'product' => $product,
            'offer' => $offers,
            'prestations' => $prestations,
            'station' => $station,
            'client'=>$client,
            'dateDebut'=>$dateDebut,
            'dateFin'=>$dateFin,
            'vehicule' => $vehicule,
            'prestation' => $prestation,
            'parametre' => $parametre,

        ]);
    }


    /**
     * Displays a form to edit an existing Produits entity.
     *
     * @Route("/edit/{id}/{statut}", name="commnde_edit", methods={"GET"})
     */
    public function editAction(Commande $commande,$statut,Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $commande->setOrderStatus($statut);
        $entityManager->flush();
        return $this->redirectToRoute('commande_completed');
    }

    /**
     * @Route("/delete/{id}", name="product_commande", methods={"GET"})
     */
    public function delete(Request $request, Product $product): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();


        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/export", name="product_export", methods={"GET"})
     */
    public function export(ProductRepository $productRepository, Request $request, TranslatorInterface $translator): Response
    {
        $keyword = $request->get('keyword', '');
        $station = $request->get('station', '');
        $items = $userRepository->findUsers($keyword, $station, 'ROLE_GERANT');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Gérants");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValue('A1', $translator->trans("Nom"));
        $sheet->setCellValue('B1', $translator->trans("Email"));
        $sheet->setCellValue('C1', $translator->trans("Téléphone"));
        $sheet->setCellValue('D1', $translator->trans("Station"));

        $i = 2;
        foreach ($items as $item) {
            $sheet->setCellValue('A' . $i, $item->getUsername());
            $sheet->setCellValue('B' . $i, $item->getEmail());
            $sheet->setCellValue('C' . $i, $item->getTel());
            $sheet->setCellValue('D' . $i, $item->getStation()->getName());
            $i++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $translator->trans("Gérants") . '.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
