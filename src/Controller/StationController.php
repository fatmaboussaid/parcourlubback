<?php

namespace App\Controller;

use App\Entity\Branch;
use App\Entity\PrestationStation;
use App\Entity\Station;
use App\Form\HorairesType;
use App\Form\StationType;
use App\Repository\PrestationStationRepository;
use App\Repository\StationRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/station")
 */
class StationController extends AbstractController
{
    /**
     * @Route("/", name="station_index", methods={"GET"})
     */
    public function index(StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $keyword = $request->get('keyword', '');
        $page = $request->get('page', '1');

        $stations = $paginator->paginate(
        // Doctrine Query, not results
            $stationRepository->findStations($keyword),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        return $this->render('station/index.html.twig', [
            'stations' => $stations,
            'keyword' => $keyword,

        ]);
    }

    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }

    /**
     * @Route("/new", name="station_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $station = new Station();
        $form = $this->createForm(StationType::class, $station);
        $entityManager = $this->getDoctrine()->getManager();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['file']->getData();
            $image = $form['image']->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $station->setImage($newFilename);
            }
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.pdf';
                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('files_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $station->setFile($newFilename);
            }
            foreach ($station->getPrestations() as $prestation) {
                    $prestation_station = new PrestationStation();
                    $prestation_station->setPrestation($prestation);
                    $prestation_station->setStation($station);
                    $entityManager->persist($prestation_station);

            }

            $entityManager->persist($station);
            $entityManager->flush();

            return $this->redirectToRoute('station_index');
        }

        return $this->render('station/new.html.twig', [
            'station' => $station,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }

    public function verifyPrestationExist($prestationsStations,$prestation){
        $verif=false;
        foreach ($prestationsStations as $prestationStation) {
            if ($prestationStation->getPrestation() == $prestation) {
                $verif=true;
            }
        }
        return $verif;
    }
    /**
     * @Route("/{id}/edit", name="station_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Station $station, PrestationStationRepository $prestationStationRepository): Response
    {
        $formOptions = array('station' => $station);
        $form = $this->createForm(StationType::class, $station, $formOptions);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form['file']->getData();
            $image = $form['image']->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $station->setImage($newFilename);
            }
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.pdf';
                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('files_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $station->setFile($newFilename);
            }
            $prestations = $station->getPrestations();
            $prestationsStations = $prestationStationRepository->findPrestationsStation('', $station->getId(), "", false);
            foreach ($prestationsStations as $prestationStation) {
                if (!$prestations->contains($prestationStation->getPrestation())) {
                    $entityManager->remove($prestationStation);
                }
            }
            foreach ($prestations as $prestation) {
                if (!$this->verifyPrestationExist($prestationsStations,$prestation)) {
                    $prestation_station = new PrestationStation();
                    $prestation_station->setPrestation($prestation);
                    $prestation_station->setStation($station);
                    $entityManager->persist($prestation_station);
                }
            }

                $entityManager->flush();

                return $this->redirectToRoute('station_index');
            }

            return $this->render('station/edit.html.twig', [
                'station' => $station,
                'form' => $form->createView(),
                'errors' => $this->getErrors($form, $form->getName())

            ]);
        }

        /**
         * @Route("/delete/{id}", name="station_delete", methods={"GET"})
         */
        public
        function delete(Request $request, Station $station): Response
        {
            $entityManager = $this->getDoctrine()->getManager();
            $offers = $station->getOffers();
            foreach ($offers as $offer) {
                $station->removeOffer($offer);
            }
            $station->setIsDeleted(true);
            $entityManager->flush();
            return $this->redirectToRoute('station_index');
        }

        /**
         * @Route("/parametrage/horaires", name="station_parametrage", methods={"GET","POST"})
         */
        public
        function parametrage(Request $request): Response
        {
            $station = $this->getUser()->getStation();
            $form = $this->createForm(HorairesType::class, $station);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $horaires = $request->get('horaires', []);
                if (array_key_exists('isOilChange', $horaires)) $station->setIsOilChange(true);
                else $station->setIsOilChange(false);
                if (array_key_exists('isClosedSunday', $horaires)) $station->setIsClosedSunday(true);
                else $station->setIsClosedSunday(false);
                if (array_key_exists('horaires', $horaires)) $station->setHoraires($horaires['horaires']);
                else  $station->setHoraires([]);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();

                return $this->redirectToRoute('station_parametrage');
            }

            return $this->render('parametrage/new.html.twig', [
                'station' => $station,
                'form' => $form->createView(),
                'errors' => $this->getErrors($form, $form->getName())

            ]);
        }

        /**
         * @Route("/station_export", name="station_export", methods={"GET"})
         */
        public
        function export(StationRepository $stationRepository, Request $request, TranslatorInterface $translator): Response
        {
            $keyword = $request->get('keyword', '');
            $items = $stationRepository->findStations($keyword);
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setTitle("Stations");
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $sheet->setCellValue('A1', $translator->trans("Nom"));
            $sheet->setCellValue('B1', $translator->trans("Adresse"));
            $sheet->setCellValue('C1', $translator->trans("Téléphone"));
            $sheet->setCellValue('D1', $translator->trans("Est Active"));

            $i = 2;
            foreach ($items as $item) {
                $sheet->setCellValue('A' . $i, $item->getName());
                $sheet->setCellValue('B' . $i, $item->getAddress());
                $sheet->setCellValue('C' . $i, $item->getTel());
                if ($item->getIsActive()) $sheet->setCellValue('D' . $i, $translator->trans("Oui"));
                else $sheet->setCellValue('D' . $i, $translator->trans("Non"));
                $i++;
            }

            // Create your Office 2007 Excel (XLSX Format)
            $writer = new Xlsx($spreadsheet);

            // Create a Temporary file in the system
            $fileName = $translator->trans("Stations") . '.xlsx';

            $temp_file = tempnam(sys_get_temp_dir(), $fileName);

            // Create the excel file in the tmp directory of the system
            $writer->save($temp_file);

            // Return the excel file as an attachment
            return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
        }

        /**
         * @Route("/station_import", name="station_import", methods={"POST"})
         */
        public
        function import(Request $request): Response
        {
            $entityManager = $this->getDoctrine()->getManager();

            $directory = $this->getParameter('kernel.project_dir') . '/public/uploads';

            $theFileName = "";
            foreach ($request->files as $uploadedFile) {

                if ($uploadedFile != null) {

                    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
                    $reader->setReadDataOnly(TRUE);
                    $spreadsheet = $reader->load($uploadedFile);
                    $worksheet = $spreadsheet->getActiveSheet();

                    // Get the highest row number and column letter referenced in the worksheet
                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                    // Increment the highest column letter
                    $highestColumn++;

                    $erreur = 0;
                    $new = 0;
                    $updated = 0;
                    $message = array();


                    for ($row = 2; $row <= $highestRow; ++$row) {

                        $name = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                        $name = trim($name);

                        $address = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                        $address = trim($address);

                        $tel = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                        $tel = trim($tel);

                        $skip = 0;

                        if ($name == "") {
                            $erreur++;
                            $skip = 1;
                            $message [] = "Ligne " . $row . " : le nom est vide ";
                            $this->addFlash('errors', "Ligne " . $row . " : le nom est vide ");
                        }

                        if ($skip == 0) {
                            $item = new Station();
                            $item->setName($name);
                            if ($address != "") {
                                //remove the last ;
                                $item->setAddress($address);
                            } else {
                                $item->setAddress('');
                            }
                            if ($tel != "") {
                                //remove the last ;
                                $item->setTel($tel);
                            } else {
                                $item->setTel('');
                            }
                            $entityManager->persist($item);
                            $new++;
                        }

                    }
                    if ($erreur == 0) {
                        $entityManager->flush();
                        $this->addFlash('success', $new . ' lignes ajoutées avec succès');
                    }
                    return $this->redirectToRoute('station_index');

                }
            }

        }

    }
