<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository,StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $keyword=$request->get('keyword', '');
        $station=$request->get('station', '');
        $page = $request->get('page', '1');
        $stations= $stationRepository->findStations();
        $users = $paginator->paginate(
        // Doctrine Query, not results
            $userRepository->findUsers($keyword,$station,'ROLE_GERANT'),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        return $this->render('user/index.html.twig', [
            'users' => $users,
            'stations' => $stations,
            'station' => $station,
            'keyword' => $keyword,

        ]);


        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }
    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }
    /**
     * @Route("/new", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->add('plainPassword', RepeatedType::class, [
            'required' => true,
            'type' => PasswordType::class,
            'error_bubbling' => true,
            'invalid_message' => "Le mot de passe et la confirmation ne sont pas identiques",
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(array('ROLE_GERANT'));
            $user->setUsername($user->getEmail());
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $user->getPlainPassword()));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())
        ]);
    }



    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $password=$user->getPassword();
        $form = $this->createForm(UserType::class, $user);
        $form->add('plainPassword', RepeatedType::class, [

            'required' => false,
            'type' => PasswordType::class,
            'error_bubbling' => true,
            'invalid_message' => "Le mot de passe et la confirmation ne sont pas identiques",
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUsername($user->getEmail());

            if ($user->getPlainPassword() ) {
                $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($password);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }

    /**
     * @Route("/delete/{id}", name="user_delete", methods={"POST"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }


    /**
     * @Route("/user_export", name="user_export", methods={"GET"})
     */
    public function export(UserRepository $userRepository, Request $request, TranslatorInterface $translator): Response
    {
        $keyword = $request->get('keyword', '');
        $station=$request->get('station', '');
        $items =   $userRepository->findUsers($keyword,$station,'ROLE_GERANT');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Gérants");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValue('A1', $translator->trans("Nom"));
        $sheet->setCellValue('B1', $translator->trans("Email"));
        $sheet->setCellValue('C1', $translator->trans("Téléphone"));
        $sheet->setCellValue('D1', $translator->trans("Station"));

        $i = 2;
        foreach ($items as $item) {
            $sheet->setCellValue('A' . $i, $item->getUsername());
            $sheet->setCellValue('B' . $i, $item->getEmail());
            $sheet->setCellValue('C' . $i, $item->getTel());
            $sheet->setCellValue('D' . $i, $item->getStation()->getName());
            $i++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $translator->trans("Gérants") . '.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
