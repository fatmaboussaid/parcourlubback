<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\OfferRepository;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/offer")
 */
class OfferController extends AbstractController
{

    /**
     * @Route("/listOffers", name="listOffers", methods={"GET"})
     */
    public function listOffers(Request $request, OfferRepository $offerRepository): Response
    {
        $max = $request->get('max', 0);
        $data = $offerRepository->findActiveOffers("", "");
        if (intval($max) != 0)  {
            $data= array_slice($data,0,intval($max));
            //dump($data);die;

        }
        return new JsonResponse($this->getOffers($data, $request->getSchemeAndHttpHost() ), 200);


    }
    /**
     * @Route("/getOffer", name="getOffer", methods={"GET"})
     */
    public function getOffer(Request $request, OfferRepository $offerRepository): Response
    {
        $id=$request->get('id',0);
        $latitude=$request->get('latitude','');
        $longitude=$request->get('longitude','');
        $offer = $offerRepository->findActiveOffer($id,$latitude,$longitude);
        if(array_key_exists('distance', $offer))
            $offer=$offer[0];
        if($offer){
            $data=[
                'code'=>1,
                'offer'=>[
                    'id' => $offer->getId(),
                    'name' => $offer->getName(),
                    'description' => $offer->getDescription(),
                    'price' => $offer->getPrice(),
                    'pricePromo' => $offer->getPricePromo(),
                    'isPromo' => $offer->getIsPromo(),
                    'image' => $request->getSchemeAndHttpHost().'/uploads/images/'.$offer->getImage(),
                    'file' => $offer->getProduct()->getFile(),
                    'filePath' => $request->getSchemeAndHttpHost().'/uploads/files/'.$offer->getProduct()->getFile(),
                    'quantity' => $offer->getQuantity(),
                    'product' => $this->getProduct($offer->getProduct(), $request->getSchemeAndHttpHost()),
                    'stations' => $this->getStations($offer->getStations(),$request->getSchemeAndHttpHost()),
                    'prestations' => $this->getPrestations($offer->getPrestations(),$request->getSchemeAndHttpHost()),

                ],
            ];

        }
        else{
            $data=[
                'code'=>2,
                'message'=>"not found offer",
            ];
        }

        return new JsonResponse($data, 200);


    }
    public function getOffers($data, $url)
    {
        $offers = [];
        foreach ($data as $offer) {
            array_push($offers, [
                'id' => $offer->getId(),
                'name' => $offer->getName(),
                'description' => $offer->getDescription(),
                'price' => $offer->getPrice(),
                'pricePromo' => $offer->getPricePromo(),
                'isPromo' => $offer->getIsPromo(),
                'image' => $url .'/uploads/images/'. $offer->getImage(),
                'file' => $offer->getProduct()->getFile(),
                'filePath' =>  $url.'/uploads/files/'.$offer->getProduct()->getFile(),
                'quantity' => $offer->getQuantity(),
                'product' => $this->getProduct($offer->getProduct(), $url),
            ]);
        }
        return $offers;
    }
    public function getStations($data,$url)
    {
        $stations = [];
        foreach ($data as $station) {
            if($station->getIsActive()== true && $station->getIsDeleted()== false){
                array_push($stations, [
                    'id' => $station->getId(),
                    'name' => $station->getName(),
                    'address' => $station->getAddress(),
                    'tel' => $station->getTel(),
                    'image' => $url.'/uploads/images/' .$station->getImage(),
                ]);
            }

        }
        return $stations;
    }
    public function getProduct($data, $url)
    {

            $product = [
                'id' => $data->getId(),
                'name' => $data->getName(),
                'description' => $data->getDescription(),
                'price' => $data->getPrice(),
                'pricePromo' => $data->getPricePromo(),
                'isPromo' => $data->getIsPromo(),
                'image' => $url.'/uploads/images/' . $data->getImage(),
                'file' => $data->getFile(),
                'filePath' => $url.'/uploads/files/'.$data->getFile(),

                'quantity' => $data->getQuantity(),

            ];



        return $product;
    }
    public function getPrestations($data, $url)
    {
        $prestations = [];
        foreach ($data as $prestation) {
            if($prestation->getIsActive()== true && $prestation->getIsDeleted()== false) {
                array_push($prestations,[
                    'id' => $prestation->getId(),
                    'name' => $prestation->getName(),
                    'icon'=>$url.'/uploads/images/' .$prestation->getIcon()
                    ]);
            }

        }
        return $prestations;
    }
}