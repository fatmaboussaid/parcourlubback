<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\OfferRepository;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{

    /**
     * @Route("/listProducts", name="listProducts", methods={"GET"})
     */
    public function listProducts(Request $request, ProductRepository $productRepository): Response
    {
        $max = $request->get('max', 0);
        $data = $productRepository->findActiveProducts('', '', $max);
        if (intval($max) != 0) {
            $data = array_slice($data, 0, intval($max));
            //dump($data);die;

        }
        return new JsonResponse(
            $this->getProducts($data, $request->getSchemeAndHttpHost())
            , 200);


    }

    /**
     * @Route("/getProduct", name="getProduct", methods={"GET"})
     */
    public function getProduct(Request $request, ProductRepository $productRepository): Response
    {
        $id = $request->get('id', 0);
        $product = $productRepository->findActiveProduct($id);
        if ($product) {
            $data = [
                'code' => 1,
                'product' => [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'description' => $product->getDescription(),
                    'price' => $product->getPrice(),
                    'pricePromo' => $product->getPricePromo(),
                    'isPromo' => $product->getIsPromo(),
                    'image' => $request->getSchemeAndHttpHost() . '/uploads/images/' . $product->getImage(),
                    'file' =>$product->getFile(),
                    'filePath' =>  $request->getSchemeAndHttpHost() . '/uploads/files/' . $product->getFile(),

                    'quantity' => $product->getQuantity(),
                    'stations' => $this->getStations($product->getStations(), $request->getSchemeAndHttpHost() ),
                ],
            ];

        } else {
            $data = [
                'code' => 2,
                'message' => "not found product",
            ];
        }

        return new JsonResponse($data, 200);
    }

    public function getProducts($data, $url)
    {
        $result = [];


        foreach ($data as $product) {
            array_push($result, [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'price' => $product->getPrice(),
                'pricePromo' => $product->getPricePromo(),
                'isPromo' => $product->getIsPromo(),
                'image' => $url . '/uploads/images/' . $product->getImage(),
                'file' => $product->getFile(),
                'filePath' => $url . '/uploads/files/' . $product->getFile(),
                'quantity' => $product->getQuantity(),

            ]);

        }
        return $result;
    }

    public function getOffers($data, $url)
    {
        $result = [];
        foreach ($data as $offer) {
            array_push($result, [
                'id' => $offer->getId(),
                'name' => $offer->getName(),
                'description' => $offer->getDescription(),
                'price' => $offer->getPrice(),
                'pricePromo' => $offer->getPricePromo(),
                'isPromo' => $offer->getIsPromo(),
                'image' => $url . '/uploads/images/' . $offer->getImage(),
                'file' => $offer->getProduct()->getFile(),
                'filePath' =>  $url . '/uploads/files/' . $offer->getProduct()->getFile(),

                'quantity' => $offer->getQuantity(),
                'stations' => $this->getStations($offer->getStations(), $url),

            ]);
        }

        return $result;
    }

    public function getStations($data, $url)
    {
        $stations = [];
        foreach ($data as $station) {
            if ($station->getIsActive() == true && $station->getIsDeleted() == false) {
                array_push($stations, [
                    'id' => $station->getId(),
                    'name' => $station->getName(),
                    'address' => $station->getAddress(),
                    'tel' => $station->getTel(),
                    'image' => $url . '/uploads/images/' . $station->getImage(),

                ]);
            }

        }
        return $stations;
    }

    /**
     * @Route("/getRecommendedProducts", name="getRecommendedProducts", methods={"GET"})
     */
    public function getRecommendedProducts(Request $request, ProductRepository $productRepository, OfferRepository $offerRepository): Response
    {
        $data = [];
        $stationid = $request->get('stationid', '');
        $recommendedLubricants = $request->get('recommendedLubricants', '[]');
        $alternativeLubricants = $request->get('alternativeLubricants', '[]');
        $carburantType = $request->get('carburantType', '');
        $recommendedProducts = $productRepository->findRecommendedProducts($stationid, $recommendedLubricants, $carburantType);
        $alternativeProducts = $productRepository->findAlternativeProducts($stationid, $alternativeLubricants, $carburantType);

        $defaultrecommendedProducts = $productRepository->findDefaultRecommendedProducts($stationid, $carburantType);
        $defaultalternativeProducts = $productRepository->findDefaultAlternativeProducts($stationid, $carburantType);
        $idsRecommendedProducts='';
        $idsAlternativeProducts='';
        $idsDefaultRecommendedProducts='';
        $idsDefaultAlternativeProducts='';
        foreach ($recommendedProducts as $recommended) {
            if (in_array($recommended, $defaultrecommendedProducts)) {
                $key = array_search($recommended, $defaultrecommendedProducts);
                unset($defaultrecommendedProducts[$key]);
            }
            if (in_array($recommended, $defaultalternativeProducts)) {
                $key = array_search($recommended, $defaultalternativeProducts);
                unset($defaultalternativeProducts[$key]);
            }
            $idsRecommendedProducts=$idsRecommendedProducts.$recommended->getId().',';
        }

        foreach ($alternativeProducts as $alternative) {
            if (in_array($alternative, $defaultrecommendedProducts)) {
                $key = array_search($alternative, $defaultrecommendedProducts);
                unset($defaultrecommendedProducts[$key]);
            }
            if (in_array($alternative, $defaultalternativeProducts)) {
                $key = array_search($alternative, $defaultalternativeProducts);
                unset($defaultalternativeProducts[$key]);
            }
            $idsAlternativeProducts=$idsAlternativeProducts.$alternative->getId().',';

        }
        foreach ($defaultrecommendedProducts as $defaultrecommended) {
            $idsDefaultRecommendedProducts=$idsDefaultRecommendedProducts.$defaultrecommended->getId().',';

        }
        foreach ($defaultalternativeProducts as $defaultalternative) {
            $idsDefaultAlternativeProducts=$idsDefaultAlternativeProducts.$defaultalternative->getId().',';

        }
        $idsRecommendedProducts=substr($idsRecommendedProducts, 0, -1);
        $idsAlternativeProducts=substr($idsAlternativeProducts, 0, -1);
        $idsDefaultRecommendedProducts=substr($idsDefaultRecommendedProducts, 0, -1);
        $idsDefaultAlternativeProducts=substr($idsDefaultAlternativeProducts, 0, -1);



        $recommendedOffers = $offerRepository->findRecommendedOffers($stationid, $idsRecommendedProducts);
        $alternativeOffers = $offerRepository->findRecommendedOffers($stationid, $idsAlternativeProducts);

        $defaultRecommendedOffers = $offerRepository->findRecommendedOffers($stationid, $idsDefaultRecommendedProducts);
        $defaultAlternativeOffers = $offerRepository->findRecommendedOffers($stationid, $idsDefaultAlternativeProducts);



        $data['defaultrecommendedProducts'] = $this->getProducts($defaultrecommendedProducts, $request->getSchemeAndHttpHost());
        $data['defaultalternativeProducts'] = $this->getProducts($defaultalternativeProducts, $request->getSchemeAndHttpHost());

        $data['recommendedProducts'] = $this->getProducts($recommendedProducts, $request->getSchemeAndHttpHost());
        $data['alternativeProducts'] = $this->getProducts($alternativeProducts, $request->getSchemeAndHttpHost());
        $data['recommendedOffers'] = $this->getOffers($recommendedOffers, $request->getSchemeAndHttpHost());
        $data['alternativeOffers'] = $this->getOffers($alternativeOffers, $request->getSchemeAndHttpHost());
        $data['defaultrecommendedOffers'] = $this->getOffers($defaultRecommendedOffers, $request->getSchemeAndHttpHost());
        $data['defaultalternativeOffers'] = $this->getOffers($defaultAlternativeOffers, $request->getSchemeAndHttpHost());

        return new JsonResponse($data, 200);


    }
}