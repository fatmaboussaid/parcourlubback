<?php

namespace App\Controller\Api;

use App\Entity\Search;
use App\Entity\User;
use App\Repository\OfferRepository;
use App\Repository\SearchRepository;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/search")
 */
class SearchController extends AbstractController
{

    /**
     * @Route("/addSearch", name="addSearch", methods={"POST"})
     */
    public function addSearch(Request $request): Response
    {
        $search=new Search();
        $search->setMarque($request->get('marque',''));
        $search->setModele($request->get('modele',''));
        $search->setYear($request->get('year',''));
        $search->setCarburant($request->get('carburant',''));
        $search->setAddress($request->get('address',''));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($search);
        $entityManager->flush();
        return new JsonResponse($search->getId(), 200);


    }
    /**
     * @Route("/steps", name="steps", methods={"POST"})
     */
    public function steps(Request $request,SearchRepository $searchRepository): Response
    {
        $stepNumber=$request->get('stepNumber','');
        $step=$request->get('step','');

        $id=$request->get('id','');

        $search=$searchRepository->find($id);
        if($search){
            if($stepNumber ==1)$search->setStep1($step);
            if($stepNumber ==2)$search->setStep2($step);
            if($stepNumber ==3)$search->setStep3($step);
            if($stepNumber ==4)$search->setStep4($step);
            if($stepNumber ==5)$search->setStep5($step);
            $this->getDoctrine()->getManager()->flush();
        }

        return new JsonResponse('yes', 200);
    }


}