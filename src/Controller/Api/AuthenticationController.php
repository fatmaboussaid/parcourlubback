<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\ResetPasswordRequestFormType;
use App\Repository\UserRepository;
use App\Service\SendEmailService;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\ResetPassword\Exception\ResetPasswordExceptionInterface;
use SymfonyCasts\Bundle\ResetPassword\ResetPasswordHelperInterface;


/**
 * @Route("/authentication")
 */
class AuthenticationController extends AbstractController
{
    /**
     * @Route("/GeneRateT0ken", name="GeneRateT0ken", methods={"POST"})
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function generateToken(UserPasswordEncoderInterface $encoder, UserRepository $userRepository, Request $request, JWTTokenManagerInterface $JWTManager, JWTEncoderInterface $JWTEncoder): Response
    {

        $entityManager = $this->getDoctrine()->getManager();

        $message = "error";
        $token = null;
        $statut = 401;
        $email = $request->get("email", "");
        $password = $request->get("password", "");
        $user = $userRepository->findOneBy(["email" => $email, "isDeleted" => false]);
        //   exec("echo generatettoken: $username >> tokenAuth.txt");

        if ($user) {


            //if password valid
            if (!$user->getIsActive()) {
                $message = "error";
                $data = array("code" => 2, "data" => "Le compte est désactivé.", "token" => $token);
                $statut = 200;
            } elseif ($user->getIsDeleted()) {
                $message = "error";
                $data = array("code" => 3, "data" => "Le compte est supprimer.", "token" => $token);
                $statut = 200;
            } else if ($encoder->isPasswordValid($user, $password) and ($user->hasRole('ROLE_CLIENT'))) {
                $token = $JWTEncoder->encode([
                    'email' => $email,
                    // 'exp' => time() + 3600 // 1 hour expiration

                ]);
                $data = array("code" => 5, "data" => "Le compte est active.", "token" => $token);
                $statut = 200;
                $message = "success";
            } else {
                $message = "error";
                $data = array("code" => 4, "data" => "password is incorrect", "token" => $token);
                $statut = 200;
            }

        } else {

            $message = "error";
            $data = array("code" => 1, "data" => "username is incorrect", "token" => $token);
            $statut = 200;
        }
        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);
    }

    /**
     * @Route("/login", name="login", methods={"POST"})
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function login(JWTEncoderInterface $JWTEncoder, JWTTokenManagerInterface $JWTManager, Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): Response
    {
        $message = "error";
        $statut = 401;
        $email = "";
        $token = $request->get('Authorization');
        if ($token) {
            try {

                $data = $JWTEncoder->decode($token);
                $email = $data['email'];

            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = array("code" => 5, "data" => $message, 'token' => $token);
                $statut = 200;
            }

            $user = $userRepository->findOneBy(["email" => $email, "isDeleted" => false]);

            if ($user) {

                //if password valid
                if (!$user->getIsActive()) {
                    $message = "error";
                    $data = array("code" => 3, "data" => "Le compte est désactivé.");
                    $statut = 200;
                }
                else if ($user->hasRole('ROLE_CLIENT')) {
                    $statut = 200;
                    $message = "success";
                    $data = array(
                        "code" => 5,
                        "user" => [
                            "id" => $user->getId(),
                            "username" => $user->getUsername(),
                            "tel" => $user->getTel(),
                            "firstName" => $user->getFirstName(),
                            "lastName" => $user->getLastName(),
                            "email" => $user->getEmail(),
                        ]
                    );

                }
                else {
                    $message = "error";
                    $data = array("code" => 4, "data" => "password is incorrect");
                    $statut = 200;
                }

            } else {

                $message = "error";
                $data = array("code" => 2, "data" => "username is incorrect");
                $statut = 200;
            }
        } else {
            $message = "Invalid Token";
            $data = array("code" => 1, "data" => $message, 'token' => $token);
            $statut = 200;
        }


        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);


    }

    /**
     * @Route("/RefreshT0ken", name="RefreshT0ken", methods={"POST"})
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function RefreshToken(Request $request, UserRepository $userRepository, JWTEncoderInterface $JWTEncoder)
    {
        $token = null;
        $message = "user not found";
        $statut = 404;

        $tokenAuth = $request->get('Authorization');


        if ($tokenAuth) {
            $tokenParts = explode(".", $tokenAuth);
            if (sizeof($tokenParts) > 1) {
                $tokenHeader = base64_decode($tokenParts[0]);
                $tokenPayload = base64_decode($tokenParts[1]);
                $jwtHeader = json_decode($tokenHeader);
                $jwtPayload = json_decode($tokenPayload, true);
                //  $refresh_token = $tokenPayload->data->refresh_token;
                $email = $jwtPayload['email'];

                $entityManager = $this->getDoctrine()->getManager();

                $user = $userRepository->findOneBy(["email" => $email, "isDeleted" => false]);
                if ($user) {
                    if (!$user->getIsActive()) {
                        $message = "error";
                        $data = array("code" => 1, "data" => "Le compte est désactivé.");
                        $statut = 200;
                    }
                    else if (!$user->hasRole('ROLE_CLIENT')) {
                        $message = "error";
                        $data = array("code" => 2, "data" => "user not client");
                        $statut = 200;
                    }
                    else  {
                        $token = $JWTEncoder->encode([
                            'email' => $email,
                            // 'exp' => time() + 3600 // 1 hour expiration

                        ]);
                        $statut = 200;
                        $message = "success";
                        $data = array(
                            "code" => 3,
                            "user" => [
                                "id" => $user->getId(),
                                "username" => $user->getUsername(),
                                "tel" => $user->getTel(),
                                "firstName" => $user->getFirstName(),
                                "lastName" => $user->getLastName(),
                                "email" => $user->getEmail(),
                            ],
                            'token' => $token
                        );

                    }




                } else {
                    $statut = 200;
                    $token = $tokenAuth;
                    $message = "User Not Found";
                    $data = ['token' => $token, 'code' => 4];

                }
            } else {
                $statut = 200;
                $token = $tokenAuth;
                $message = "Invalid Token";
                $data = [ 'code' => 5,'token' => $token];

            }

        } else {
            $statut = 200;
            $token = $tokenAuth;
            $message = "There is no token sent";
            $data = [ 'code' => 6,'token' => $token];

        }

        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);

    }

    /**
     * @Route("/inscription", name="inscription", methods={"POST"})
     */
    public function inscription(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder, JWTEncoderInterface $JWTEncoder): Response
    {
        $message = "error";
        $statut = 401;
        $email = trim($request->get('email', ''));
        $tel = trim($request->get('tel', ''));
        $firstName = $request->get('firstName', '');
        $lastName = $request->get('lastName', '');
        $password = $request->get('password', '');
        $acceptSms = $request->get('acceptSms', '');
        $acceptEmail = $request->get('acceptEmail', '');

        if (($email == '') || ($tel == '') || ($password == '') || ($firstName == '')) {
            $message = "error";
            $data = array("code" => 1, "data" => "Incomplete data", [
                "email" => json_encode($request),

            ]);
            $statut = 200;
        } else {
            $user = $userRepository->findOneBy(["email" => $email]);
            if ($user) {
                $message = "error";
                $data = array("code" => 2, "data" => "Cet email est déja utilisé");
                $statut = 200;
            } else {
                $user = new User();
                $user->setEmail($email);
                $user->setUsername($email);
                $user->setTel($tel);
                $user->setFirstName($firstName);
                $user->setLastName($lastName);
                $user->setAcceptEmail($acceptEmail);
                $user->setAcceptSms($acceptSms);
                $user->setRoles(array('ROLE_CLIENT'));
                $user->setPassword($encoder->encodePassword($user, $password));
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                $token = $JWTEncoder->encode([
                    'email' => $email,
                    // 'exp' => time() + 3600 // 1 hour expiration

                ]);
                $data = array("code" => 3, "token" => $token,
                    "user" => [
                        "id" => $user->getId(),
                        "username" => $user->getUsername(),
                        "tel" => $user->getTel(),
                        "firstName" => $user->getFirstName(),
                        "lastName" => $user->getLastName(),
                        "email" => $user->getEmail(),
                    ]);
                $statut = 200;
                $message = "success";


                //$message = "success";
                // $data = array("code" => 3, "data" =>$email);
                //  $statut = 200;
            }


        }


        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);


    }

    /**
     *
     * @Route("/forget_password", name="forgot_password_request")
     */
    public function request(Request $request, MailerInterface $mailer,SendEmailService $sendEmailService,ResetPasswordHelperInterface $resetPasswordHelper): Response
    {
        $email=$request->get('email');
        if($email != ''){

        }
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
            'email' => $email,
        ]);

        // Do not reveal whether a user account was found or not.
        if (!$user) {
            $message = "error";
            $data = array("code" => 1, "data" => "user not found", [
                "email" => $email,
                ]);
            $statut = 200;
        }
        else{

            try {
                $resetToken =$resetPasswordHelper->generateResetToken($user);

            } catch (ResetPasswordExceptionInterface $e) {
                // If you want to tell the user why a reset email was not sent, uncomment
                // the lines below and change the redirect to 'app_forgot_password_request'.
                // Caution: This may reveal if a user is registered or not.

                $message = "error";
                $data = array("code" =>2, "data" => $e->getReason(), [
                    "email" => $email,
                ]);
                $statut = 200;
                return new JsonResponse(array(
                    'data' => $data,
                    'statut' => $statut
                ), $statut);

            }
            $result= $sendEmailService->sendEmails($user->getEmail(),'Site www.mavidange.tn rénitialisation de votre mot de passe',
                'reset_password/emailClient.html.twig',[ 'resetToken' => $resetToken, 'user'=>$user]);
            $data = array("code" => 3, "data" => "verifier votre email", [
                "email" => $email,'result'=>$result
            ]);
            $statut = 200;
        }




        // Store the token object in session for retrieval in check-email route.
        //$this->setTokenObjectInSession($resetToken);

        return new JsonResponse(array(
            'data' => $data,
            'statut' => $statut
        ), $statut);

    }
    /**
     *
     * @Route("/reset_password", name="reset_password", methods={"POST"})
     */
    public function reset(Request $request,ResetPasswordHelperInterface $resetPasswordHelper, UserPasswordEncoderInterface $passwordEncoder, string $token = null): Response
    {
        $token=$request->get('token');

        if ($token) {
            try {
                $user = $resetPasswordHelper->validateTokenAndFetchUser($token);
            } catch (ResetPasswordExceptionInterface $e) {
                $message = "error";
                $data = array("code" =>1, "data" => $e->getReason(),
                    "token" => $token,
                );
                $statut = 200;
                return new JsonResponse(array(
                    'data' => $data,
                    'statut' => $statut
                ), $statut);
            }
            $resetPasswordHelper->removeResetRequest($token);
            $password= $request->get('password');
            $confirmpassword= $request->get('confirmpassword');

            // Encode the plain password, and set it.
            $encodedPassword = $passwordEncoder->encodePassword(
                $user,
                $password
            );

            $user->setPassword($encodedPassword);
            $this->getDoctrine()->getManager()->flush();
            $message = "error";
            $data = array("code" =>3, "data" =>"c fait", [
                "token" => $token,
            ]);
            $statut = 200;
        }
        else{
            $message = "error";
            $data = array("code" =>2, "data" =>"No reset password token found in the URL or in the session.", [
                "token" => $token,
            ]);
            $statut = 200;
        }

        return new JsonResponse(array(
            'data' => $data,
            'statut' => $statut
        ), $statut);


    }


    /**
     * @Route("/updateProfile", name="updateProfile", methods={"POST"})
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function updateProfile(JWTEncoderInterface $JWTEncoder, JWTTokenManagerInterface $JWTManager, Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository): Response
    {
        $message = "error";
        $statut = 401;
        $email = "";
        $token = $request->get('Authorization');
        if ($token) {
            try {

                $data = $JWTEncoder->decode($token);
                $email = $data['email'];

            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = array("code" => 5, "data" => $message, 'token' => $token);
                $statut = 200;
            }

            $user = $userRepository->findOneBy(["email" => $email, "isDeleted" => false]);
            $userEmail=$userRepository->findOneBy(["email" => $request->get('email')]);

            if ($user) {
                if ($userEmail && $user->getId()!= $userEmail->getId()) {
                    $message = "error";
                    $data = array("code" => 6, "data" => "email deja existe.");
                    $statut = 200;
                }
                else if (!$user->getIsActive()) {
                    $message = "error";
                    $data = array("code" => 3, "data" => "Le compte est désactivé.");
                    $statut = 200;
                }
                else if ($user->hasRole('ROLE_CLIENT')) {
                    $user->setFirstName(trim($request->get('firstName')));
                    $user->setEmail(trim($request->get('email')));
                    $user->setTel(trim($request->get('tel')));
                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->flush();
                    $statut = 200;
                    $message = "success";
                    $data = array(
                        "code" => 5,
                        "user" => [
                            "id" => $user->getId(),
                            "username" => $user->getUsername(),
                            "tel" => $user->getTel(),
                            "firstName" => $user->getFirstName(),
                            "lastName" => $user->getLastName(),
                            "email" => $user->getEmail(),
                        ]
                    );

                }
                else {
                    $message = "error";
                    $data = array("code" => 4, "data" => "password is incorrect");
                    $statut = 200;
                }

            } else {

                $message = "error";
                $data = array("code" => 2, "data" => "username is incorrect");
                $statut = 200;
            }
        } else {
            $message = "Invalid Token";
            $data = array("code" => 1, "data" => $message, 'token' => $token);
            $statut = 200;
        }


        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);


    }
    /**
     * @Route("/updatePassword", name="updatePassword", methods={"POST"})
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function updatePassword( UserPasswordEncoderInterface $encoder,JWTEncoderInterface $JWTEncoder, JWTTokenManagerInterface $JWTManager, Request $request,  UserRepository $userRepository): Response
    {
        $message = "error";
        $statut = 401;
        $email = "";
        $token = $request->get('Authorization');
        if ($token) {
            try {

                $data = $JWTEncoder->decode($token);
                $email = $data['email'];

            } catch (\Exception $e) {
                $message = $e->getMessage();
                $data = array("code" => 1, "data" => $message, 'token' => $token);
                $statut = 200;
            }

            $user = $userRepository->findOneBy(["email" => $email, "isDeleted" => false]);

            if ($user) {
                if (!$user->getIsActive()) {
                    $message = "error";
                    $data = array("code" => 2, "data" => "Le compte est désactivé.");
                    $statut = 200;
                }
                else if ($user->hasRole('ROLE_CLIENT')) {
                    $oldpassword=$request->get('oldpassword');
                    $newpassword=$request->get('newpassword');
                    if( !$encoder->isPasswordValid($user,$oldpassword)){
                        $message = "error";
                        $data = array("code" => 3, "data" => "Votre ancien mot de passe est erroné");
                        $statut = 200;
                    }
                    else{
                        $user->setPassword($encoder->encodePassword($user,$newpassword));
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->flush();
                        $statut = 200;
                        $message = "success";
                        $data = array(
                            "code" => 4,
                            "data" => "success"
                        );
                    }




                }
                else {
                    $message = "error";
                    $data = array("code" => 5, "data" => "password is incorrect");
                    $statut = 200;
                }

            }
            else {

                $message = "error";
                $data = array("code" => 6, "data" => "username is incorrect");
                $statut = 200;
            }
        }
        else {
            $message = "Invalid Token";
            $data = array("code" => 7, "data" => $message, 'token' => $token);
            $statut = 200;
        }


        return new JsonResponse(array(
            'message' => $message,
            'data' => $data,
            'statut' => $statut
        ), $statut);


    }

}