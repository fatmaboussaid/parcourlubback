<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\OfferRepository;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/station")
 */
class StationController extends AbstractController
{

    private $url='';



    public function __construct()
    {


    }

    /**
     * @Route("/listStations", name="listStations", methods={"GET"})
     */
    public function listStations(Request $request, StationRepository $stationRepository): Response
    {
        $this->url = $request->getSchemeAndHttpHost().'/uploads/images/';
        $isOilChange=$request->get('isOilChange','false');
        $latitude=$request->get('latitude','');
        $longitude=$request->get('longitude','');


        $data = $stationRepository->findActiveStations('',$isOilChange,$latitude,$longitude);



        return new JsonResponse($this->getStations($data,$this->url), 200);


    }
    /**
     * @Route("/getStation", name="getStation", methods={"GET"})
     */
    public function findStation(Request $request, StationRepository $stationRepository): Response
    {
        $id=$request->get('id');
        $this->url = $request->getSchemeAndHttpHost().'/uploads/images/';

        $station = $stationRepository->findActiveStation($id);
        return new JsonResponse( $this->getStation($station,$this->url), 200);


    }
    public function getStations($data,$url)
    {
        $stations = [];
        foreach ($data as $station) {
            if(array_key_exists('distance', $station))
                $station=$station[0];
            if($station->getIsActive()== true && $station->getIsDeleted()== false){
                array_push($stations, $this->getStation($station,$url));
            }
        }
        return $stations;
    }
    public function getStation($data,$url)
    {
        $station=[];
            if($data->getIsActive()== true && $data->getIsDeleted()== false){
               $station=[
                   'id' => $data->getId(),
                   'name' => $data->getName(),
                   'address' => $data->getAddress(),
                   'tel' => $data->getTel(),
                   'horaires' => $data->getHoraires(),
                   'image' => $url.$data->getImage(),
                   'isClosedSunday' => $data->getIsClosedSunday(),
                   'isOilChange' => $data->getIsOilChange(),
                   'prestationStations' => $this->getPrestationsStation($data->getPrestationStations(),$url),
               ];
            }


        return $station;
    }
    public function getPrestationsStation($data,$url)
    {
        $prestations = [];
        foreach ($data as $prestation) {
            if(($prestation->getPrestation()->getIsActive()== true && $prestation->getPrestation()->getIsDeleted()== false)){
                array_push($prestations, [
                    'id' => $prestation->getId(),
                    'prestation' => $this->getPrestation($prestation->getPrestation(), $url)
                ]);
            }

        }
        return $prestations;
    }
    public function getPrestation($data,$url)
    {
        if($data->getIsActive()== true && $data->getIsDeleted()== false) {
            return [
                'id' => $data->getId(),
                'name' => $data->getName(),
                'icon'=>$url.$data->getIcon()

            ];
        }
        else{
            return [];
        }


    }

}