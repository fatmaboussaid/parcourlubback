<?php

namespace App\Controller;

use App\Entity\PrestationStation;
use App\Form\PrestationStationType;
use App\Repository\PrestationRepository;
use App\Repository\PrestationStationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/prestation/station")
 */
class PrestationStationController extends AbstractController
{
    /**
     * @Route("/", name="prestation_station_index", methods={"GET"})
     */
    public function index(PrestationStationRepository $prestationStationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $keyword = $request->get('keyword', '');
        $page = $request->get('page', '1');


        $prestation_stations = $paginator->paginate(
        // Doctrine Query, not results
            $prestationStationRepository->findPrestationsStation($keyword, $this->getUser()->getStation(), "", false),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        return $this->render('prestation_station/index.html.twig', [
            'prestation_stations' => $prestation_stations,
            'keyword' => $keyword,

        ]);
    }
    /**
     * @Route("/getPrestations", name="getPrestations", methods={"GET"})
     */
    public function getPrestations(Request $request, PrestationStationRepository $prestationStationRepository)
    {
        $station = $request->get('station');
        $prestation_stations = $prestationStationRepository->findPrestationsStation('', $station, "", false);
        $responseArray['prestations']=[];
        foreach ($prestation_stations as $prestation_station) {
            $responseArray['prestations'][] = array(
                "id" => $prestation_station->getPrestation()->getId(),
                "name" => htmlspecialchars($prestation_station->getPrestation()->getName())
            );
        }

        return new JsonResponse($responseArray);


    }

    /**
     * @Route("/delete/{id}", name="prestation_station_delete", methods={"GET"})
     */
    public function delete(Request $request, PrestationStation $prestationStation): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($prestationStation);
        $entityManager->flush();
        return $this->redirectToRoute('prestation_station_index');
    }
}
