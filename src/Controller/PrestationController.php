<?php

namespace App\Controller;

use App\Entity\Prestation;
use App\Entity\PrestationStation;
use App\Form\PrestationType;
use App\Repository\PrestationRepository;
use App\Repository\PrestationStationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

/**
 * @Route("/prestation")
 */
class PrestationController extends AbstractController
{
    /**
     * @Route("/", name="prestation_index", methods={"GET"})
     */
    public function index(PrestationRepository $prestationRepository,PrestationStationRepository $prestationStationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $keyword = $request->get('keyword', '');
        $page = $request->get('page', '1');
        $station = '';
        if ($this->getUser()->hasRole('ROLE_GERANT')) {
            //$station = $this->getUser()->getStation()->getId();
        }
        $result=$prestationRepository->findPrestations($keyword, $station);
        if ($this->getUser()->hasRole('ROLE_GERANT')) {
            $station = $this->getUser()->getStation()->getId();
            $prestation_stations = $prestationStationRepository->findPrestationsStation('', $station, "", false);
            foreach ($prestation_stations as $prestationStation) {
                $key = array_search($prestationStation->getPrestation(), $result);
                if ($key !== false) {
                    unset($result[$key]);
                }
            }
        }


        $prestations = $paginator->paginate(
        // Doctrine Query, not results
            $result,
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        return $this->render('prestation/index.html.twig', [
            'prestations' => $prestations,
            'keyword' => $keyword,

        ]);

    }



    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }

    /**
     * @Route("/new", name="prestation_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $prestation = new Prestation();
        $form = $this->createForm(PrestationType::class, $prestation);
        $form->add('icon', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => true,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $icon = $form['icon']->getData();
            if ($icon) {
                $originalFilename = pathinfo($icon->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $icon->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $prestation->setIcon($newFilename);
            }
            $entityManager = $this->getDoctrine()->getManager();
            //$prestation->setStation($this->getUser()->getStation());
            $entityManager->persist($prestation);
            $entityManager->flush();

            return $this->redirectToRoute('prestation_index');
        }

        return $this->render('prestation/new.html.twig', [
            'prestation' => $prestation,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())
        ]);
    }


    /**
     * @Route("/{id}/edit", name="prestation_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Prestation $prestation): Response
    {
        $form = $this->createForm(PrestationType::class, $prestation);
        $form->add('icon', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $icon = $form['icon']->getData();
            if ($icon) {
                $originalFilename = pathinfo($icon->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $icon->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $prestation->setIcon($newFilename);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('prestation_index');
        }

        return $this->render('prestation/edit.html.twig', [
            'prestation' => $prestation,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())
        ]);
    }

    /**
     * @Route("/station/add/{id}", name="prestation_add", methods={"GET","POST"})
     */
    public function add(Request $request, Prestation $prestation): Response
    {
        $prestation_station = new PrestationStation();
        $prestation_station->setPrestation($prestation);
        $prestation_station->setStation($this->getUser()->getStation());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($prestation_station);
        $entityManager->flush();
        return $this->redirectToRoute('prestation_station_index');
    }

    /**
     * @Route("/delete/{id}", name="prestation_delete", methods={"GET"})
     */
    public function delete(Request $request, Prestation $prestation): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $prestation->setIsDeleted(true);
        $entityManager->flush();


        return $this->redirectToRoute('prestation_index');
    }
}
