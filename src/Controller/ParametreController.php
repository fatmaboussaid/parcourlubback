<?php

namespace App\Controller;

use App\Entity\Parametre;
use App\Form\ParametreDeviseType;
use App\Form\ParametreLanguageType;
use App\Form\ParametreSmsType;
use App\Repository\ParametreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/parametre")
 */
class ParametreController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function languageMenu(ParametreRepository $parametreRepository)
    {

        $parametre=$parametreRepository->find(1);
        if(!$parametre){
            $parametre = new Parametre();
        }
        return $this->render('block/navbar.html.twig',[

            "parametre" => $parametre
        ]);
    }
    /**
     * @Route("/translate", name="fr_en")
     */
    public function translationAction(Request $request)
    {

        $to = $request->get('to', 'fr');


        // some logic to determine the $locale
        $this->session->set('_locale', $to);

    /*    $user = $this->getUser();
        $user->setPreferedLanguage($to);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
*/
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);

        //return $this->redirectToRoute('site_homepage');

    }
    /**
     * @Route("/parametre_language", name="parametre_language", methods={"GET","POST"})
     */
    public function parametreLanguage(Request $request,ParametreRepository $parametreRepository): Response
    {
        $parametre=$parametreRepository->find(1);
        if(!$parametre){
            $parametre = new Parametre();
        }
        $form = $this->createForm(ParametreLanguageType::class, $parametre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if(!in_array(  $this->session->get('_locale'),$parametre->getLanguage())){
                $key=key($parametre->getLanguage());
                $this->session->set('_locale', $parametre->getLanguage()[$key]);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parametre);
            $entityManager->flush();

            return $this->redirectToRoute('parametre_language');
        }

        return $this->render('parametre/language.html.twig', [
            'parametre' => $parametre,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }
    /**
     * @Route("/parametre_devise", name="parametre_devise", methods={"GET","POST"})
     */
    public function parametreDevise(Request $request,ParametreRepository $parametreRepository): Response
    {
        $parametre=$parametreRepository->find(1);
        if(!$parametre){
            $parametre = new Parametre();
        }
        $form = $this->createForm(ParametreDeviseType::class, $parametre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parametre);
            $entityManager->flush();

            return $this->redirectToRoute('parametre_devise');
        }

        return $this->render('parametre/devise.html.twig', [
            'parametre' => $parametre,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }
    /**
     * @Route("/parametre_sms", name="parametre_sms", methods={"GET","POST"})
     */
    public function parametreSMS(Request $request,ParametreRepository $parametreRepository): Response
    {
        $parametre=$parametreRepository->find(1);
        if(!$parametre){
            $parametre = new Parametre();
        }
        $form = $this->createForm(ParametreSmsType::class, $parametre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parametre);
            $entityManager->flush();

            return $this->redirectToRoute('parametre_sms');
        }

        return $this->render('parametre/sms.html.twig', [
            'parametre' => $parametre,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }

    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }



}
