<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Search;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use App\Repository\ProductRepository;
use App\Repository\SearchRepository;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/search")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/", name="search_index", methods={"GET"})
     */
    public function index(SearchRepository $searchRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $keyword = $request->get('keyword', '');
        $dateDebut = $request->get('dateDebut', '');
        $dateFin = $request->get('dateFin', '');
        $page = $request->get('page', '1');
        $searchs = $paginator->paginate(
        // Doctrine Query, not results
            $searchRepository->findAllSearchs($keyword,$dateDebut,$dateFin),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );

        return $this->render('search/index.html.twig', [
            'searchs' => $searchs,
            'keyword' => $keyword,
            'dateDebut' => $dateDebut,
            'dateFin' => $dateFin,
        ]);
    }



    /**
     * @Route("/delete/{id}", name="search_delete", methods={"GET"})
     */
    public function delete($id,Request $request,SearchRepository $searchRepository): Response
    {
        $search=$searchRepository->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($search);
        $entityManager->flush();
        return $this->redirectToRoute('search_index');
    }

}
