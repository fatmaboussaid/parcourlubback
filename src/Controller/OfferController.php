<?php

namespace App\Controller;

use App\Entity\Offer;
use App\Entity\Station;
use App\Form\OfferType;
use App\Repository\OfferRepository;
use App\Repository\ParametreRepository;
use App\Repository\ProductRepository;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/offer")
 */
class OfferController extends AbstractController
{
    /**
     * @Route("/", name="offer_index", methods={"GET"})
     */
    public function index(ParametreRepository $parametreRepository, OfferRepository $offerRepository,ProductRepository $productRepository, StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $keyword = $request->get('keyword', '');
        $station = $request->get('station', '');
        $id = $request->get('id', '');
        $page = $request->get('page', '1');
        $stations = $stationRepository->findStations();
        if($this->getUser()->hasRole('ROLE_GERANT')){
            $station=$this->getUser()->getStation()->getId();
        }
        $offers = $paginator->paginate(
        // Doctrine Query, not results
            $offerRepository->findOffers($keyword,$station,$id),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        $parametre=$parametreRepository->find(1);

        return $this->render('offer/index.html.twig', [
            'offers' => $offers,
            'stations' => $stations,
            'station' => $station,
            'keyword' => $keyword,
            'id' => $id,
            'parametre' => $parametre,

        ]);
    }
    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }
    /**
     * @Route("/new", name="offer_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $offer = new Offer();
        $form = $this->createForm(OfferType::class, $offer);
        $form->add('image', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => true,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        if($this->getUser()->hasRole('ROLE_ADMIN')) {
            $form->add('stations', EntityType::class, array(
                    'class' => Station::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->where('s.isActive=true and s.isDeleted = false');
                    },
                    'placeholder' => 'choisir une station',
                    'attr' => ['data-plugin' => 'select2'],
                )
            );
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form['image']->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $offer->setImage($newFilename);
            }


            if($this->getUser()->hasRole('ROLE_ADMIN')){
                //$stations=$offer->getStations();
               // foreach ($stations as $station) {
              //      $offer->addStation($station);
              //  }
            }else{
                $offer->addStation($this->getUser()->getStation());
            }
           // dump($offer);die;
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($offer);
            $entityManager->flush();

            return $this->redirectToRoute('offer_index');
        }

        return $this->render('offer/new.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }


    /**
     * @Route("/{id}/edit", name="offer_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Offer $offer): Response
    {
        $form = $this->createForm(OfferType::class, $offer);
        $form->add('image', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        if($this->getUser()->hasRole('ROLE_ADMIN')) {
            $form->add('stations', EntityType::class, array(
                    'class' => Station::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->where('s.isActive=true and s.isDeleted = false');
                    },
                    'placeholder' => 'choisir une station',
                    'attr' => ['data-plugin' => 'select2'],
                )
            );
            }


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form['image']->getData();

            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $offer->setImage($newFilename);
            }

            if($this->getUser()->hasRole('ROLE_ADMIN')){
              /*  $stations=$offer->getStations();
                foreach ($stations as $station) {
                    $offer->removeStation($station);
                }
                $stations=$offer->getProduct()->getStations();
                foreach ($stations as $station) {
                    $offer->addStation($station);
                }*/
            }


            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('offer_index');
        }

        return $this->render('offer/edit.html.twig', [
            'offer' => $offer,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }

    /**
     * @Route("/delete/{id}", name="offer_delete", methods={"GET"})
     */
    public function delete(Request $request, Offer $offer): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return $this->redirectToRoute('offer_index');
    }
    /**
     * @Route("/export", name="offer_export", methods={"GET"})
     */
    public function export(UserRepository $userRepository, Request $request, TranslatorInterface $translator): Response
    {
        $keyword = $request->get('keyword', '');
        $station = $request->get('station', '');
        $items = $userRepository->findUsers($keyword, $station, 'ROLE_GERANT');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Gérants");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValue('A1', $translator->trans("Nom"));
        $sheet->setCellValue('B1', $translator->trans("Email"));
        $sheet->setCellValue('C1', $translator->trans("Téléphone"));
        $sheet->setCellValue('D1', $translator->trans("Station"));

        $i = 2;
        foreach ($items as $item) {
            $sheet->setCellValue('A' . $i, $item->getUsername());
            $sheet->setCellValue('B' . $i, $item->getEmail());
            $sheet->setCellValue('C' . $i, $item->getTel());
            $sheet->setCellValue('D' . $i, $item->getStation()->getName());
            $i++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $translator->trans("Gérants") . '.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
