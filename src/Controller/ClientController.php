<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/client")
 */
class ClientController extends AbstractController
{
    /**
     * @Route("/", name="client_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository,StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $keyword=$request->get('keyword', '');
        $acceptSMS=$request->get('acceptSMS', '');
        $acceptEmail=$request->get('acceptEmail', '');

        $page = $request->get('page', '1');
        $clients = $paginator->paginate(
        // Doctrine Query, not results
            $userRepository->findUsers($keyword,'','ROLE_CLIENT',$acceptSMS,$acceptEmail),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        return $this->render('client/index.html.twig', [
            'clients' => $clients,
            'keyword' => $keyword,
            'acceptSMS' => $acceptSMS,
            'acceptEmail' => $acceptEmail,

        ]);

    }



    /**
     * @Route("/client_export", name="client_export", methods={"GET"})
     */
    public function export(UserRepository $userRepository, Request $request, TranslatorInterface $translator): Response
    {
        $keyword = $request->get('keyword', '');
        $acceptSMS=$request->get('acceptSMS', '');
        $acceptEmail=$request->get('acceptEmail', '');
        $items =   $userRepository->findUsers($keyword,'','ROLE_CLIENT',$acceptSMS,$acceptEmail);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Gérants");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);


        $sheet->setCellValue('A1', $translator->trans("Nom"));
        $sheet->setCellValue('B1', $translator->trans("Email"));
        $sheet->setCellValue('C1', $translator->trans("Téléphone"));
        $sheet->setCellValue('D1', $translator->trans("Accepte de recevoir des SMS"));
        $sheet->setCellValue('E1', $translator->trans("Accepte de recevoir des Emails"));

        $i = 2;
        foreach ($items as $item) {
            $sheet->setCellValue('A' . $i, $item->getUsername());
            $sheet->setCellValue('B' . $i, $item->getEmail());
            $sheet->setCellValue('C' . $i, $item->getTel());

            if($item->getAcceptSms())$sheet->setCellValue('D' . $i, $translator->trans("OUI"));
            else  $sheet->setCellValue('D' . $i, $translator->trans("NON"));


            if($item->getAcceptEmail())$sheet->setCellValue('E' . $i, $translator->trans("OUI"));
            else  $sheet->setCellValue('E' . $i, $translator->trans("NON"));
            $i++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $translator->trans("Clients") . '.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
