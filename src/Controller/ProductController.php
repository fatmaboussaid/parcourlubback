<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Station;
use App\Form\ProductType;
use App\Repository\ParametreRepository;
use App\Repository\ProductRepository;
use App\Repository\StationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index", methods={"GET"})
     */
    public function index(ParametreRepository $parametreRepository, ProductRepository $productRepository, StationRepository $stationRepository, Request $request, PaginatorInterface $paginator): Response
    {

        $keyword = $request->get('keyword', '');
        $station = $request->get('station', '');

        $page = $request->get('page', '1');
        $stations = $stationRepository->findStations();

        $products = $paginator->paginate(
        // Doctrine Query, not results
            $productRepository->findProducts($keyword,$station),
            // Define the page parameter
            $request->query->getInt('page', $page),
            // Items per page
            10
        );
        $parametre=$parametreRepository->find(1);

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'keyword' => $keyword,
            'stations' => $stations,
            'station' => $station,
            'parametre' => $parametre,

        ]);
    }

    private function getErrors($baseForm, $baseFormName)
    {
        $errors = array();
        if ($baseForm instanceof \Symfony\Component\Form\Form) {


            foreach ($baseForm->getErrors() as $key => $error) {

                $errors[] = $error->getMessage();
            }
            foreach ($baseForm->all() as $key => $child) {


                if (($child instanceof \Symfony\Component\Form\Form)) {
                    $cErrors = $this->getErrors($child, $baseFormName . "_" . $child->getName());
                    $errors = array_merge($errors, $cErrors);
                }

            }


        }
        return $errors;
    }

    public function getProductsLubricant(): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://lubricant.localbeta.ovh/app_dev.php/getProductsBycounty?country_code=TN');
        $result = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result);

        return $data->products;
    }

    /**
     * @Route("/new", name="product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        if($this->getUser()->hasRole('ROLE_ADMIN')) {
            $form->add('stations', EntityType::class, array(
                    'class' => Station::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->where('s.isActive=true and s.isDeleted = false');
                    },
                    'placeholder' => 'choisir une station',
                    'attr' => ['data-plugin' => 'select2'],
                )
            );
        }
        $form->add('image', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => true,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        $form->handleRequest($request);
        $products = $this->getProductsLubricant();
        if ($form->isSubmitted() && $form->isValid()) {
            $productLubricant = $request->get('productLubricant');
            if ($productLubricant != '' && $productLubricant != '0') $product->setIdLubricant($productLubricant);
            $image = $form['image']->getData();
            $file = $form['file']->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $product->setImage($newFilename);
            }
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.pdf';
                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('files_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $product->setFile($newFilename);
            }
            if($this->getUser()->hasRole('ROLE_ADMIN')){
                //$stations=$offer->getStations();
                // foreach ($stations as $station) {
                //      $offer->addStation($station);
                //  }
            }else{
                $product->addStation($this->getUser()->getStation());
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'products' => $products,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }


    /**
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        if($this->getUser()->hasRole('ROLE_ADMIN')) {
            $form->add('stations', EntityType::class, array(
                    'class' => Station::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('s')
                            ->where('s.isActive=true and s.isDeleted = false');
                    },
                    'placeholder' => 'choisir une station',
                    'attr' => ['data-plugin' => 'select2'],
                )
            );
        }
        $form->add('image', FileType::class, [
            'label' => false,
            'attr' => ['placeholder' => 'choisir image'],
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/ief',
                    ],
                    'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                ])
            ],
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $form['image']->getData();
            $file = $form['file']->getData();
            if ($image) {
                $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.png';
                // Move the file to the directory where brochures are stored
                try {
                    $image->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $product->setImage($newFilename);
            }
            if ($file) {
                $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.pdf';
                // Move the file to the directory where brochures are stored
                try {
                    $file->move(
                        $this->getParameter('files_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $product->setFile($newFilename);
            }
            if($this->getUser()->hasRole('ROLE_ADMIN')){
                //$stations=$offer->getStations();
                // foreach ($stations as $station) {
                //      $offer->addStation($station);
                //  }
            }else{
                $product->addStation($this->getUser()->getStation());
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'errors' => $this->getErrors($form, $form->getName())

        ]);
    }

    /**
     * @Route("/delete/{id}", name="product_delete", methods={"GET"})
     */
    public function delete(Request $request, Product $product): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        if($this->getUser()->hasRole('ROLE_GERANT')){
            $station=$this->getUser()->getStation();
            $product->removeStation($station);
        }
        else{
            $product->setIsDeleted(true);
        }
        $entityManager->flush();


        return $this->redirectToRoute('product_index');
    }

    /**
     * @Route("/export", name="product_export", methods={"GET"})
     */
    public function export(ProductRepository $productRepository, Request $request, TranslatorInterface $translator): Response
    {
        $keyword = $request->get('keyword', '');
        $station = $request->get('station', '');
        $items = $userRepository->findUsers($keyword, $station, 'ROLE_GERANT');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Gérants");
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $sheet->setCellValue('A1', $translator->trans("Nom"));
        $sheet->setCellValue('B1', $translator->trans("Email"));
        $sheet->setCellValue('C1', $translator->trans("Téléphone"));
        $sheet->setCellValue('D1', $translator->trans("Station"));

        $i = 2;
        foreach ($items as $item) {
            $sheet->setCellValue('A' . $i, $item->getUsername());
            $sheet->setCellValue('B' . $i, $item->getEmail());
            $sheet->setCellValue('C' . $i, $item->getTel());
            $sheet->setCellValue('D' . $i, $item->getStation()->getName());
            $i++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = $translator->trans("Gérants") . '.xlsx';

        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        // Return the excel file as an attachment
        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

}
