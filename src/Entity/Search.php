<?php

namespace App\Entity;

use App\Repository\SearchRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SearchRepository::class)
 */
class Search
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modele;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carburant;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ipAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $step1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $step2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $step3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $step4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $step5;
    public function __construct()
    {
        $this->createAt=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getCarburant(): ?string
    {
        return $this->carburant;
    }

    public function setCarburant(?string $carburant): self
    {
        $this->carburant = $carburant;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getStep1(): ?string
    {
        return $this->step1;
    }

    public function setStep1(?string $step1): self
    {
        $this->step1 = $step1;

        return $this;
    }

    public function getStep2(): ?string
    {
        return $this->step2;
    }

    public function setStep2(?string $step2): self
    {
        $this->step2 = $step2;

        return $this;
    }

    public function getStep3(): ?string
    {
        return $this->step3;
    }

    public function setStep3(?string $step3): self
    {
        $this->step3 = $step3;

        return $this;
    }

    public function getStep4(): ?string
    {
        return $this->step4;
    }

    public function setStep4(?string $step4): self
    {
        $this->step4 = $step4;

        return $this;
    }

    public function getStep5(): ?string
    {
        return $this->step5;
    }

    public function setStep5(?string $step5): self
    {
        $this->step5 = $step5;

        return $this;
    }
}
