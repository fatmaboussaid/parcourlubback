<?php

namespace App\Entity;

use App\Repository\StationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=StationRepository::class)
 */
class Station
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="station")
     */
    private $users;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, mappedBy="stations")
     */
    private $products;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\ManyToMany(targetEntity=Offer::class, mappedBy="stations")
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity=Prestation::class, mappedBy="station")
     */
    private $prestations;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $horaires = [];
    public static $horaire_values = array(
         "de 8h à 10h" =>'1',
         "de 10h à 12h"=>'2',
         "de 12h à 14h"=>'3',
         "de 14h à 16h"=>'4',
         "de 16h à 18h"=>'5',
        "de 18h à 20h"=>'6',
    );

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOilChange;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isClosedSunday;

    /**
     * @ORM\OneToMany(targetEntity=Commande::class, mappedBy="station")
     */
    private $commandes;

    /**
     * @ORM\OneToMany(targetEntity=PrestationStation::class, mappedBy="station")
     */
    private $prestationStations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=6, nullable=true)
     * @Assert\Type(type="Numeric", message = "La valeur {{ value }} doit être de type decimal")
     */
    private $latitude;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=6, nullable=true)
     * @Assert\Type(type="Numeric", message = "La valeur {{ value }} doit être de type decimal")
     */
    private $longitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;





    public function __construct()
    {
        $this->setCreateAt(new \DateTime());
        $this->users = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->isActive=true;
        $this->isDeleted=false;
        $this->isOilChange=true;
        $this->isClosedSunday=true;
        $this->offers = new ArrayCollection();
        $this->prestations = new ArrayCollection();
        $this->commandes = new ArrayCollection();
        $this->prestationStations = new ArrayCollection();
    }
    public function getHoraireFromIndex($i)
    {
        return self::$horaire_values[$i];
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(?string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setStation($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getStation() === $this) {
                $user->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addStation($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            $product->removeStation($this);
        }

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->addStation($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->removeElement($offer)) {
            $offer->removeStation($this);
        }

        return $this;
    }

    /**
     * @return Collection|Prestation[]
     */
    public function getPrestations(): Collection
    {
        return $this->prestations;
    }

    public function addPrestation(Prestation $prestation): self
    {
        if (!$this->prestations->contains($prestation)) {
            $this->prestations[] = $prestation;
            $prestation->setStation($this);
        }

        return $this;
    }

    public function removePrestation(Prestation $prestation): self
    {
        if ($this->prestations->removeElement($prestation)) {
            // set the owning side to null (unless already changed)
            if ($prestation->getStation() === $this) {
                $prestation->setStation(null);
            }
        }

        return $this;
    }

    public function getHoraires(): ?array
    {
        return $this->horaires;
    }

    public function setHoraires(?array $horaires): self
    {
        $this->horaires = $horaires;

        return $this;
    }

    public function getIsOilChange(): ?bool
    {
        return $this->isOilChange;
    }

    public function setIsOilChange(?bool $isOilChange): self
    {
        $this->isOilChange = $isOilChange;

        return $this;
    }

    public function getIsClosedSunday(): ?bool
    {
        return $this->isClosedSunday;
    }

    public function setIsClosedSunday(?bool $isClosedSunday): self
    {
        $this->isClosedSunday = $isClosedSunday;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setStation($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getStation() === $this) {
                $commande->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PrestationStation[]
     */
    public function getPrestationStations(): Collection
    {
        return $this->prestationStations;
    }

    public function addPrestationStation(PrestationStation $prestationStation): self
    {
        if (!$this->prestationStations->contains($prestationStation)) {
            $this->prestationStations[] = $prestationStation;
            $prestationStation->setStation($this);
        }

        return $this;
    }

    public function removePrestationStation(PrestationStation $prestationStation): self
    {
        if ($this->prestationStations->removeElement($prestationStation)) {
            // set the owning side to null (unless already changed)
            if ($prestationStation->getStation() === $this) {
                $prestationStation->setStation(null);
            }
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }




}
