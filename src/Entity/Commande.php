<?php

namespace App\Entity;

use App\Repository\CommandeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommandeRepository::class)
 */
class Commande
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Offer::class, inversedBy="commandes")
     */
    private $offer;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="commandes")
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity=Prestation::class, inversedBy="commandes")
     */
    private $prestations;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="commandes")
     */
    private $station;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modele;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carburant;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfReceipt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $receptionPeriod;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $paymentStatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderStatus;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isCompleted;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="commandes")
     */
    private $user;

    private $orderStatusToString =[
        0 => "Non complète",
        1 => "Non traitée",
        2 => "En cours de traitement",
        3 => "Traitée",
        4 => "Livrée",
        5 => "Annuler",
    ];
    private $orderStatusCssClass=[
        0 => "badge-light",
        1 => "badge-secondary",
        2 => "badge-warning",
        3 => "badge-info",
        4 => "badge-success",
        5 => "badge-danger",
    ];

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createAt;

    public  function getStatusOrderToString():?string
    {
        return $this->orderStatusToString[$this->orderStatus];
    }


    public  function getStatusOrderCssClass(): ?string
    {
        return $this->orderStatusCssClass[$this->orderStatus];
    }

    public function __construct()
    {
        $this->prestations = new ArrayCollection();
        $this->paymentStatus=false;
        $this->orderStatus=0;
        $this->quantity=1;
        $this->createAt=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOffer(): ?Offer
    {
        return $this->offer;
    }

    public function setOffer(?Offer $offer): self
    {
        $this->offer = $offer;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|Prestation[]
     */
    public function getPrestations(): Collection
    {
        return $this->prestations;
    }

    public function addPrestation(Prestation $prestation): self
    {
        if (!$this->prestations->contains($prestation)) {
            $this->prestations[] = $prestation;
        }

        return $this;
    }

    public function removePrestation(Prestation $prestation): self
    {
        $this->prestations->removeElement($prestation);

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(?string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(?string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getCarburant(): ?string
    {
        return $this->carburant;
    }

    public function setCarburant(?string $carburant): self
    {
        $this->carburant = $carburant;

        return $this;
    }

    public function getDateOfReceipt(): ?\DateTimeInterface
    {
        return $this->dateOfReceipt;
    }

    public function setDateOfReceipt(?\DateTimeInterface $dateOfReceipt): self
    {
        $this->dateOfReceipt = $dateOfReceipt;

        return $this;
    }

    public function getReceptionPeriod(): ?string
    {
        return $this->receptionPeriod;
    }

    public function setReceptionPeriod(string $receptionPeriod): self
    {
        $this->receptionPeriod = $receptionPeriod;

        return $this;
    }

    public function getPaymentStatus(): ?bool
    {
        return $this->paymentStatus;
    }

    public function setPaymentStatus(?bool $paymentStatus): self
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    public function getOrderStatus(): ?int
    {
        return $this->orderStatus;
    }

    public function setOrderStatus(?int $orderStatus): self
    {
        $this->orderStatus = $orderStatus;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(?bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid(?string $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(?\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

}
