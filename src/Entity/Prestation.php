<?php

namespace App\Entity;

use App\Repository\PrestationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass=PrestationRepository::class)
 */
class Prestation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")

     */
    private $price;



    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\ManyToMany(targetEntity=Commande::class, mappedBy="prestations")
     */
    private $commandes;

    /**
     * @ORM\OneToMany(targetEntity=PrestationStation::class, mappedBy="prestation")
     */
    private $prestationStations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icon;

    /**
     * @ORM\ManyToMany(targetEntity=Offer::class, mappedBy="prestations")
     */
    private $offers;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="prestations")
     */
    private $station;
    public function __construct()
    {
        $this->isActive=true;
        $this->isDeleted=false;
        $this->price=0;
        $this->commandes = new ArrayCollection();
        $this->prestationStations = new ArrayCollection();
        $this->offers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->addPrestation($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            $commande->removePrestation($this);
        }

        return $this;
    }

    /**
     * @return Collection|PrestationStation[]
     */
    public function getPrestationStations(): Collection
    {
        return $this->prestationStations;
    }

    public function addPrestationStation(PrestationStation $prestationStation): self
    {
        if (!$this->prestationStations->contains($prestationStation)) {
            $this->prestationStations[] = $prestationStation;
            $prestationStation->setPrestation($this);
        }

        return $this;
    }

    public function removePrestationStation(PrestationStation $prestationStation): self
    {
        if ($this->prestationStations->removeElement($prestationStation)) {
            // set the owning side to null (unless already changed)
            if ($prestationStation->getPrestation() === $this) {
                $prestationStation->setPrestation(null);
            }
        }

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->addPrestation($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->removeElement($offer)) {
            $offer->removePrestation($this);
        }

        return $this;
    }
}
