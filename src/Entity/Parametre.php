<?php

namespace App\Entity;

use App\Repository\ParametreRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParametreRepository::class)
 */
class Parametre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $language = [];
    public static $languageToString = [
        'Français' => 'fr',
        'English' => 'en',
        'العربية' => 'ar',
    ];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $devise;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsNewCommand;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsConfirmCommand;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsRdvClient;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsRdvManager;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsCommandNextDay;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $smsRappelVidange;
    public function getLanguageFromIndex($i)
    {
        $key = array_search($i, self::$languageToString);

        if ($key) {
            return $key;
        }
        return 'Non définie';
        //   return self::$carburantToString[$i];

    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLanguage(): ?array
    {
        return $this->language;
    }

    public function setLanguage(?array $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getDevise(): ?string
    {
        return $this->devise;
    }

    public function setDevise(?string $devise): self
    {
        $this->devise = $devise;

        return $this;
    }

    public function getSmsNewCommand(): ?string
    {
        return $this->smsNewCommand;
    }

    public function setSmsNewCommand(?string $smsNewCommand): self
    {
        $this->smsNewCommand = $smsNewCommand;

        return $this;
    }

    public function getSmsConfirmCommand(): ?string
    {
        return $this->smsConfirmCommand;
    }

    public function setSmsConfirmCommand(?string $smsConfirmCommand): self
    {
        $this->smsConfirmCommand = $smsConfirmCommand;

        return $this;
    }

    public function getSmsRdvClient(): ?string
    {
        return $this->smsRdvClient;
    }

    public function setSmsRdvClient(?string $smsRdvClient): self
    {
        $this->smsRdvClient = $smsRdvClient;

        return $this;
    }

    public function getSmsRdvManager(): ?string
    {
        return $this->smsRdvManager;
    }

    public function setSmsRdvManager(?string $smsRdvManager): self
    {
        $this->smsRdvManager = $smsRdvManager;

        return $this;
    }

    public function getSmsCommandNextDay(): ?string
    {
        return $this->smsCommandNextDay;
    }

    public function setSmsCommandNextDay(?string $smsCommandNextDay): self
    {
        $this->smsCommandNextDay = $smsCommandNextDay;

        return $this;
    }

    public function getSmsRappelVidange(): ?string
    {
        return $this->smsRappelVidange;
    }

    public function setSmsRappelVidange(?string $smsRappelVidange): self
    {
        $this->smsRappelVidange = $smsRappelVidange;

        return $this;
    }
}
