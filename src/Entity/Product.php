<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="float")
     * @Assert\Type(
     *     type="float",
     *     message="le prix {{ value }} n'est pas valide."
     * )
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\Type(
     *     type="float",
     *     message="le prix promo {{ value }} n'est pas valide."
     * )
     */
    private $pricePromo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPromo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $idLubricant;

    /**
     * @ORM\ManyToMany(targetEntity=Station::class, inversedBy="products")
     */
    private $stations;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="product")
     */
    private $offers;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\OneToMany(targetEntity=Commande::class, mappedBy="product")
     */
    private $commandes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $file;
    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $motorisation = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $preconisation = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $alternative = [];

    public static $carburantToString = [
        'Essence' => '0',
        'Diesel' => '1',
        'Diesel avec FAP' => '2',
    ];



    public function getCarburantFromIndex($i)
    {
        $key = array_search($i, self::$carburantToString);

        if ($key) {
           return $key;
        }
        return 'Non définie';
     //   return self::$carburantToString[$i];

    }

    public function __construct()
    {
        $this->stations = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->isActive = true;
        $this->isDeleted = false;
        $this->price = 0;
        $this->pricePromo = 0;
        $this->quantity = 0;
        $this->commandes = new ArrayCollection();
        $this->motorisation=[];
        $this->preconisation=[];
        $this->alternative=[];

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPricePromo(): ?float
    {
        return $this->pricePromo;
    }

    public function setPricePromo(?float $pricePromo): self
    {
        $this->pricePromo = $pricePromo;

        return $this;
    }

    public function getIsPromo(): ?bool
    {
        return $this->isPromo;
    }

    public function setIsPromo(?bool $isPromo): self
    {
        $this->isPromo = $isPromo;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(?bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getIdLubricant(): ?int
    {
        return $this->idLubricant;
    }

    public function setIdLubricant(?int $idLubricant): self
    {
        $this->idLubricant = $idLubricant;

        return $this;
    }

    /**
     * @return Collection|Station[]
     */
    public function getStations(): Collection
    {
        return $this->stations;
    }

    public function addStation(Station $station): self
    {
        if (!$this->stations->contains($station)) {
            $this->stations[] = $station;
        }

        return $this;
    }

    public function removeStation(Station $station): self
    {
        $this->stations->removeElement($station);

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setProduct($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->removeElement($offer)) {
            // set the owning side to null (unless already changed)
            if ($offer->getProduct() === $this) {
                $offer->setProduct(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setProduct($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->removeElement($commande)) {
            // set the owning side to null (unless already changed)
            if ($commande->getProduct() === $this) {
                $commande->setProduct(null);
            }
        }

        return $this;
    }


    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(?string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getMotorisation(): ?array
    {
        return $this->motorisation;
    }

    public function setMotorisation(?array $motorisation): self
    {
        $this->motorisation = $motorisation;

        return $this;
    }

    public function getPreconisation(): ?array
    {
        return $this->preconisation;
    }

    public function setPreconisation(?array $preconisation): self
    {
        $this->preconisation = $preconisation;

        return $this;
    }

    public function getAlternative(): ?array
    {
        return $this->alternative;
    }

    public function setAlternative(?array $alternative): self
    {
        $this->alternative = $alternative;

        return $this;
    }


}
