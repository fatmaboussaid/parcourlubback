<?php
namespace  App\Utils;

class ClicToPay
{
    //protected $base_url = "https://ipay.clictopay.com/payment/rest"; //prod
    protected $base_url = "https://test.clictopay.com/payment/rest";//test
    protected $click_to_pay_user = "1120280019";
    protected $click_to_pay_password = "M9Ltxd24F";
    protected $amount = 0;
    protected $currency = 788;//Euro 978 , TND: 788
    protected $order_number = "";
    protected $language = "fr";



    public function payViaClickToPay($order_number, $amount, $currency, $lg = "fr")
    {
        $this->order_number = $order_number;
        $this->amount = $amount*1000;
        $this->currency = $currency;
        $this->language = $lg;

        $cURLConnection = curl_init();

        $convertedTime = date('Y-m-d\TH:i:s', strtotime('+15 minutes', strtotime(date('Y-m-d\TH:i:s'))));


        //$url =  $this->base_url . '/register.do?amount=' . $this->amount . '&currency=' . $this->currency . '&language=' . $this->language . '&orderNumber=' . $this->order_number . '&password=' . $this->click_to_pay_password . '&returnUrl=https://www.med.tn/payments/index.php&userName=' . $this->click_to_pay_user . '&jsonParams={"orderNumber":' . $this->order_number . '}&pageView=DESKTOP&expirationDate='.$convertedTime;
        $url =  $this->base_url . '/register.do?amount=' . $this->amount . '&currency=' . $this->currency . '&language=' . $this->language . '&orderNumber=' . $this->order_number . '&password=' . $this->click_to_pay_password . '&returnUrl=https://lub-online-front.app4mob.net&userName=' . $this->click_to_pay_user . '&pageView=DESKTOP';

        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($cURLConnection);

        curl_close($cURLConnection);

        $decoded_result = json_decode($result);
        return $decoded_result;
       

    }

    public function verifyOrder($orderId){

       $url =  $this->base_url . "/getOrderStatusExtended.do?orderId=$orderId&language=fr&password=".$this->click_to_pay_password."&userName=".$this->click_to_pay_user;
       
       $cURLConnection = curl_init();

       curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($cURLConnection);

        curl_close($cURLConnection);

        $decoded_result = json_decode($result);
        return $decoded_result;
    }
}
