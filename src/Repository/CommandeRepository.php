<?php

namespace App\Repository;

use App\Entity\Commande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commande[]    findAll()
 * @method Commande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commande::class);
    }
    public function findCompletedCommandes($product="",$offer="",$station="",$statut="",$qrcode="",
                                           $dateDebut="",$dateFin="",$client="",$idcommande="",$vehicule,$prestation)
    {

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  true');
        if ($idcommande != '') {
            $query->andWhere('c.id = :idcommande ')
                ->setParameter('idcommande',  $idcommande );
        }
        if ($product != '') {
            $query->leftJoin('c.product','p')
                ->andWhere('p.id = :product ')
                ->setParameter('product',  $product );
        }


        if ($offer != '') {
            $query->leftJoin('c.offer','f')
                ->andWhere('f.id = :offer ')
                ->setParameter('offer',  $offer );
        }
        if ($station != '') {
            $query->leftJoin('c.station','s')
                ->andWhere('s.id = :station ')
                ->setParameter('station',  $station );
        }
        if ($statut != '') {
            $query->andWhere('c.orderStatus = :statut ')
                ->setParameter('statut',  $statut );
        }
        if ($qrcode != '') {
            $query->andWhere('c.uid like :uid ')
                ->setParameter('uid',  '%'.$qrcode.'%' );
        }
        if ($client != '') {
            $query->leftJoin('c.user','u')
                ->andWhere('u.firstName like :client or u.lastName like :client or 
                 u.tel like :client or u.email like :client ')
                ->setParameter('client',  '%'.$client.'%' );
        }


        if ($dateDebut != "") {
            try {
                $dateDebut = \DateTime::createFromFormat("d/m/Y", $dateDebut);
            }catch ( \Exception $e){
                $dateDebut = new \DateTime();
            }

            $query->andWhere('c.createAt >= :dateDebut ')
                ->setParameter('dateDebut', $dateDebut->format('Y-m-d 00:00:00'));
        }

        if ($dateFin != "") {
            try {
                $dateFin = \DateTime::createFromFormat("d/m/Y", $dateFin);
            }catch ( \Exception $e){
                $dateFin = new \DateTime();
            }
            $query->andWhere('c.createAt <= :dateFin ')
                ->setParameter('dateFin',  $dateFin->format('Y-m-d 23:59:59'));
        }
        if ($prestation != '') {
            $query->leftJoin('c.prestations','pr')
                ->andWhere('pr.id = :prestation')
                ->setParameter('prestation',  $prestation);
        }
        if ($vehicule != '') {
            $query->andWhere('c.marque like :vehicule or c.marque like :vehicule or c.year
             like :vehicule or c.carburant like :vehicule ')
                ->setParameter('vehicule',  '%'.$vehicule.'%' );
        }
        return $query->orderBy('c.createAt', 'DESC')->getQuery()->getResult();
    }
    public function findCommandesByDate($date="")
    {

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  true and c.orderStatus != 5');

        if ($date != '') {
            $date1 = \DateTime::createFromFormat("d/m/Y", $date);



            $query->andWhere('c.dateOfReceipt >= :date_debut and  c.dateOfReceipt <= :date_fin')
                ->setParameter('date_debut', $date1->format('Y-m-d 00:00:00'))
                ->setParameter('date_fin', $date1->format('Y-m-d 23:59:59'));

        }


        return $query->orderBy('c.id', 'DESC')->getQuery()->getResult();
    }
    public function findCommandesHistoriqueByUser($user="")
    {
        $now = new \DateTime();

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  true')
           ->andWhere('c.dateOfReceipt <= :now  or c.dateOfReceipt is null or c.orderStatus = 5')
            ->setParameter('now', $now->format('Y-m-d'));
        if ($user != '') {
            $query->leftJoin('c.user','u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $user);
        }


        return $query->orderBy('c.id', 'DESC')->getQuery()->getResult();
    }
    public function findCommandesRendezVousByUser($user="")
    {
        $now = new \DateTime();

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  true')
            ->andWhere('c.dateOfReceipt > :now and (c.orderStatus != 4 and c.orderStatus != 5) ')
            ->setParameter('now', $now->format('Y-m-d'));
        if ($user != '') {
            $query->leftJoin('c.user','u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $user);
        }


        return $query->orderBy('c.id', 'DESC')->getQuery()->getResult();
    }
    public function findCommandesByUser($user="")
    {

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  true')
            ->andWhere('c.dateOfReceipt is null and (c.orderStatus != 4 and c.orderStatus != 5)')
        ;
        if ($user != '') {
            $query->leftJoin('c.user','u')
                ->andWhere('u.id = :userId')
                ->setParameter('userId', $user);
        }


        return $query->orderBy('c.id', 'DESC')->getQuery()->getResult();
    }
    public function findInCompletedCommandes($product="",$offer="",$station="",$dateDebut="",$dateFin="",
                                             $client="",$vehicule="",$prestation="")
    {

        $query = $this->createQueryBuilder('c')
            ->where('c.isCompleted =  false');
        if ($product != '') {
            $query->leftJoin('c.product','p')
                ->andWhere('p.id = :product ')
                ->setParameter('product',  $product );
        }
        if ($offer != '') {
            $query->leftJoin('c.offer','f')
                ->andWhere('f.id = :offer ')
                ->setParameter('offer',  $offer );
        }
        if ($station != '') {
            $query->leftJoin('c.station','s')
                ->andWhere('s.id = :station ')
                ->setParameter('station',  $station );
        }
        if ($client != '') {
            $query->leftJoin('c.user','u')
                ->andWhere('u.firstName like :client or u.lastName like :client or 
                 u.tel like :client or u.email like :client ')
                ->setParameter('client',  '%'.$client.'%' );
        }


        if ($dateDebut != "") {
            try {
                $dateDebut = \DateTime::createFromFormat("d/m/Y", $dateDebut);
            }catch ( \Exception $e){
                $dateDebut = new \DateTime();
            }

            $query->andWhere('c.createAt >= :dateDebut ')
                ->setParameter('dateDebut', $dateDebut->format('Y-m-d 00:00:00'));
        }

        if ($dateFin != "") {
            try {
                $dateFin = \DateTime::createFromFormat("d/m/Y", $dateFin);
            }catch ( \Exception $e){
                $dateFin = new \DateTime();
            }
            $query->andWhere('c.createAt <= :dateFin ')
                ->setParameter('dateFin',  $dateFin->format('Y-m-d 23:59:59'));
        }
        if ($prestation != '') {
            $query->leftJoin('c.prestations','pr')
                ->andWhere('pr.id = :prestation')
                ->setParameter('prestation',  $prestation);
        }
        if ($vehicule != '') {
            $query->andWhere('c.marque like :vehicule or c.marque like :vehicule or c.year
             like :vehicule or c.carburant like :vehicule ')
                ->setParameter('vehicule',  '%'.$vehicule.'%' );
        }
        return $query->orderBy('c.id', 'DESC')->getQuery()->getResult();
    }
    // /**
    //  * @return Commande[] Returns an array of Commande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Commande
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
