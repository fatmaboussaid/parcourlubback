<?php

namespace App\Repository;

use App\Entity\Search;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Search|null find($id, $lockMode = null, $lockVersion = null)
 * @method Search|null findOneBy(array $criteria, array $orderBy = null)
 * @method Search[]    findAll()
 * @method Search[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SearchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Search::class);
    }
    public function findAllSearchs($keyword="",$dateDebut,$dateFin)
    {
        $query = $this->createQueryBuilder('s');
        if ($keyword != '') {
            $query->andWhere('s.marque like :keyword or s.marque like :keyword or s.year like :keyword or s.carburant like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }
        if ($dateDebut != "") {
            try {
                $dateDebut = \DateTime::createFromFormat("d/m/Y", $dateDebut);
            }catch ( \Exception $e){
                $dateDebut = new \DateTime();
            }

            $query->andWhere('s.createAt >= :dateDebut ')
                ->setParameter('dateDebut', $dateDebut->format('Y-m-d 00:00:00'));
        }

        if ($dateFin != "") {
            try {
                $dateFin = \DateTime::createFromFormat("d/m/Y", $dateFin);
            }catch ( \Exception $e){
                $dateFin = new \DateTime();
            }
            $query->andWhere('s.createAt <= :dateFin ')
                ->setParameter('dateFin',  $dateFin->format('Y-m-d 23:59:59'));
        }
        return $query->orderBy('s.id', 'DESC')->getQuery()->getResult();
    }
    // /**
    //  * @return Search[] Returns an array of Search objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Search
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
