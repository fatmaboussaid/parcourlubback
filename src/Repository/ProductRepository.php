<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findProducts($keyword = "", $station = "")
    {
        $query = $this->createQueryBuilder('p')
            ->where('p.isDeleted =  false ');
        if ($keyword != '') {
            $query->andWhere('p.name like :keyword or p.description like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }

    public function findActiveProducts($keyword = "", $station = "",$max=0)
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.stations','s', 'WITH', 's.isDeleted =  false and s.isActive =  true')
            ->where('p.isDeleted =  false ')
            ->andWhere('p.isActive =  true ');

        if ($keyword != '') {
            $query->andWhere('p.name like :keyword or p.description like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }

        if ($station != '') {
            $query->andWhere('s.id = :station ')
                ->setParameter('station',  $station );
        }
        if (intval($max) != 0)  {
          //  $query->setMaxResults($max);
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    public function findRecommendedProducts($stationid,$recommendedLubricants,$carburantType='')
    {

        $recommendedLubricants = str_replace("[", "", $recommendedLubricants);
        $recommendedLubricants = str_replace("]", "", $recommendedLubricants);

        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.stations','s','with','s.id = :stationId')
            ->andWhere('p.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and s.isActive =  true ')
           //->andWhere('p.quantity > 0 ')
            ->andWhere('p.idLubricant in (:recommendedLubricants)')
            ->setParameter('stationId',$stationid)
            ->setParameter('recommendedLubricants',$recommendedLubricants)

        ;
        if($carburantType != ''){

            $query->andWhere('p.motorisation like :carburantType')
             ->setParameter('carburantType', '%:'.strlen($carburantType).':"' . $carburantType . '"%' );

            ;
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    public function findDefaultRecommendedProducts($stationid,$carburantType='')
    {


        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.stations','s','with','s.id = :stationId')
            ->andWhere('p.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and s.isActive =  true ')
            //->andWhere('p.quantity > 0 ')
            ->setParameter('stationId',$stationid)

        ;
        if($carburantType != ''){

            $query->andWhere('  p.preconisation like :carburantType  ')
                ->setParameter('carburantType', '%:'.strlen($carburantType).':"' . $carburantType . '"%' );
            ;
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    public function findAlternativeProducts($stationid,$alternativeLubricants,$carburantType='')
    {
        $alternativeLubricants = str_replace("[", "", $alternativeLubricants);
        $alternativeLubricants = str_replace("]", "", $alternativeLubricants);
        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.stations','s','with','s.id = :stationId')

            // ->where('s.id = :stationId')
            ->andWhere('p.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and s.isActive =  true ')
            //->andWhere('p.quantity > 0 ')
            ->andWhere('p.idLubricant in (:alternativeLubricants)')
           // ->andWhere('p.idLubricant in (:alternativeLubricants)')
           ->setParameter('stationId',$stationid)

            ->setParameter('alternativeLubricants',$alternativeLubricants)
        ;
        if($carburantType != ''){
            $query->andWhere('  p.motorisation like :carburantType  ')
                ->setParameter('carburantType', '%:'.strlen($carburantType).':"' . $carburantType . '"%' );

            ;
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    public function findDefaultAlternativeProducts($stationid,$carburantType='')
    {

        $query = $this->createQueryBuilder('p')
            ->innerJoin('p.stations','s','with','s.id = :stationId')

            // ->where('s.id = :stationId')
            ->andWhere('p.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and s.isActive =  true ')
            //->andWhere('p.quantity > 0 ')
            // ->andWhere('p.idLubricant in (:alternativeLubricants)')
            ->setParameter('stationId',$stationid)

        ;
        if($carburantType != ''){
            $query->andWhere('p.alternative like :carburantType  ')
                ->setParameter('carburantType', '%:'.strlen($carburantType).':"' . $carburantType . '"%' );
            ;
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }




    public function findActiveProduct($id)
    {
        $query = $this->createQueryBuilder('p')
           // ->innerJoin('p.stations','s')
           ->leftJoin('p.stations','s', 'WITH', 's.isDeleted =  false and s.isActive =  true')
            ->where('p.isDeleted =  false ')
            ->andWhere('p.isActive =  true ')
            ->andWhere('p.id = :id ')
            ->setParameter('id', $id);

        return $query->getQuery()->getOneOrNullResult();
    }
    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
