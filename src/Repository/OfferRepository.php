<?php

namespace App\Repository;

use App\Entity\Offer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Offer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offer[]    findAll()
 * @method Offer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Offer::class);
    }
    public function findOffers($keyword="",$station="",$id="")
    {
        $query = $this->createQueryBuilder('o')
            ->leftJoin('o.product','p')
            ->leftJoin('o.stations','s')
            ->where('o.isDeleted =  false and p.isDeleted =  false and s.isDeleted =  false');
        if ($keyword != '') {
            $query->andWhere('o.name like :keyword or o.description like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }
        if ($station != '') {
            $query->andWhere('s.id = :station ')
                ->setParameter('station',  $station );
        }
        if ($id != '') {
            $query->andWhere('o.id = :id ')
                ->setParameter('id',  $id );
        }
        return $query->orderBy('o.id', 'DESC')->getQuery()->getResult();
    }
    public function findActiveOffers($keyword="",$station="",$max=0)
    {
        $query = $this->createQueryBuilder('o')
            ->addSelect('s')
            ->addSelect('p')
            ->innerJoin('o.product','p', 'WITH', 'p.isDeleted =  false and p.isActive =  true')
            ->innerJoin('o.stations','s', 'WITH', 's.isDeleted =  false and s.isActive =  true')
            ->where(' o.isDeleted =  false  ')
            ->andWhere('o.isActive =  true ');

        if ($keyword != '') {
            $query->andWhere('o.name like :keyword or o.description like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }
        if ($station != '') {
            $query->andWhere('s.id = :station ')
                ->setParameter('station',  $station );
        }

        return $query->orderBy('o.id', 'DESC')->getQuery()->getResult();
    }
    public function findActiveOffer($id,$latitude='',$longitude='')
    {
        $query = $this->createQueryBuilder('o')
            ->innerJoin('o.product','p', 'WITH', 'p.isDeleted =  false and p.isActive =  true')
            ->innerJoin('o.stations','s', 'WITH', 's.isDeleted =  false and s.isActive =  true')
            ->where('o.isDeleted =  false and p.isDeleted =  false ')
            ->andWhere('o.isActive =  true and p.isActive =  true ')
            ->andWhere('o.id = :id ')
            ->setParameter('id', $id);

      /*  if ($latitude != '' && $longitude != '') {
            //    $select='SELECT latitude, longitude, SQRT(
            // POW(69.1 * (latitude - [startlat]), 2) +
            //  POW(69.1 * ([startlng] - longitude) * COS(latitude / 57.3), 2)) AS distance
//FROM TableName HAVING distance < 25 ORDER BY distance';
            $query->addSelect('SQRT(power(69.1 * (s.latitude - :latitude), 2) +
            power(69.1 * (:longitude - s.longitude) * COS(s.latitude / 57.3), 2)) AS distance')
                ->setParameter('latitude', $latitude)
                ->setParameter('longitude', $longitude)
                ->orderBy('case when distance is null then 2 else 1 end, distance' ,'ASC')
            ;
        }
        else{
            $query->orderBy('s.name');
        }*/
        return $query->getQuery()->getOneOrNullResult();
    }

    public function findRecommendedOffers($stationid,$idsProducts)
    {
        $query = $this->createQueryBuilder('o')
            ->leftJoin('o.product','p')
            ->leftJoin('o.stations','s')
            ->where('s.id = :stationId')
            ->andWhere('p.isDeleted =  false and o.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and o.isActive =  true and s.isActive =  true')
            //->andWhere('o.quantity > 0 ')
            ->setParameter('stationId',$stationid)

        ;

        if(strlen($idsProducts)>=1){
            $query->andWhere('p.id in (:idsProducts)')
                ->setParameter('idsProducts',$idsProducts);
        }
        else{
            $query->andWhere('p.id in (0)');
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    public function findAlternativeOffers($stationid,$idsAlternativeProducts)
    {

        $query = $this->createQueryBuilder('o')
            ->leftJoin('o.product','p')
            ->leftJoin('o.stations','s')
            ->where('s.id = :stationId')
            ->andWhere('p.isDeleted =  false and o.isDeleted =  false and s.isDeleted =  false')
            ->andWhere('p.isActive =  true and o.isActive =  true and s.isActive =  true')

            ->setParameter('stationId',$stationid)

        ;
        if(strlen($idsAlternativeProducts)>2){

            $query->andWhere('p.id in (:idsAlternativeProducts)')
                ->setParameter('idsAlternativeProducts',$idsAlternativeProducts)
            ;
        }
        else{
            $query->andWhere('p.id in (0)');
        }
        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }


    // /**
    //  * @return Offer[] Returns an array of Offer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Offer
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
