<?php

namespace App\Repository;

use App\Entity\Prestation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Prestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prestation[]    findAll()
 * @method Prestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prestation::class);
    }
    public function findPrestations($keyword="",$station="",$prestationsIds="")
    {
        $query = $this->createQueryBuilder('p')
          ->where('p.isDeleted =  false and p.isActive = true');
        if ($keyword != '') {
            $query->andWhere('p.name like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }

        if ($station != '') {
               //  WHERE ps.produit is null and p.country like :country order by p.id desc '



          /*  $query->leftJoin('p.prestationStations','ps','with', 'p.id= ps.prestation')
            //   ->leftJoin('ps.station','s','with', 'ps.station = s.id')
               // ->andWhere('ps IS NULL')
                //->andWhere('ps.id IS NOT NULL')
               ->andWhere('s.id != :station or ps is Null')
                ->setParameter('station', $station)*/
            ;
        }
        $prestationsIds = str_replace('[', '', $prestationsIds);
        $prestationsIds = str_replace(']', '', $prestationsIds);
        $prestationsIds = str_replace('"', '', $prestationsIds);
        if ($prestationsIds != '') {
            $query
                ->andWhere('p.id IN ('.$prestationsIds.') ')
              // ->setParameter('prestationsIds',  '1 , 2')
            ;
        }

        return $query->orderBy('p.id', 'DESC')->getQuery()->getResult();
    }
    // /**
    //  * @return Prestation[] Returns an array of Prestation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prestation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
