<?php

namespace App\Repository;

use App\Entity\PrestationStation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PrestationStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrestationStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrestationStation[]    findAll()
 * @method PrestationStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrestationStationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrestationStation::class);
    }
    public function findPrestationsStation($keyword="",$station="",$prestationsIds="",$verif=true)
    {
        $query = $this->createQueryBuilder('ps')
            ->leftJoin('ps.prestation','p','with','ps.prestation = p ')
            ->where('p.isDeleted =  false and p.isActive = true')
        ;
        if ($keyword != '') {
            $query->andWhere('ps.name like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }

        if ($station != '') {
            $query
                ->leftJoin('ps.station','s')
                ->andWhere('s.id = :station ')
             //   ->andWhere('s.isDeleted =  false and s.isActive = true')
                ->setParameter('station', $station);
        }

        $prestationsIds = str_replace('[', '', $prestationsIds);
        $prestationsIds = str_replace(']', '', $prestationsIds);
        $prestationsIds = str_replace('"', '', $prestationsIds);
        if ($prestationsIds != '') {
            $query->andWhere('ps.id IN ('.$prestationsIds.') ');
        }
        else{
            if($verif) $query->andWhere('ps.id IN (0) ');
        }

        return $query->orderBy('ps.id', 'DESC')->getQuery()->getResult();
    }
    // /**
    //  * @return PrestationStation[] Returns an array of PrestationStation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PrestationStation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
