<?php

namespace App\Repository;

use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Station|null find($id, $lockMode = null, $lockVersion = null)
 * @method Station|null findOneBy(array $criteria, array $orderBy = null)
 * @method Station[]    findAll()
 * @method Station[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Station::class);
    }

    // /**
    //  * @return Station[] Returns an array of Station objects
    //  */

    public function findStations($keyword="")
    {
        $query = $this->createQueryBuilder('s')
            ->where('s.isDeleted =  false');
        if ($keyword != '') {
            $query->andWhere('s.name like :keyword or s.address like :keyword or s.tel like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }
        return $query->orderBy('s.id', 'DESC')->getQuery()->getResult();
    }
    public function findActiveStations($keyword="",$isOilChange=false,$latitude='',$longitude='')
    {
        $isOilChange = json_decode($isOilChange);

        $query = $this->createQueryBuilder('s')
            ->where('s.isDeleted =  false')
            ->andWhere('s.isActive =  true');
        if ($keyword != '') {
            $query->andWhere('s.name like :keyword or s.address like :keyword or s.tel like :keyword ')
                ->setParameter('keyword', '%' . $keyword . '%');
        }


        if ($isOilChange ==true) {
            $query->andWhere('s.isOilChange = true');
        }
        if ($latitude != '' && $longitude != '') {
        //    $select='SELECT latitude, longitude, SQRT(
   // POW(69.1 * (latitude - [startlat]), 2) +
  //  POW(69.1 * ([startlng] - longitude) * COS(latitude / 57.3), 2)) AS distance
//FROM TableName HAVING distance < 25 ORDER BY distance';
            $query->addSelect('SQRT(power(69.1 * (s.latitude - :latitude), 2) +
            power(69.1 * (:longitude - s.longitude) * COS(s.latitude / 57.3), 2)) AS distance')
                ->setParameter('latitude', $latitude)
                ->setParameter('longitude', $longitude)
                ->orderBy('case when distance is null then 2 else 1 end, distance' ,'ASC')
            ;
        }
        else{
            $query->orderBy('s.name','ASC');
        }
        //dump($query->getQuery()->getResult());die;
        return $query->getQuery()->getResult();
    }
    public function findActiveStation($id)
    {
        $query = $this->createQueryBuilder('s')
            ->where('s.id =  :id')
            ->andWhere('s.isDeleted =  false')
            ->andWhere('s.isActive =  true')
            ->setParameter('id',$id);

        return $query->getQuery()->getOneOrNullResult();
    }
    /*
    public function findOneBySomeField($value): ?Station
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
