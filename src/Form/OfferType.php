<?php

namespace App\Form;

use App\Entity\Offer;
use App\Entity\Prestation;
use App\Entity\Product;
use App\Entity\Station;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['required' => true, 'trim' => true])
            ->add('description', TextareaType::class,[ 'required' => false,])
            ->add('price', NumberType::class, [
                'required' => true,
                'trim' => true,
                'invalid_message' => 'le prix est invalide',

                'attr' => [
                    'min' => 0,
                ]])
            ->add('pricePromo', NumberType::class, [
                'invalid_message' => 'le prix promo est invalide',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ]
            ])
            ->add('quantity', NumberType::class, array(
                'attr' => array('min' => 0)
            ))
            ->add('isPromo')
            ->add('isActive')

            ->add('product', EntityType::class, array(
                    'class' => Product::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'multiple' => false,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('p')
                            ->where('p.isActive=true and p.isDeleted = false');
                    },
                    'placeholder' => 'choisir un produit',
                    'attr' => ['data-plugin' => 'select2'],
                )
            )
            ->add('prestations', EntityType::class, array(
                    'class' => Prestation::class,
                    'choice_label' => 'name',
                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('p')
                            ->where('p.isActive=true and p.isDeleted = false')
                            ->orderBy('p.name');
                    },
                    'placeholder' => 'choisir la liste des prestations',
                    'attr' => ['data-plugin' => 'select2'],
                )
            )


        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
