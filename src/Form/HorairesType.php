<?php

namespace App\Form;

use App\Entity\Station;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HorairesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('horaires',ChoiceType::class, array(
                'required'   => true,
                'multiple' => true,
                'placeholder' => 'Choisir votre horaire',
                'label' =>false,
                'choices'  =>Station::$horaire_values

            ))
            ->add('isClosedSunday')
            ->add('isOilChange')
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Station::class,
        ]);
    }
}
