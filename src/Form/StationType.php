<?php

namespace App\Form;

use App\Entity\Prestation;
use App\Entity\PrestationStation;
use App\Entity\Station;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class StationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['required' => true, 'trim' => true])
            ->add('address', null, ['trim' => true])
            ->add('tel', null, ['trim' => true])
            ->add('isActive')
            ->add('image', FileType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'choisir image'],
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                            'image/ief',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger une image valide',
                    ])
                ],
            ])
            ->add('horaires', ChoiceType::class, array(
                'required' => true,
                'multiple' => true,
                'placeholder' => 'Choisir votre horaire',
                'label' => false,
                'choices' => Station::$horaire_values

            ))
            ->add('isClosedSunday')
            ->add('isOilChange')
            /*->add('prestationStations', EntityType::class, array(
                    'class' => PrestationStation::class,
                    'choice_label' => function ($prestationStation) {
                        return $prestationStation->getPrestation()->getName();
                    },

                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        $station = $options['station'];
                        $query = $er->createQueryBuilder('ps')
                            ->leftJoin('ps.prestation', 'p', 'with', 'ps.prestation = p ')
                            ->where('p.isDeleted =  false and p.isActive = true')
                            ->orderBy('p.name');
                        if ($station != '') {
                            $query
                                ->leftJoin('ps.station', 's')
                                ->andWhere('s.id = :stationId ')
                                //   ->andWhere('s.isDeleted =  false and s.isActive = true')
                                ->setParameter('stationId', $station->getId());
                        }
                        return $query;
                    },
                    'placeholder' => 'choisir la liste des prestations',
                    'attr' => ['data-plugin' => 'select2'],
                )
            */
            ->add('prestations', EntityType::class, array(
                    'class' => Prestation::class,
                    'choice_label' => function ($prestation) {
                        return $prestation->getName();
                    },
                    'required' => false,
                    'multiple' => true,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        $station = $options['station'];
                        $query = $er->createQueryBuilder('p')
                            ->where('p.isDeleted =  false and p.isActive = true')
                            ->orderBy('p.name');

                        return $query;
                    },
                    'placeholder' => 'choisir la liste des prestations',
                    'attr' => ['data-plugin' => 'select2'],
                ))
            ->add('latitude', NumberType::class, array(
                'scale' => 6,
                'label' => false,
                'required' => false,
            ))
            ->add('longitude', NumberType::class, array(
                'scale' => 6,
                'label' => false,
                'required' => false,
            ))
            ->add('file', FileType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'choisir un fichier'],

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger un fichier  pdf',

                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Station::class,
            'station' => Station::class,
        ]);
    }
}
