<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Station;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['required' => true, 'trim' => true])
            ->add('price', NumberType::class, [
                'required' => true,
                'trim' => true,
                'invalid_message' => 'le prix est invalide',

                'attr' => [
                    'min' => 0,
                ]])
            ->add('pricePromo', NumberType::class, [
                'invalid_message' => 'le prix promo est invalide',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ]
            ])
            ->add('quantity', NumberType::class, array(
                'attr' => array('min' => 0)
            ))
            ->add('isPromo')
            ->add('isActive')
            ->add('motorisation', ChoiceType::class, [
                    'choices' => Product::$carburantToString,
                    'required' => false,
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => ['class' => 'checkbox-custom checkbox-primary'],
                    'choice_translation_domain' => 'messages',
                ]
            )
            ->add('preconisation', ChoiceType::class, [
                    'choices' => Product::$carburantToString,
                    'required' => false,
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => ['class' => 'checkbox-custom checkbox-primary'],
                    'choice_translation_domain' => 'messages',
                ]
            )
            ->add('alternative', ChoiceType::class, [
                    'choices' => Product::$carburantToString,
                    'required' => false,
                    'expanded' => true,
                    'multiple' => true,
                    'attr' => ['class' => 'checkbox-custom checkbox-primary'],
                    'choice_translation_domain' => 'messages',
                ]
            )
            ->add('file', FileType::class, [
                'label' => false,
                'attr' => ['placeholder' => 'choisir un fichier'],

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'Veuillez télécharger un fichier  pdf',

                    ])
                ],
            ])
            ->add('description', TextareaType::class, ['required' => false,]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
