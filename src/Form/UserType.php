<?php

namespace App\Form;

use App\Entity\Station;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', null, ['required' => true, 'trim' => true])
            ->add('firstName', null, ['required' => true, 'trim' => true])
            ->add('lastName', null, ['required' => true, 'trim' => true])
            ->add('tel', null, ['required' => false, 'trim' => true])
            ->add('codeSap', null, ['required' => false, 'trim' => true])
            ->add('station', EntityType::class, array(
                    'class' => Station::class,
                    'choice_label' => 'name',
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('s')
                            ->where(' s.isDeleted = false');
                    },
                    'placeholder' => 'choisir une station',
                    'attr' => ['data-plugin' => 'select2'],
                )
            )
            ->add('isActive')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
