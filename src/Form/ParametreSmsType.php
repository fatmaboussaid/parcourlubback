<?php

namespace App\Form;

use App\Entity\Parametre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParametreSmsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('smsNewCommand', TextareaType::class,[ 'required' => false,])
            ->add('smsConfirmCommand', TextareaType::class,[ 'required' => false,])
            ->add('smsRdvClient', TextareaType::class,[ 'required' => false,])
            ->add('smsRdvManager', TextareaType::class,[ 'required' => false,])
            ->add('smsCommandNextDay', TextareaType::class,[ 'required' => false,])
            ->add('smsRappelVidange', TextareaType::class,[ 'required' => false,])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Parametre::class,
        ]);
    }
}
