<?php


namespace App\Service;
use Endroid\QrCode\Builder\BuilderInterface;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class QrCodeService
{

    private $builder;
    /**
     * @param BuilderInterface $builder
     */
    public function __construct( )
    {
        $this->builder=new Builder();

    }
    public function qrcode($query,$savepath,$uid){
        $dateTime=new \DateTime();
        $dateString=$dateTime->format('d-m-Y H:i:s');
        $result = $this->builder
            ->data($query)
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->size(400)
            ->margin(10)
            ->labelText($uid)
            ->build();
        $result->saveToFile( $savepath);
        return $result->getDataUri();
    }
}