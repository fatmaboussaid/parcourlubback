<?php


namespace App\Service;

use App\Repository\ParametreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;

class SendEmailService
{

    private $mailer;
    private $templating;
    private $parametreRepository;


    public function __construct(\Swift_Mailer $mailer, \Twig\Environment $templating,ParametreRepository $parametreRepository)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->parametreRepository = $parametreRepository;

    }

    public function sendEmails($emails, $subject, $template, $data,$type)
    {

        $transport = (new \Swift_SmtpTransport('smtp.office365.com', 25, 'tls'))
            ->setUsername('notification.tn@total.tn')
            ->setPassword('botmOn82021!');
        $transport->setLocalDomain('[127.0.0.1]');
        $mailer = new \Swift_Mailer($transport);

        try {

            $parametre=$this->parametreRepository->find(1);
            $data['devise']='TND';
            if($parametre &&  $parametre->getDevise()){
                $data['devise']= $parametre->getDevise();
            }

            $message = (new \Swift_Message($subject))
                ->setFrom(array('notification.tn@total.tn' => "Ma vidange en ligne"))
                ->setTo($emails)
                ->setBcc(['fatma.boussaid@app4mob.net','fatma.boussaid@app4mob.net'])
            ;
            $data['img'] ='';
            if($data['commande']->getUid() and $type ==1)
                $data['img'] = $message->embed(\Swift_Image::fromPath('uploads/qrcodes/'.$data['commande']->getUid() . '.png'));
            $message->setBody(
                $this->templating->render(
                    $template, $data
                ),
                'text/html'
            );
            $mailer->send($message);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        $result = $this->mailer->send($message);
        return $result;
    }

}